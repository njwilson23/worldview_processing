#! /usr/bin/env python

import os
import sys
import karta
import subprocess
import tempfile
import traceback
import numpy as np
import meltpack
from wvutil import get_region, get_sceneid, ensureisdir

INPUTS = "dems"
SUFFIX = "_30m.tif"
REFERENCE_DEM = "gimpdem_90m_northern.tif"
OUTPUTS = "dems-aligned"
TEMPDIR = "tmp-align"
OVERWRITE = False

ensureisdir(OUTPUTS)
ensureisdir(TEMPDIR)

bedrock = {"Nioghalvfjerdsbrae": karta.read_shapefile("Nioghalvfjerdsbrae/niog_bedrock.shp"),
           "Petermann": karta.read_shapefile("Petermann/petermann_bedrock.shp"),
           "Ryder": karta.read_shapefile("Ryder/ryder_bedrock.shp"),
           "Steensby": karta.read_shapefile("Steensby/steensby_bedrock.shp")
           }

def read_transform(prefix):
    with open(prefix+"-inverse-transform.txt") as f:
        rows = [list(map(float, line.split())) for line in f.readlines()]
    return np.array(rows)

def apply_translation(grid, transform):
    newgrid = grid.copy()
    newgrid._transform = (grid.transform[0]+transform[0,3],
                          grid.transform[1]+transform[1,3],
                          grid.transform[2], grid.transform[3],
                          grid.transform[4], grid.transform[5])
    newgrid.values += transform[2,3]
    return newgrid

fnms = [f for f in os.listdir(INPUTS) if f.endswith(SUFFIX)]
reference = karta.read_geotiff(REFERENCE_DEM)

for demfnm in fnms:

    region = get_region(get_sceneid(demfnm))
    outfnm = os.path.join(OUTPUTS, os.path.split(demfnm)[1])

    if os.path.isfile(outfnm) and not OVERWRITE:
        print("skipping %s" % outfnm)
        continue

    print("working on %s (%s)" % (demfnm, region))
    dem = karta.read_geotiff(os.path.join(INPUTS, demfnm))
    dem.values[dem.values<-100] = dem.nodata
    ref = reference.clip(*dem.extent)

    print("merging rasters", end="")
    dem_masked = dem.mask_by_poly(bedrock[region])
    ref_masked = ref.mask_by_poly(bedrock[region])
    print("  done")

    # Hack for space efficiency (works with LZW compression)
    dem_masked.values[np.isnan(dem_masked.values)] = -3.4028234663852886e+38
    dem_masked._nodata = -3.4028234663852886e+38

    try:
        # This is a fairly cavalier way of handling temporary files. There are
        # no hard guarantees that the file modified with a file extension won't
        # exist, or that a seperate thread won't create the same randomized
        # file stem. It's pretty unlikely though, so for now I'm just going to
        # cross my fingers.
        reftmpname = os.path.join(TEMPDIR, tempfile.mkstemp(dir=TEMPDIR)[1]+".tif")
        demtmpname = os.path.join(TEMPDIR, tempfile.mkstemp(dir=TEMPDIR)[1]+".tif")
        prefix = os.path.join(TEMPDIR, tempfile.mkstemp(dir=TEMPDIR)[1])
        print("saving temp gtiffs", end="")
        dem_masked.to_geotiff(demtmpname, compress='LZW')
        ref_masked.to_geotiff(reftmpname)
        print("done")

        cmd = ["pc_align", "--max-displacement", "100.0",
               "--compute-translation-only",
               demtmpname, reftmpname, "-o", prefix]
        print(" ".join(cmd))

        print("calling pc_align", end="")
        subprocess.call(cmd)
        print("done")
        T = read_transform(prefix)

    except Exception as e:
        traceback.print_exc()
        sys.exit(1)

    finally:
        os.remove(reftmpname)
        os.remove(demtmpname)

    del ref, dem_masked, ref_masked

    dem_aligned = apply_translation(dem, T)
    # Hack for space efficiency (works with LZW compression)
    dem_aligned.values[np.isnan(dem_aligned.values)] = -3.4028234663852886e+38
    dem_aligned._nodata = -3.4028234663852886e+38
    dem_aligned.to_geotiff(outfnm, compress='LZW')

    del dem_aligned, dem

    try:
        meltpack.notify.send_mail('mail.conf', 'cedar@ironicmtn.com', 'nwilson@whoi.edu',
         "finished aligning {0}\n{1}".format(demfnm, T), subject=get_sceneid(demfnm))
    except Exception as e:
        print("failed to send notification")
        print(e)

