import numpy as np
import karta
import os
import time
from concurrent.futures import ProcessPoolExecutor

from gridstats import gridstats
from wvutil import get_sceneid, get_baseline

REGION = "Steensby"
data_root = "steensby-analysis"

def exclude_bad_pixels(a):
    a[(~np.isnan(a)) & (np.abs(a) > 100)] = np.nan
    return a

def fnm2baseline(fnm):
    return get_baseline(*map(get_sceneid, fnm.split("_")))

dhdt_directory = os.path.join(data_root, "ldhdt-grids-128m/")
dhdt_filepaths = [os.path.join(dhdt_directory, f)
                      for f in os.listdir(dhdt_directory)
                      if f.endswith(".tif") and fnm2baseline(f) > 120]

dhdt_grids = [karta.read_geotiff(f, bandclass=karta.raster.SimpleBand).apply(exclude_bad_pixels)
              for f in dhdt_filepaths]

# make a list of scene ids
scids = []
for fp in dhdt_filepaths:
    scid0, scid1 = map(get_sceneid, os.path.split(fp)[1].split("_"))
    scids.append(scid0)
    scids.append(scid1)
scids = list(set(scids))

def fnm_constructable_from_scids(fnm, scids):
    scid0, scid1 = map(get_sceneid, os.path.split(fnm)[1].split("_"))
    return (scid0 in scids) and (scid1 in scids)

def compute_grid_mean(scids):
    grids = [g for g, f in zip(dhdt_grids, dhdt_filepaths) if fnm_constructable_from_scids(f, scids)]
#     n_required = len(dhdt_filepaths)
#     n_grids = len(grids)
#     grids.extend([grids[i] for i in np.random.choice(n_grids, n_required-n_grids)])
    gridmean = karta.raster.merge(grids)
    for g in grids:
        del(g)
    return gridmean

def bootstrap(meanfunc, arguments, niter, perc=1.0):
    indices = np.arange(len(arguments))
    nsamples = int(np.floor(perc*len(arguments)))

    results = []
    def oncomplete(fut):
        if fut.done():
            results.append(fut.result())
        else:
            print(fut.exception())

    with ProcessPoolExecutor(max_workers=4) as pool:
        for _ in range(niter):
            selected_indices = np.random.choice(indices, nsamples, replace=True)
            args = [arguments[i] for i in selected_indices]
            fut = pool.submit(meanfunc, args)
            fut.add_done_callback(oncomplete)

    return results

t0 = time.time()
output = bootstrap(compute_grid_mean, scids, 1000)
print("completed {0} runs in {1} seconds".format(len(output), time.time()-t0))

dhdt_mean, dhdt_var, dhdt_count = gridstats(output)
dhdt_mean.to_geotiff("bootstrap_{0}_dhdt_mean.tif".format(REGION))
dhdt_var.to_geotiff("bootstrap_{0}_dhdt_var.tif".format(REGION))
dhdt_count.to_geotiff("bootstrap_{0}_dhdt_count.tif".format(REGION))

