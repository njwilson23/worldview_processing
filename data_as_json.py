import karta
import meltpack
import numpy as np
import json

ADOTS = {
    "79N": karta.read_geotiff("adot-composites/Nioghalvfjerdsbrae-512m_20160921.tif"),
    "PG": karta.read_geotiff("adot-composites/Petermann-512m_20160921.tif"),
    "RG": karta.read_geotiff("adot-composites/Ryder-512m_20160921.tif")
}

DEMS = {
    "79N": karta.read_geotiff("dem-composites-512m/Nioghalvfjerdsbrae-512m.tif"),
    "PG": karta.read_geotiff("dem-composites-512m/Petermann-512m.tif"),
    "RG": karta.read_geotiff("dem-composites-512m/Ryder-512m.tif")
}

DATUMS = {
    "79N": 15.2,
    "RG": 12.1,
    "PG": 7.24
}

OUTLINES = {
    "79N": karta.read_geojson("Nioghalvfjerdsbrae.geojson"),
    "RG": karta.read_geojson("Ryder.geojson"),
    "PG": karta.read_geojson("Petermann.geojson")
}

DEPTHS = {}
SLOPES = {}
json_list = []
R = 917 / 1028

for glacier, dem in DEMS.items():
    dem.mask_by_poly(OUTLINES[glacier], inplace=True)
    DEPTHS[glacier] = dem.apply(lambda a: -(a-DATUMS[glacier])*R / (1-R))
    _slope = karta.raster.slope(dem)
    _slope[:,:] = meltpack.filt.smooth5(_slope[:,:].astype(np.float64), 3)
    SLOPES[glacier] = _slope

for glacier in DEMS.keys():
    adot = ADOTS[glacier]
    slope = SLOPES[glacier]
    depth = DEPTHS[glacier]

    json_list_individual = []
    m = adot.data_mask & slope.data_mask & depth.data_mask
    for a, s, d in zip(adot[m], slope[m], depth[m]):
        json_list.append({"glacier": glacier,
                          "adot": float(a),
                          "slope": float(s),
                          "depth": float(d)})

        json_list_individual.append({"glacier": glacier,
                                     "adot": float(a),
                                     "slope": float(s),
                                     "depth": float(d)})

    with open("json/{0}.json".format(glacier), "w") as f:
        json.dump(json_list_individual, f)

with open("json/all-512.json", "w") as f:
    json.dump([a for i, a in enumerate(json_list) if i%7 == 0], f)