import numpy as np
import karta
import os
import time
from concurrent.futures import ProcessPoolExecutor

from gridstats import gridstats
from wvutil import get_sceneid, get_baseline

REGION = "Ryder"
data_root = "ryder-analysis-new"

print("{0}:\n\t{1}".format(REGION, data_root))

def exclude_bad_pixels(a):
    a[np.isnan(a)] = 1e6
    a[np.abs(a) > 100] = np.nan
    return a

def fnm2baseline(fnm):
    return get_baseline(*map(get_sceneid, fnm.split("_")))

print("\tloading data")
dhdt_directory = os.path.join(data_root, "ldhdt-grids-128m/")
dhdt_filepaths = [os.path.join(dhdt_directory, f)
                      for f in os.listdir(dhdt_directory)
                      if f.endswith(".tif") and fnm2baseline(f) > 180]

dhdt_grids = [karta.read_geotiff(f, bandclass=karta.raster.SimpleBand).apply(exclude_bad_pixels)
              for f in dhdt_filepaths]

def compute_grid_mean(grids):
    gridmean = karta.raster.merge(grids)
    for g in grids:
        del(g)
    return gridmean

def bootstrap(meanfunc, arguments, niter, perc=1.0):
    indices = np.arange(len(arguments))
    nsamples = int(np.floor(perc*len(arguments)))

    results = []
    def oncomplete(fut):
        if fut.done():
            results.append(fut.result())
        else:
            print(fut.exception())

    with ProcessPoolExecutor(max_workers=4) as pool:
        for _ in range(niter):
            selected_indices = np.random.choice(indices, nsamples, replace=True)
            args = [arguments[i] for i in selected_indices]
            fut = pool.submit(meanfunc, args)
            fut.add_done_callback(oncomplete)

    return results

print("\tgenerating replicates")
t0 = time.time()
output = bootstrap(compute_grid_mean, dhdt_grids, 1000)
print("completed {0} runs in {1} seconds".format(len(output), time.time()-t0))

print("\tcomputing statistics")
dhdt_mean, dhdt_var, dhdt_count = gridstats(output)
print("\tsaving results to {0}/bootstrap_paired*".format(data_root))
dhdt_mean.to_geotiff(os.path.join(data_root,
            "bootstrap_paired_{0}_dhdt_mean.tif".format(REGION)))
dhdt_var.to_geotiff(os.path.join(data_root,
            "bootstrap_paired_{0}_dhdt_var.tif".format(REGION)))
dhdt_count.to_geotiff(os.path.join(data_root,
            "bootstrap_paired_{0}_dhdt_count.tif".format(REGION)))

