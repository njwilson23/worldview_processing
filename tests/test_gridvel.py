import unittest
import numpy as np
import karta

import sys
sys.path.append("../bin/")
import gridvel

class TestGridvelCLI(unittest.TestCase): pass

class TestGridvelFuncs(unittest.TestCase):

    def test_read_hdf(self):
        # verifies that HDF can be loaded, has the expected keys, each key is a
        # vector of the right length, and there are no NaNs
        data = gridvel.read_hdf("data/103001004177AE00_1030010043C01400.h5")
        for key in ("x", "y", "dx", "dy", "mx", "my", "strength"):
            self.assertTrue(key in data)
            self.assertEqual(len(data[key]), 181425)
            self.assertEqual(np.sum(np.isnan(data[key])), 0)

    def test_fillnodata(self):
        x, y = np.meshgrid(np.arange(100), np.arange(100))
        data = np.sin(x/3) + np.cos(y/2) * np.cos(x/5)
        np.random.seed(49)
        msk = (np.random.random(10000) > 0.2).reshape([100, 100])
        data_sparse = data.copy()
        data_sparse[msk] = np.nan
        grid = karta.RegularGrid([-5, -5, 10, 10, 0, 0], values=data_sparse)
        gridvel.fillnodata(grid)

        error = np.nansum(np.abs(grid[:,:] - data)) / data.size
        self.assertTrue(error < 0.03)

    def test_choose_transform1(self):
        # basic usage
        x, y = np.meshgrid(np.arange(24), np.arange(24))
        dx = 5.0
        dy = 5.0
        transform = gridvel.choose_transform(x, y, dx, dy)
        self.assertEqual([-2.5, -2.5, 5.0, 5.0, 0.0, 0.0], transform)

    def test_choose_transform2(self):
        # shift origin
        x, y = np.meshgrid(np.arange(24), np.arange(24))
        x -= 10
        y -= 15
        dx = 5.0
        dy = 5.0
        transform = gridvel.choose_transform(x, y, dx, dy)
        self.assertEqual([-12.5, -17.5, 5.0, 5.0, 0.0, 0.0], transform)

    def test_choose_transform3(self):
        # use different dx/dy
        x, y = np.meshgrid(np.arange(24), np.arange(24))
        dx = 2.0
        dy = 7.0
        transform = gridvel.choose_transform(x, y, dx, dy)
        self.assertEqual([-1.0, -3.5, 2.0, 7.0, 0.0, 0.0], transform)

if __name__ == "__main__":
    unittest.main()

