import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
from wvutil import ensureisdir

INPUTS = "dems-10m-aligned/"
OUTPUTS = "hillshades-10m-aligned/"
OVERWRITE = True

ensureisdir(OUTPUTS)

def _call_cmd(cmd):
    return subprocess.call(cmd)

with ThreadPoolExecutor(max_workers=4) as pool:

    for fnm in (f for f in os.listdir(INPUTS) if f.endswith(".tif")):

        fnmout = os.path.join(OUTPUTS, fnm)
        if os.path.isfile(fnmout) and not OVERWRITE:
            print("skipping %s" % fnmout)
        else:
            cmd = ["gdaldem", "hillshade",
                   "-co", "COMPRESS=LZW", "-co", "TILED=YES",
                   os.path.join(INPUTS, fnm), fnmout]
            print(" ".join(cmd))
            pool.submit(_call_cmd, cmd)
