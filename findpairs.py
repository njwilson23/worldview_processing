#! /usr/bin/env python

import argparse
import itertools
import karta
import numpy as np
import os
import shapely.geometry
from wvutil import get_sceneid, get_date, get_region, get_geometry

def reproject(geom, crs):
    return type(geom)(geom.get_vertices(crs=crs),
                      properties=geom.properties, crs=crs)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Find scene pairs matching criteria")
    parser.add_argument("scenes", nargs="+")
    parser.add_argument("--region", action="store", default="", type=str)
    parser.add_argument("--polygon", action="store", default="", type=str)
    parser.add_argument("--max-days", action="store", default=99999, type=int)
    parser.add_argument("--min-days", action="store", default=0, type=int)
    parser.add_argument("--min-overlap-area", action="store", default=0, type=float)
    parser.add_argument("--pyfmt", action="store_true", default=False)
    parser.add_argument("--debug-max-comparisons", action="store", default=-1, type=int)
    args = parser.parse_args()

    sceneids = set(map(get_sceneid, args.scenes))

    if len(args.polygon) != 0:
        poly_k = karta.vector.read_geojson(args.polygon, crs=karta.crs.NSIDCNorth)[0]
        poly = shapely.geometry.shape(poly_k.__geo_interface__["geometry"])
    else:
        poly = None

    if len(args.region) != 0:
        sceneids = [s for s in sceneids if get_region(s) == args.region]
    else:
        sceneids = list(sceneids)

    sceneids.sort(key=get_date)

    matches = []
    nmatches = 0
    if args.pyfmt:
        print("[", end="")
    for i, comb in enumerate(itertools.combinations(sceneids, 2)):
        if i == args.debug_max_comparisons:
            break

        scid1, scid2 = comb
        if get_date(scid1) > get_date(scid2):
            scid1, scid2 = scid2, scid1
        dtdays = (get_date(scid2)-get_date(scid1)).days

        if (args.min_days <= dtdays <= args.max_days):

            gk1 = karta.vector.read_geojson(get_geometry(scid1))[0]
            gk2 = karta.vector.read_geojson(get_geometry(scid2))[0]
            gk1.crs = karta.crs.LonLatWGS84
            gk2.crs = karta.crs.LonLatWGS84

            gs1 = shapely.geometry.shape(reproject(gk1, karta.crs.NSIDCNorth).__geo_interface__["geometry"])
            gs2 = shapely.geometry.shape(reproject(gk2, karta.crs.NSIDCNorth).__geo_interface__["geometry"])

            overlap = gs1.intersection(gs2)
            if poly is not None:
                overlap = poly.intersection(overlap)

            overlap_area = overlap.area
            if overlap.area >= args.min_overlap_area:
                nmatches += 1
                d1 = get_date(scid1)
                d2 = get_date(scid2)
                if args.pyfmt:
                    print("    ('{0}', '{1}'),".format(scid1, scid2))
                else:
                    print("{0:4d} {1} {2}    {3} {4} {5:4d} {6:6.1f}".format(
                            nmatches, scid1, scid2,
                            d1.strftime("%Y-%m-%d"), d2.strftime("%Y-%m-%d"),
                            dtdays, overlap.area/1e6))
                matches.append((scid1, scid2))
    if args.pyfmt:
        print("]")

print("{0} matches found".format(nmatches))
