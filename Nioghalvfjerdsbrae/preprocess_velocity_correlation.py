
import os
import subprocess
import karta
from concurrent.futures import ThreadPoolExecutor
from find_scene_pairs import find_scene_pairs


pairs = find_scene_pairs()

# For each pair:
#   - create a directory "vel_DATE1_DATE2_ID1_ID2"
#   - chop the scenes and put a version in the new directory
#   - run aspvel?

def get_datestr(dirname):
    return os.path.split(dirname)[-1].split("_")[2]

def get_idstr(dirname):
    return os.path.split(dirname)[-1].split("_")[3]

new_dirs = []

for pair in pairs:
    dir1, dir2 = pair[:2]
    newdir = "vel_{date1}_{date2}_{id1}_{id2}".format(date1=get_datestr(dir1),
                                                      date2=get_datestr(dir2),
                                                      id1=get_idstr(dir1),
                                                      id2=get_idstr(dir2))
    if not os.path.isdir(newdir):
        print("creating %s" % newdir)
        os.makedirs(newdir)

    new_dirs.append(newdir)

################ SCENE CHOPPING ###################

def chop_scene(dirname, bbox, outdir):
    outfnm = os.path.join(outdir, get_datestr(dirname)+".tif")

    cmd = ["gdalwarp", "-overwrite",  "-q",
            "-co", "COMPRESS=PACKBITS",
            "-te", str(bbox[0]), str(bbox[1]), str(bbox[2]), str(bbox[3]),
            os.path.join(dirname, "projL-clip.tif"), outfnm]

    retcode = subprocess.call(cmd)
    if retcode != 0:
        print("Warning: thread failed with code %d" % retcode)
    return retcode

with ThreadPoolExecutor(max_workers=4) as pool:
    for pair, outdir in zip(pairs, new_dirs):
        pool.submit(chop_scene, pair[0], pair[2], outdir)
        pool.submit(chop_scene, pair[1], pair[2], outdir)

