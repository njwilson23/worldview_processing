#! /usr/bin/env python
#
# Script to (somewhat) efficiently convert an image to compressed 8-bit
#
# Reasons to do this:
# - Less disk space (~%15)
# - imcorr only works with 8 bit imagery anyway
#
# Usage convert_to_byte.py <input.tif> <output.tif>

import numpy as np
import osgeo.gdal
import osgeo.gdalconst as gc
import sys

osgeo.gdal.UseExceptions()

def getchunks(nx, ny, chunksize=(512, 512), overlap=(0, 0)):
    """ Generator for rectangular chunk bounds from an image """
    x = 0
    y = 0
    i = 0
    while True:
        i += 1
        dx = min(chunksize[0], nx-x+overlap[0])
        dy = min(chunksize[1], ny-y+overlap[1])
        yield (x, x+dx, y, y+dy)
        x += dx - overlap[0]
        if x == nx:
            x = 0
            y += dy - overlap[1]
        if y == ny:
            break

dataset = osgeo.gdal.Open(sys.argv[1], gc.GA_ReadOnly)
band = dataset.GetRasterBand(1)
vmem = band.GetVirtualMemArray()

# Write tiles because working in blocks - avoid writing partial tiles
driver = osgeo.gdal.GetDriverByName("GTiff")
output = driver.Create(sys.argv[2], dataset.RasterXSize, dataset.RasterYSize, 1,
                       osgeo.gdal.GDT_Byte, ["COMPRESS=PACKBITS", "TILED=YES"])

output.SetGeoTransform(dataset.GetGeoTransform())
output.SetProjection(dataset.GetProjection())
output_band = output.GetRasterBand(1)

nodata = band.GetNoDataValue()
output_band.SetNoDataValue(255)

band.ComputeStatistics(True)
rmin = band.GetMinimum()
rmax = band.GetMaximum()

# Set chunk size to the a multiple of the GTiff tile size (default 256)
for chunk in getchunks(output.RasterXSize, output.RasterYSize, chunksize=(256*30, 256*30)):
    print(chunk)
    arr = band.ReadAsArray(chunk[0], chunk[2], chunk[1]-chunk[0], chunk[3]-chunk[2])
    arr8 = np.round(254*(arr-rmin)/(rmax-rmin)).astype(np.uint8)
    arr8[arr==nodata] = 255
    output_band.WriteArray(arr8, chunk[0], chunk[2])

output_band = None
output = None
