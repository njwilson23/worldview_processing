# README

This directory contains results from Worldview image processing, as well as a
collection of scripts. The data and processing steps are described below.


## Scripts

`asprun.py`, `aspvel.py`

(see separate *asputil* documentation)

`gen_yaml.py`

Autogenerates configuration files for `asprun.py`

`convert_to_byte.py`

Converts an image to uint8 for use by imcorr.

`clip_projected_images.sh`

Uses `convert_to_byte.py` and `gdalwarp` to convert all projected (left) images
to uint8 clipped by the Nioghalvfjerdsbrae outline.

`get_clipped_bbox.py`

Crudely estimates the minimum bounding box to contain non-null points clipped by
a shapefile.

`find_scene_pairs.py`

Searches all results to find pairs of images that overlap and are within some
temporal baseline of each other. These images may be useful for velocity
extraction.

`preprocess_velocity_correlation.py`

Uses gdalwarp to clip overlapping sections of projected images, ready for
velocity correlation. Imports `find_scene_pairs.py` as a module.

`make_all_velocity.py`

Quick a dirty script that goes into each folder named `vel_#####` and runs
aspvel.py to generate a set of velocity components. Essentially does what a
Makefile could do, but it was taking me too long to figure out all of the
necessary text transformations.

# Data

`results_WV*`

Contains outputs of ASP stereo pipeline for Worldview image pairs

