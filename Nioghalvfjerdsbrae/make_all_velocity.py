#! /usr/bin/env python

import os
import re
import datetime
import subprocess

from os import getcwd
from os.path import isfile, basename

def get_date(s):
    return datetime.datetime(int(s[:4]), int(s[4:6]), int(s[6:8]))

velocity_dirs = list(filter(lambda s: s.startswith("vel_"), os.listdir(getcwd())))
velocity_dirs.sort()

for directory in velocity_dirs:
    os.chdir(directory)

    try:

        print("Working in %s" % basename(getcwd()))

        scenes = list(filter(lambda s: s.endswith(".tif") and \
                                       re.match("[0-9]{8}.*", s),
                             os.listdir(getcwd())))
        if len(scenes) != 2:
            msg = "\n".join(["input scenes ambiguous"] + scenes)
            raise IOError(msg)

        scenes.sort()

        # Correlation
        if not isfile("res_all.h5"):
            ndays = (get_date(scenes[1]) - get_date(scenes[0])).days
            ret = subprocess.call(["aspcorr.py", scenes[0], scenes[1], str(ndays)])
            if ret != 0:
                raise RuntimeError("CORRELATION FAILURE: %s" % basename(getcwd()))

        # Masking
        if not isfile("mask.tif"):
            ret = subprocess.call(["create_mask.py"] + scenes + ["mask.tif"])
            if ret != 0:
                raise RuntimeError("MASK FAILURE: %s" % basename(getcwd()))

        # Interpolation
        if (not isfile("uvel.tif")) or (not isfile("vvel.tif")):
            cmd = ["vel_interpolate.py", "res_all.h5", "uvel.tif", "vvel.tif",
                    "-m", "mask.tif", "-g", "mask.tif"]
            ret = subprocess.call(cmd)
            if ret != 0:
                raise RuntimeError("INTERPOLATION FAILURE: %s" % basename(getcwd()))

    except RuntimeError as e:
        print(e)

    finally:
        os.chdir("../")

