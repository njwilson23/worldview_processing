import numpy as np
import karta
import re
import datetime
import os

def data_bbox(scene, nodata=None):
    """ Determine the minimum bbox in scene that contains all non-nodata pixels
    """
    if nodata is None:
        nodata = scene.nodata

    # define a function to identify nodata
    if np.isnan(nodata):
        isnodata = np.isnan
    else:
        def isnodata(a):
            return a == nodata

    dx, dy = scene.transform[2:4]
    x0 = x1 = scene.transform[0] + 0.5*dx
    y0 = y1 = scene.transform[1] + 0.5*dy
    ny, nx = scene.size

    # Assumes that Tiffs have row blocks (usually true)
    # Reading a row is fast, so process from bottom to top
    bottom = True
    left_bound = []
    right_bound = []

    step = 50      # go half a kilometer at a time
    for i in range(0, ny, step):
        row = scene.values[i,:]
        row_nodata = isnodata(row)

        if np.all(row_nodata):
            if bottom:
                y0 += dy*step
                y1 += dy*step
        else:
            bottom = False
            y1 = scene.transform[1]+(i+0.5)*dy

            idx = np.argwhere(~row_nodata)
            left_bound.append(idx[0][0])
            right_bound.append(idx[-1][0])

    # minus dx from left and plus dx on right
    x0 = (min(left_bound)-0.5)*dx + scene.transform[0]
    x1 = (max(right_bound)+1.5)*dx + scene.transform[0]
    return (x0, y0, x1, y1)

def test_data_bbox():
    scene = karta.read_geotiff("results_WV01_20110716_1020010015509A00_1020010014"
                             "C80C00/projL-clip.tif", in_memory=False)
    bbox = data_bbox(scene, 255)
    print(bbox)
    print(scene.bbox)

def overlap_area(bbox1, bbox2):
    """ Return the area of the overlap between two bboxes. If a dimension
    doesn't overlap, return 0.0. """
    x0 = max(bbox1[0], bbox2[0])
    x1 = min(bbox1[2], bbox2[2])
    y0 = max(bbox1[1], bbox2[1])
    y1 = min(bbox1[3], bbox2[3])
    area = max(0.0, x1-x0) * max(0.0, y1-y0)
    return (x0, y0, x1, y1), area

def test_overlap():
    assert overlap_area((0, 0, 1, 1), (0.5, 0.5, 1.5, 1.0)) == 0.25
    assert overlap_area((0, 0, 1, 1), (2, 2, 3, 3)) == 0.0
    assert overlap_area((0, 0, 1, 1), (0.5, 2, 1.5, 3)) == 0.0

def get_date(dirname):
    match = re.search("_[0-9]{8}_", dirname)
    if not match:
        raise RuntimeError("couldn't get a date from %s" % dirname)
    s = dirname[match.span()[0]+1:match.span()[1]-1]
    return datetime.date(int(s[:4]), int(s[4:6]), int(s[6:8]))

def test_get_date():
    dirname = "results_WV02_20130726_1030010024723400_103001002483B000"
    print(get_date(dirname))

def get_datespan(dir1, dir2):
    return get_date(dir2)-get_date(dir1)

def test_get_datespan():
    print(get_datespan("results_WV02_20130726_1030010024723400_103001002483B000",
                       "results_WV02_20120730_103001001BC5CD00_103001001A9FAA00"))

def count_shared_pixels(scene1, scene2, bbox1=None, bbox2=None):
    if bbox1 is None:
        bbox1 = data_bbox(scene1)
    if bbox2 is None:
        bbox2 = data_bbox(scene2)

    nodata = scene1.nodata
    if np.isnan(nodata):
        isnodata = np.isnan
    else:
        def isnodata(a):
            return a == nodata

    assert(isnodata(scene2.nodata))         # require scenes to use same nodata

    # Calculate the bbox of overlap
    bbox = [max(bbox1[0], bbox2[0]), max(bbox1[1], bbox2[1]),
            min(bbox1[2], bbox2[2]), min(bbox1[3], bbox2[3])]

    # Calculate the bounds of data pixels for each image
    idx_bounds1 = (scene1.get_indices(bbox[0], bbox[1]),
                   scene1.get_indices(bbox[2], bbox[3]))
    idx_bounds2 = (scene2.get_indices(bbox[0], bbox[1]),
                   scene2.get_indices(bbox[2], bbox[3]))

    # Choose an appropriate number of rows to read as a time
    nx = idx_bounds1[1][1] - idx_bounds1[0][1]
    ny = idx_bounds1[1][0] - idx_bounds1[0][0]

    if nx != (idx_bounds2[1][1] - idx_bounds2[0][1]):
        print("WARNING: incompatible swath sizes in count_shared_pixels")

    bandwidth = 1000000 // nx      # assuming 4-byte numbers and 400 mb memory use (conservative)
    pixel_count = 0
    i = 0
    while i != ny:
        inext = min(ny, i+bandwidth)

        swath1 = scene1.values[idx_bounds1[0][0]+i:idx_bounds1[0][0]+inext,
                               idx_bounds1[0][1]:idx_bounds1[1][1]]

        swath2 = scene2.values[idx_bounds2[0][0]+i:idx_bounds2[0][0]+inext,
                               idx_bounds2[0][1]:idx_bounds2[1][1]]
        i = inext

        pixel_count += np.sum((~isnodata(swath1)) & (~isnodata(swath2)))

    return pixel_count

def test_count_shared_pixels():
    s1 = karta.read_geotiff("results_WV01_20130522_1020010024638D00_1020010022299900/projL-clip.tif", in_memory=False)
    s2 = karta.read_geotiff("results_WV01_20130729_1020010024AA0300_102001002386E900/projL-clip.tif", in_memory=False)

    bb1 = data_bbox(s1)
    bb2 = data_bbox(s2)

    npixels = count_shared_pixels(s1, s2, bbox1=bb1, bbox2=bb2)
    print(npixels)
    return

def get_worldview_dirs():
    # Get a list of directories containing projected WorldView scenes
    directories = []
    for dirpath, dirnames, filenames in os.walk(os.getcwd()):
        for fnm in filter(lambda s: s=="projL-clip.tif", filenames):
            directories.append(dirpath)
    return directories

def find_scene_pairs():

    directories = get_worldview_dirs()

    # Find potential pairs with a baseline of 2-90 days
    # finds 20 matches at 90 or 120 days
    # can find 24 at 360 days
    potential_pairs = [(dir1, dir2) for dir1 in directories
                                    for dir2 in directories
                        if 2 <= get_datespan(dir1, dir2).days <= 360]

    print("Found %d matches within max temporal baseline" % len(potential_pairs))

    # Check potential pairs to find images that overlap
    # Start by checking data bounding boxes of potential matches
    # If sufficiently overlapping, count the shared data pixels between scenes
    overlapping_pairs = []

    for dir1, dir2 in potential_pairs:
        scene1 = karta.read_geotiff(os.path.join(dir1, "projL-clip.tif"), in_memory=False)
        bbox1 = data_bbox(scene1, scene1.nodata)

        scene2 = karta.read_geotiff(os.path.join(dir2, "projL-clip.tif"), in_memory=False)
        bbox2 = data_bbox(scene2, scene2.nodata)

        overlap_bbox, area = overlap_area(bbox1, bbox2)

        if area > 1e8:
            # Test whether data pixels overlap
            npixels = count_shared_pixels(scene1, scene2, bbox1=bbox1, bbox2=bbox2)

            if npixels > 1e6:
                overlapping_pairs.append((dir1, dir2, overlap_bbox, area))

        del scene1, scene2

    print("Reduced to %d overlapping matches" % len(overlapping_pairs))
    for pair in overlapping_pairs:
        print("{0} - {1} : {2}".format(os.path.split(pair[0])[-1],
                                       os.path.split(pair[1])[-1], pair[3]))

    return overlapping_pairs

if __name__ == "__main__":
    #test_overlap()
    #test_data_bbox()
    #test_get_date()
    #test_get_datespan()
    #test_count_shared_pixels()
    find_scene_pairs()
