#! /usr/bin/env python
#
# Given a raster and a shapefiles, estimate the minimum bounding box guaranteed
# to contain all non-null pixels clipped by the shapefile. That is, return the
# size the clipped array with the null pixels removed.
#

import sys
import karta
import numpy as np

try:
    raster = sys.argv[1]
    shpfile = sys.argv[2]
except IndexError:
    sys.stderr.write("Usage: get_clipped_bbox <GeoTiff> <shapefile>\n")
    sys.exit(1)

shp = karta.read_shapefile(shpfile)[0]

if not isinstance(shp, karta.Polygon):
    sys.stderr.write("Error: shapefile doesn't contain a polygon\n")
    sys.exit(1)

grid = karta.read_geotiff(raster)
grid.values = grid.values[::100,::100]
grid._transform = (grid.transform[0], grid.transform[1], grid.transform[2]*100,
                   grid.transform[3]*100, grid.transform[4], grid.transform[5])

clipped = grid.mask_by_poly(shp)
del grid

X, Y = clipped.center_coords()
msk = np.isnan(clipped.values) | (clipped.values == clipped.nodata) | (clipped.values == 0.0)
xmn = np.min(X[~msk]) - 100*clipped.transform[2]
xmx = np.max(X[~msk]) + 100*clipped.transform[2]
ymn = np.min(Y[~msk]) - 100*clipped.transform[3]
ymx = np.max(Y[~msk]) + 100*clipped.transform[3]

sys.stdout.write("%d %d %d %d\n" % (np.floor(xmn), np.floor(ymn), np.ceil(xmx), np.ceil(ymx)))
