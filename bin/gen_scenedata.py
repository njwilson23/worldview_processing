#! /usr/bin/env
#
# This script infers the WorldView image filename patterns within a list of
# directories, and prints to stdout a list of triples:
#
#   dirname     left_pattern    right_pattern

from __future__ import print_function, division
import argparse
import math
import os

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("directories", nargs="+", action="store")
    args = parser.parse_args()

    directories = []
    for name in args.directories:
        if os.path.isdir(name):
            directories.append(name)
        else:
            print("# ignoring '{0}' because it is not a directory".format(name))

    for dirname in directories:

        _, _, scid1, scid2 = os.path.split(dirname)[1].split("_")
        filelist = filter(lambda f: f.endswith(".ntf"), os.listdir(dirname))
        group1 = []
        group2 = []
        for f in filelist:
            if scid1 in os.path.split(f)[1]:
                group1.append(f)
            elif scid2 in os.path.split(f)[1]:
                group2.append(f)
            else:
                print("# unknown third filename".format(f))

        patterns = []
        for group in (group1, group2):
            pattern_vec = []
            for charset in zip(*group):
                if len(set(charset)) == 1:
                    pattern_vec.append(charset[0])
                else:
                    pattern_vec.append("*")
            patterns.append("".join(pattern_vec))

        if patterns[0] == patterns[1]:
            print("# error - patterns identical ({0})".format(os.path.split(dirname)[1]))
        else:
            print(os.path.split(dirname)[1], patterns[0], patterns[1])

