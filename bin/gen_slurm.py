#! /usr/bin/env python

import os
import re

template = """#! /bin/bash
#SBATCH --partition=sched_mit_hill,newnodes
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=8000
#SBATCH --time=0-12:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=njwilson@mit.edu
#SBATCH --job-name={0}

asprun.py --clean-after {1}
"""

def filter_configs(fnm):
    return fnm.endswith(".yaml") and fnm.startswith("WV")

def get_sceneid(fnm):
    m = re.search("[A-Z0-9]{16}", fnm)
    if m is None:
        raise ValueError("cannot parse scene id from %s" % fnm)
    else:
        return fnm[m.start():m.end()]

for fnm in filter(filter_configs, os.listdir(".")):

    sceneid = get_sceneid(fnm)
    slurmfnm = fnm.replace(".yaml", ".slurm")
    with open(slurmfnm, "w") as f:
        f.write(template.format(sceneid, fnm))
    print(slurmfnm)
