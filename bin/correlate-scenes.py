#! /usr/bin/env python
""" Use an image correlator to determine matching chips between paired scenes. """

from __future__ import print_function

import argparse
import os
import sys
import h5py
import karta
import numpy as np
import meltpack

def compare_with_velocities(uvel, vvel, pts, displ, dt):
    """ Compute expected displacement at *pts* and return the difference from
    *displ* """
    x = pts[:,0]
    y = pts[:,1]

    xmin, xmax, ymin, ymax = uvel.extent
    mask = (x > xmin) & (x < xmax) & (y > ymin) & (y < ymax)
    x = x[mask]
    y = y[mask]

    dx_obs = displ[mask,0]
    dy_obs = displ[mask,1]
    dx = uvel.sample(x, y)*dt
    dy = vvel.sample(x, y)*dt
    out = np.nan*np.zeros(displ.shape)
    out[mask,0] = dx_obs-dx
    out[mask,1] = dy_obs-dy
    return out

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="compute offsets between two "
        "images using a deformation field as a guess")
    parser.add_argument("image1", type=str, action="store",
            help="First satellite image")
    parser.add_argument("image2", type=str, action="store",
            help="Second satellite image")
    parser.add_argument("uvel", type=str, action="store",
            help="Horizontal velocity/deformation field used to provide initial"
                 " estimates and access mismatch")
    parser.add_argument("vvel", type=str, action="store",
            help="Vertical velocity/deformation field used to provide initial"
                 " estimates and access mismatch")
    parser.add_argument("output", type=str, action="store",
            help="Name of the output HDF5 dataset")
    parser.add_argument("--baseline", type=float, default=1.0, action="store",
            metavar="DAYS", help="Temporal baseline between scenes in days")
    parser.add_argument("--resolution", action="store", nargs=2, metavar=("DX", "DY"),
            default=(20.0, 20.0), type=float,
            help="Resolution of output correlations")
    parser.add_argument("--threads", action="store", default=4, type=int,
            metavar="N", help="Number of worker threads")
    parser.add_argument("--search-size", action="store", nargs=2, metavar=("NY", "NX"),
            default=(256, 256), type=int, help="Size of search window")
    parser.add_argument("--ref-size", action="store", nargs=2,
            default=(64, 64), type=int, metavar=("NY", "NX"),
            help="Size of reference window (1/2 search window works well)")
    parser.add_argument("--overwrite", default=False, action="store_true",
            help="Permit existing files to be overwritten")

    args = parser.parse_args()

    if os.path.isfile(args.output) and not args.overwrite:
        print("{0} already exists - aborting".format(args.output))
        sys.exit(1)

    SimpleBand = karta.raster.band.SimpleBand
    uvel = karta.read_geotiff(args.uvel, bandclass=SimpleBand)
    vvel = karta.read_geotiff(args.vvel, bandclass=SimpleBand)
    sc1 = karta.read_geotiff(args.image1, bandclass=SimpleBand)
    sc2 = karta.read_geotiff(args.image2, bandclass=SimpleBand)

    dt = args.baseline / 365.0
    pts, dspl, strength = meltpack.correlate.correlate_scenes(
            sc1, sc2, uvel, vvel, dt,
            refsize=tuple(args.ref_size),
            searchsize=tuple(args.search_size),
            nprocs=args.threads,
            resolution=(args.resolution[0], args.resolution[1]))
    mismatch = compare_with_velocities(uvel, vvel, pts, dspl, dt)

    options = {"compression": "gzip"}
    ds = h5py.File(args.output, mode="w")
    try:
        ds.attrs["id1"] = args.image1
        ds.attrs["id2"] = args.image2
        ds.attrs["crs"] = sc1.crs.get_proj4()

        ds.create_dataset("x", data=pts[:,0].astype(np.float32), **options)
        ds.create_dataset("y", data=pts[:,1].astype(np.float32), **options)
        ds.create_dataset("dx", data=dspl[:,0].astype(np.float32), **options)
        ds.create_dataset("dy", data=dspl[:,1].astype(np.float32), **options)
        ds.create_dataset("strength", data=strength.astype(np.float32), **options)
        ds.create_dataset("mismatch_x", data=mismatch[:,0].astype(np.float32), **options)
        ds.create_dataset("mismatch_y", data=mismatch[:,1].astype(np.float32), **options)
    finally:
        ds.close()

