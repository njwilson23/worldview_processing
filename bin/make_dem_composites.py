#! /usr/bin/env python
import os
import numpy as np
import karta
from wvutil import get_region, get_sceneid, ensureisdir, is_blacklisted

RES = 128
DEMS = "../dems-{0}m-autoaligned/".format(RES)
OUTDIR = "../dem-composites-{0}m/".format(RES)
OVERWRITE = True

ensureisdir(OUTDIR)

# sort dems by region
regions = {}

# def union_bbox(grids):
#     bbs = [g.bbox for g in grids]
#     return (min(b[0] for b in bbs),
#             min(b[1] for b in bbs),
#             max(b[2] for b in bbs),
#             max(b[3] for b in bbs))
# 
# def fix_bbox(bbox, resolution):
#     left = bb[0]-(bb[0]%resolution)
#     right = bb[1]-(bb[1]%resolution) + resolution
#     bottom = bb[2]-(bb[2]%resolution)
#     top = bb[3]-(bb[3]%resolution) + resolution
#     return (left, bottom, right, top)

def fix_transform_nearest_neighbour(T, resolution):
    left = T[0]-(T[0]%resolution)
    bottom = T[1]-(T[1]%resolution)
    return (left, bottom, T[2], T[3], T[4], T[5])

for fnm in os.listdir(DEMS):
    if fnm.endswith(".tif") and not is_blacklisted(get_sceneid(fnm)):
        reg = get_region(get_sceneid(fnm))
        regions.setdefault(reg, [])
        regions[reg].append(fnm)

for reg, filelist in regions.items():

    outfnm = os.path.join(OUTDIR, "{0}_composite_20161016.tif".format(reg.lower()))
    print(reg, outfnm)
    if not os.path.isfile(outfnm) or OVERWRITE:
        grids = [karta.read_geotiff(os.path.join(DEMS, fnm)) for fnm in filelist]
        for g in grids:
            g.apply(lambda a: np.where(a < -1000, g.nodata, a), inplace=True)
            g._transform = fix_transform_nearest_neighbour(g.transform, RES)
        composite = karta.raster.merge(grids)
        composite.to_geotiff(outfnm)
    else:
        print("  skipping")
