#! /usr/bin/env python
import argparse
import h5py
import karta
import meltpack
import numpy as np
import os
import scipy.interpolate
import scipy.ndimage
import sys

def read_hdf(fnm):
    """ Read an HDF5 dataset filled with correlation data (as created by
    compute_correlation_offsets.py) and return a dictionary containing
    observation coordinates, displacements, and mismatch estimates. """
    with h5py.File(fnm, mode='r') as ds:
        x = ds['x'].value
        y = ds['y'].value
        dx = ds['dx'].value
        dy = ds['dy'].value
        mx = ds['mismatch_x'].value
        my = ds['mismatch_y'].value
        try:
            strength = ds['strength'].value
        except KeyError:
            strength = 1000*np.ones_like(x)
    msk = np.isnan(x) | np.isnan(y) | np.isnan(dx) | np.isnan(dy) | np.isnan(mx) | np.isnan(my)
    return {"x": x[~msk], "y": y[~msk],
            "dx": dx[~msk], "dy": dy[~msk],
            "mx": mx[~msk], "my": my[~msk],
            "strength": strength[~msk]}

def fillnodata(grid, where=None):
    """ interpolate over nodata. operate IN PLACE!

    the boolean mask *where* can be provided to limit where hole-filling occurs
    (default true everywhere)
    """
    X, Y = grid.coordmesh()
    if where is None:
        where = np.ones(X.shape, dtype=np.bool)
    m = where & (~grid.data_mask)       # place at which to interpolate
    fint = scipy.interpolate.CloughTocher2DInterpolator(np.c_[X[~m], Y[~m]], grid[~m])
    grid[m] = fint(X[m], Y[m])
    return grid

def erode_data(grid, **kwargs):
    """ erode grid data mask. operate IN PLACE!
    """
    msk = scipy.ndimage.morphology.binary_erosion(grid.data_mask, **kwargs)
    grid[~msk] = grid.nodata
    return grid

def choose_transform(x, y, dx, dy):
    """ from a set of coodinates, choose a reasonable geotransform """
    T = [(x.min()//dx)*dx-0.5*dx, (y.min()//dy)*dy-0.5*dy, dx, dy, 0.0, 0.0]
    return T

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=
            """Consolidate correlation data to construct a deformation map.""")
    parser.add_argument("corr_offsets", type=str,
            help="Input file (HDF5)")
    parser.add_argument("prefix", type=str,
            help="Output file prefix (GeoTiff)")
    parser.add_argument("-tr", type=float, nargs=2, metavar=('DX', 'DY'),
            default=(100.0, 100.0), required=False,
            help="Output grid resolution")
    parser.add_argument("--days", type=float, default=365.0,
            help="Elapsed time represented by correlation vectors. If not "
                 "provided, the gridded representation of the correlation "
                 "displacements is returned.")
    parser.add_argument("--polymask", type=str, default="",
            help="Polygon (shapefile or GeoJSON) beyond which output will be "
                 "masked.")
    parser.add_argument("--smooth-iterations", type=int, default=3,
            help="Number of iterations of a 3x3 smoothing filter applied to "
                 "observations.")
    parser.add_argument("--median-iterations", type=int, default=2,
            help="Number of iterations of a 3x3 median filter applied to "
                 "observations.")
    parser.add_argument("--maximum-mismatch-factor", type=float, default=0.5,
            help="Maximum mismatch in terms of a factor times the measured "
                 "displacement")
    parser.add_argument("--overwrite", action="store_true", default=False)

    args = parser.parse_args()

    outdx = "{0}_dx.tif".format(args.prefix)
    outdy = "{0}_dy.tif".format(args.prefix)
    if os.path.isfile(outdx) and os.path.isfile(outdy) and (not args.overwrite):
        print("{0} and {1} already exist".format(outdx, outdy))
        sys.exit(1)

    dx, dy = args.tr
    indata = read_hdf(args.corr_offsets)
    f = args.maximum_mismatch_factor
    mismatch = np.sqrt(indata["mx"]**2 + indata["my"]**2)
    displacement = np.sqrt(indata["dx"]**2 + indata["dy"]**2)
    msk = ((mismatch > f*displacement) & (mismatch > 10.0)) | (indata["strength"] < 2.0)

    if np.sum(~msk) < 100:
        print("too few valid correlations ({0})".format(np.sum(~msk)))
        sys.exit(1)

    # Grid data
    T = choose_transform(indata["x"], indata["y"], dx, dy)
    grid_dx = karta.raster.gridpoints(indata["x"][~msk], indata["y"][~msk],
                                      indata["dx"][~msk]*365.0/args.days, T,
                                      karta.crs.NSIDCNorth)
    grid_dy = karta.raster.gridpoints(indata["x"][~msk], indata["y"][~msk],
                                      indata["dy"][~msk]*365.0/args.days, T,
                                      karta.crs.NSIDCNorth)
    grid_str = karta.raster.gridpoints(indata["x"][~msk], indata["y"][~msk],
                                       indata["strength"][~msk]*365.0/args.days, T,
                                       karta.crs.NSIDCNorth)
    grid_str[~grid_str.data_mask] = -1.0

    # Filter data
    for i in range(args.median_iterations):
        grid_dx[:,:] = meltpack.filt.medianfilt(grid_dx[:,:])
        grid_dy[:,:] = meltpack.filt.medianfilt(grid_dy[:,:])

    if args.smooth_iterations > 0:
        smiter = args.smooth_iterations
        grid_dx[:,:] = meltpack.filt.smooth5(grid_dx[:,:], smiter)
        grid_dy[:,:] = meltpack.filt.smooth5(grid_dy[:,:], smiter)

    # Fill holes
    grid_dx = fillnodata(grid_dx)
    grid_dy = fillnodata(grid_dy)

    # Erode edges
    stx = np.array([[0, 1, 0],
                    [1, 1, 1],
                    [0, 1, 0]], dtype=np.bool)
    grid_dx = erode_data(grid_dx, structure=stx, iterations=4)
    grid_dy = erode_data(grid_dy, structure=stx, iterations=4)

    # Mask
    if len(args.polymask) != 0:
        if args.polymask.endswith("json"):
            poly = karta.read_geojson(args.polymask)[0]
        elif args.polymask.endswith(".shp"):
            poly = karta.read_shapefile(args.polymask)[0]
        else:
            raise ValueError("POLYMASK filetype unknown: {0}".format(args.polymask))
        grid_dx.mask_by_poly(poly, inplace=True)
        grid_dy.mask_by_poly(poly, inplace=True)

    grid_dx.to_geotiff(outdx)
    grid_dy.to_geotiff(outdy)
