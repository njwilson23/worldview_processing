#! /usr/bin/env python
"""
This script takes CryoSat transits in CSV form and attempts to compute
corrections to raw WorldView DEMs (via pc_align). The transform data and the
transformed point clouds are written to disk.

Parameters
----------
INPUTS : directory
    The DEMs to transform
STABLE_REFERENCE_DATA : file
    The stable (e.g. bedrock) positions to use as a reference. All of this data
    within the DEM bounds will be used.
TRANSIENT_REFERENCE_DATA : directory
    The CSV reference data for overflights over floating ice. This data will be
    limited by a maximum temporal offset MAX_DAYS (see below).
    A tide correction will be applied to make the data match the tidal phase of
    the DEM under consideration.
OUTPUTS : directory
    Where to place transformation files and transformed DEMs
REGIONS : set of region names
    Regions to consider (e.g. {'Nioghalvfjerdsbrae', 'Petermann', 'Ryder'})
MAX_DAYS : int
    Maximum number of days a reference point can be away from the DEM date
OVERWRITE : bool
    Whether to overwrite existing results
"""

from __future__ import print_function
import os
import re
import json
import datetime
import subprocess
import traceback
import shutil
import numpy as np
from scipy.interpolate import interp1d

import karta
from wvutil import (get_sceneid,
                     get_region,
                     get_date,
                     get_geometry,
                     get_best_outline,
                     get_tidal_height_by_region,
                     ensureisdir,
                     timestamp,
                     tz_utc)

INPUTS = "/nobackup1/natw/dems"
STABLE_REFERENCE_DATA = "/nobackup1/natw/cryosat-stable/petermann_nonice.csv"
TRANSIENT_REFERENCE_DATA = "/nobackup1/natw/cryosat-transects/petermann"
OUTPUTS = "/nobackup1/natw/cryosat-alignments"
REGIONS = {"Petermann"}
OVERWRITE = True
MAX_DAYS = 15

# INPUTS = "/home/natw/Documents/Greenland_data/WorldView/petermann-8m-dems"
# STABLE_REFERENCE_DATA = "/home/natw/Documents/Greenland_data/CryoSat/csv-nonice/petermann_nonice.csv"
# TRANSIENT_REFERENCE_DATA = "/home/natw/Documents/Greenland_data/CryoSat/csv-petermann"
# OUTPUTS = "/home/natw/Documents/Greenland_data/WorldView/test-alignments"
# REGIONS = {"Petermann"}
# OVERWRITE = True
# MAX_DAYS = 15

ensureisdir(OUTPUTS)

dem_filenames = [os.path.join(INPUTS, f) for f in os.listdir(INPUTS) if f.endswith(".tif")]

def str2date(s):
    if len(s) != 15:
        raise ValueError("input string expected to have length 15")
    return datetime.datetime(int(s[:4]), int(s[4:6]), int(s[6:8]),
                             int(s[9:11]), int(s[11:13]), int(s[13:15]),
                             tzinfo=tz_utc)

def cryosat_date(fnm):
    """ returns a range of dates spanned by a cryosat product file

    may raise ValueError if something goes wrong extracting dates
    """
    m = re.search(r"[0-9]{8,8}T[0-9]{6,6}", fnm)
    if m is not None:
        dt0 = str2date(fnm[m.start():m.end()])
    else:
        raise ValueError
    n = re.search(r"[0-9]{8,8}T[0-9]{6,6}", fnm[m.end():])
    if n is not None:
        dt1 = str2date(fnm[m.end()+n.start():m.end()+n.end()])
    else:
        raise ValueError
    return dt0, dt1

def date_filter(dem_date, cryo_date0, cryo_date1, max_days=10):
    """ return whether dem_date is within *max_days* of the interval
    cryo_date0 -- cryo_date1
    """
    if ((dem_date > cryo_date0 - datetime.timedelta(days=max_days))
        and (dem_date < cryo_date1 + datetime.timedelta(days=max_days))):
        return True
    else:
        return False

def parse_record(rec):
    """ parse a line from a CryoSat CSV returning datetime, lon, lat, elev,
    quality, flag
    """
    days, seconds, microseconds, lon, lat, elev, qual, flag = rec.split(",")
    dt = datetime.datetime(2000, 1, 1, 0, 0, 0, tzinfo=tz_utc) + \
         datetime.timedelta(days=int(days), seconds=int(seconds) + int(microseconds)/1000.0)
    return dt, float(lon), float(lat), float(elev), int(qual), int(flag)

def read_cryosat_csv(fnm):
    """ read a cryosat csv and return lists of datetime, lon, lat, elev,
    quality and flag data
    """
    with open(fnm) as f:
        lines = f.readlines()
    if len(lines) != 0:
        dt, lon, lat, elev, qual, flag = zip(*[parse_record(line) for line in lines])
        return dt, lon, lat, elev, qual, flag
    else:
        return [], [], [], [], [], []

for demfnm in dem_filenames:

    scid = get_sceneid(demfnm)
    region = get_region(scid)

    if region not in REGIONS:
        continue

    demdate = get_date(scid)
    outline, _ = get_best_outline(region, demdate)

    outfnm_trans = os.path.join(OUTPUTS, os.path.split(demfnm)[1].replace(".tif",".dat"))
    outfnm_tif = os.path.join(OUTPUTS, os.path.split(demfnm)[1])

    if os.path.isfile(outfnm_tif) and os.path.isfile(outfnm_trans) and not OVERWRITE:
        print("skipping %s, %s" % (outfnm_trans, outfnm_tif))
        continue

    # get the bounding box
    geom = get_geometry(scid)
    geomdict = json.loads(geom)
    try:
        crds = geomdict["geometry"]["coordinates"][0]
        xmin = min(a[0] for a in crds)
        ymin = min(a[1] for a in crds)
        xmax = max(a[0] for a in crds)
        ymax = max(a[1] for a in crds)
    except KeyError:
        # aww snap
        print("error getting geometry at %s" % scid)
        xmin = -180
        xmax = 180
        ymin = -90
        ymax = 90

    # find the CSV transects that may contain relevant transient data
    csv_filenames = []
    for csvfnm in os.listdir(TRANSIENT_REFERENCE_DATA):
        try:
            if date_filter(demdate, *cryosat_date(csvfnm), max_days=MAX_DAYS):
                csv_filenames.append(csvfnm)
        except ValueError:
            pass

    if len(csv_filenames) == 0:
        print("error: no reference cryosat transects within window")

    # collect reference data
    X, Y, ELEV = [], [], []

    # load the stable reference data and clip
    with open(STABLE_REFERENCE_DATA) as f:
        y, x, z = zip(*[map(float, a.split(",")) for a in f.readlines()])

    if len(x) != 0:
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)

        msk = (x>=xmin) & (y>=ymin) & (x<=xmax) & (y<=ymax)
        X.extend(x[msk])
        Y.extend(y[msk])
        ELEV.extend(z[msk])

    # construct a fast tide predictor function
    tide_time = demdate - datetime.timedelta(days=MAX_DAYS+1)
    delta_time = datetime.timedelta(hours=1)
    tidal_times = []
    tidal_heights = []
    while tide_time < demdate + datetime.timedelta(days=MAX_DAYS+1):
        tidal_heights.append(get_tidal_height_by_region(region, tide_time))
        tidal_times.append(timestamp(tide_time))
        tide_time += delta_time

    tide_interpolator = interp1d(tidal_times, tidal_heights)
    dem_tidal_correction = tide_interpolator(timestamp(demdate))

    # collect transient data that fits dem extent
    for csvfnm in csv_filenames:
        dt, x, y, z, qual, flag = \
                read_cryosat_csv(os.path.join(TRANSIENT_REFERENCE_DATA, csvfnm))
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        qual = np.array(qual)
        flag = np.array(flag)

        # filter by points on the ice tongue
        msk = np.array([outline.contains(karta.Point((_x, _y),
                                                     crs=karta.crs.LonLatWGS84))
                for _x, _y in zip(x, y)], dtype=np.bool)
        dt = [timestamp(_dt) for _dt,m in zip(dt, msk) if m]
        x = x[msk]
        y = y[msk]
        z = z[msk]
        qual = qual[msk]
        flag = flag[msk]

        # on the ice tongue, make tide consistent with when DEM was captured
        ztide = tide_interpolator(dt)
        z = z - ztide + dem_tidal_correction

        # extend the master record
        if len(x) != 0:
            msk = (flag == 0) & (x>=xmin) & (y>=ymin) & (x<=xmax) & (y<=ymax)
            X.extend(x[msk])
            Y.extend(y[msk])
            ELEV.extend(z[msk])

    print("have {0} total reference points".format(len(ELEV)))

    try:
        # save concatenated data to a temporary file
        tmpcsv = os.path.join(OUTPUTS, "cryosat-{0}.csv".format(scid))
        with open(tmpcsv, "w") as f:
            f.writelines([",".join(map(str, items))+"\n" for items in zip(Y, X, ELEV)])

        # run pc_align to compute transform
        prefix = os.path.join(OUTPUTS, "intermediate-{0}".format(scid))
        cmd = ["pc_align", "--max-displacement", "300.0",
               "--save-inv-transformed-reference-points",
               "--compute-translation-only",
               demfnm, tmpcsv, "-o", prefix]
        print(" ".join(cmd))
        retcode = subprocess.call(cmd)

        if retcode == 0:
            shutil.copyfile(prefix+"-inverse-transform.txt", outfnm_trans)
            shutil.move(prefix+"-trans_reference.tif", outfnm_tif)
        else:
            print("pc_align failure occured")

    except Exception as e:
        traceback.print_exc()
        continue

    finally:
        # os.remove(tmpcsv)
        pass

