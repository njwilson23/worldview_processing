#! /usr/bin/env python

import os
import sys
import subprocess
import traceback
from concurrent.futures import ProcessPoolExecutor
import wvutil

TOPDIR = "/nobackup1/natw/hillshades-4m-aligned-filled/"
SAVEDIR = "/nobackup1/natw/correlation-50m-256-4m/"
OVERWRITE = False
RES = (50.0, 50.0)
REGIONS = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}

wvutil.ensureisdir(SAVEDIR)

scenepairs_fnm = sys.argv[1]
with open(scenepairs_fnm, "r") as f:
    pairs = [line.split() for line in f.readlines() if len(line) != 0]

def get_input_image(sceneid, dirname):
    for fnm in os.listdir(dirname):
        if (fnm.endswith(".tif") and (wvutil.get_sceneid(fnm) == sceneid)):
            return os.path.join(dirname, fnm)
    raise KeyError("%s not found in %s" % (sceneid, dirname))

velocity_data = {
        "Nioghalvfjerdsbrae":
            {"u": "/nobackup1/natw/measures/niog_2008_2009_vx.tif",
             "v": "/nobackup1/natw/measures/niog_2008_2009_vy.tif"},
        "Petermann":
            {"u": "/nobackup1/natw/measures/petermann_vx.tif",
             "v": "/nobackup1/natw/measures/petermann_vy.tif"},
        "Ryder":
            {"u": "/nobackup1/natw/measures/ryder_vx.tif",
             "v": "/nobackup1/natw/measures/ryder_vy.tif"},
        "Steensby":
            {"u": "/nobackup1/natw/measures/steensby_vx.tif",
             "v": "/nobackup1/natw/measures/steensby_vy.tif"},
            }

with ProcessPoolExecutor(16) as executor:

    for i, (scid1, scid2) in enumerate(pairs):

        reg = wvutil.get_region(scid1)

        if reg not in REGIONS:
            continue

        try:
            if wvutil.get_date(scid1) > wvutil.get_date(scid2):
                scid1, scid2 = scid2, scid1
        except Exception as e:
            traceback.print_exc()
            continue

        try:
            image1 = get_input_image(scid1, TOPDIR)
            image2 = get_input_image(scid2, TOPDIR)
        except KeyError:
            continue

        dt = wvutil.get_baseline(scid1, scid2)
        out_fnm = os.path.join(SAVEDIR, "{0}_{1}.h5".format(scid1, scid2))

        print("Processing %s, %s" % (scid1, scid2))
        print("\t", wvutil.get_date(scid1), wvutil.get_date(scid2))

        cmd = ["python", "correlate-scenes.py",
                image1, image2,
                velocity_data[reg]["u"],
                velocity_data[reg]["v"],
                out_fnm,
                "--search-size", "256", "256",
                "--ref-size", "128", "128",
                "--baseline", str(dt),
                "--resolution", str(RES[0]), str(RES[1]),
                "--threads", "3"]
        if OVERWRITE:
            cmd.append("--overwrite")

        print(" ".join(cmd))
        executor.submit(subprocess.call, cmd)

