import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
from wvutil import ensureisdir

INPUTS = ["/nobackup1/natw/dems-2m-aligned-filled/",
          "/nobackup1/natw/dems-4m-aligned-filled/",
          "/nobackup1/natw/dems-8m-aligned-filled/",
          "/nobackup1/natw/dems-64m-aligned-filled/"]
OUTPUTS = ["/nobackup1/natw/hillshades-2m-aligned-filled/",
           "/nobackup1/natw/hillshades-4m-aligned-filled/",
           "/nobackup1/natw/hillshades-8m-aligned-filled/",
           "/nobackup1/natw/hillshades-64m-aligned-filled/"]
OVERWRITE = False

for output in OUTPUTS:
    ensureisdir(output)

def _call_cmd(cmd):
    return subprocess.call(cmd)

with ThreadPoolExecutor(max_workers=16) as pool:

    for inputs, outputs in zip(INPUTS, OUTPUTS):
        for fnm in (f for f in os.listdir(inputs) if f.endswith(".tif")):

            fnmout = os.path.join(outputs, fnm)
            if os.path.isfile(fnmout) and not OVERWRITE:
                print("skipping %s" % fnmout)
            else:
                cmd = ["gdaldem", "hillshade",
                       "-co", "COMPRESS=LZW",
                       "-co", "TILED=YES",
                       os.path.join(inputs, fnm), fnmout]
                print(" ".join(cmd))
                pool.submit(_call_cmd, cmd)
