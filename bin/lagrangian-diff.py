#! /usr/bin/env python

import argparse
import os
import sys
import h5py
import karta
import numpy as np
import meltpack

def read_correlation_data(fnm):
    with h5py.File(fnm, "r") as ds:
        x = ds["x"].value
        y = ds["y"].value
        dx = ds["dx"].value
        dy = ds["dy"].value
        strength = ds["strength"].value
        mx = ds["mismatch_x"].value
        my = ds["mismatch_y"].value
    return x, y, dx, dy, mx, my, strength

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=
            "computes the lagragian difference between two DEMs")
    parser.add_argument("dem1",
                        action="store",
                        type=str)
    parser.add_argument("dem2",
                        action="store",
                        type=str)
    parser.add_argument("correlation",
                        action="store",
                        type=str)
    parser.add_argument("output",
                        action="store",
                        type=str)
    parser.add_argument("--baseline",
                        action="store",
                        type=float,
                        default=1.0,
                        metavar="DAYS",
                        help="temporal baseline between scenes")
    parser.add_argument("--to-ice-thickness",
                        action="store_true",
                        default=False,
                        help="Convert DEM changes to hydrostatic ice thickness "
                             "changes")
    parser.add_argument("--tidal-heights",
                        type=float,
                        nargs=2,
                        action="store",
                        default=(0.0, 0.0),
                        help="Specify the tidal height for each scene, which "
                             "will be removed from the elevation difference")
    parser.add_argument("--rhoi",
                        type=float,
                        action="store",
                        default=920.0,
                        help="Specify ice density (default 920 kg/m^3)")
    parser.add_argument("--rhow",
                        type=float,
                        action="store",
                        default=1028.0,
                        help="Specify water density (default 1028 kg/m^3)")
    parser.add_argument("--reference-poly",
                        action="store",
                        default="",
                        metavar="SHP",
                        help="If provided, compute a vertical correction that "
                             "matches the areas within a polygon shapefile. "
                             "For example, this might be a polygon mapping "
                             "non-moving bedrock features.")
    parser.add_argument("--maximum-mismatch",
                        action="store",
                        default=0.25,
                        type=float,
                        help="Fraction of displacement mismatch permitted by "
                             "data qualiity filter.")
    parser.add_argument("--overwrite",
                        action="store_true",
                        default=False)
    args = parser.parse_args()

    if os.path.isfile(args.output) and not args.overwrite:
        print("{0} already exists - aborting".format(args.o))
        sys.exit(1)


    dem1 = karta.read_geotiff(args.dem1)
    dem2 = karta.read_geotiff(args.dem2)
    dem1[dem1[:,:] < -100] = dem1.nodata
    dem2[dem2[:,:] < -100] = dem2.nodata

    x, y, dx, dy, mx, my, strength = read_correlation_data(args.correlation)

    # Mask out spurious correlations (NaNs and high mismatch)
    nm = np.isnan(dx)|np.isnan(dy)|np.isnan(mx)|np.isnan(my)
    mag = np.sqrt(dx[~nm]**2 + dy[~nm]**2)
    mis = np.sqrt(mx[~nm]**2 + my[~nm]**2)
    # Mismatch <25% or mismatch < 30 m/yr
    mask = (mis < args.maximum_mismatch*mag) | (mis*365.0/args.baseline < 30)
    x = x[~nm][mask]
    y = y[~nm][mask]
    dx = dx[~nm][mask]
    dy = dy[~nm][mask]
    strength = strength[~nm][mask]

    # Mask out correlations beyond the range of the DEM grids
    bb = meltpack.utilities.overlap_bbox(dem1.data_bbox, dem2.data_bbox)
    mask = (x>bb[0]+100) & (x<bb[2]-100) & \
           (y>bb[1]+100) & (y<bb[3]-100) & \
           (x+dx>bb[0]+100) & (x+dx<bb[2]-100) & \
           (y+dy>bb[1]+100) & (y+dy<bb[3]-100)
    x = x[mask]
    y = y[mask]
    dx = dx[mask]
    dy = dy[mask]
    strength = strength[mask]

    # Extract differences from the remaining points
    z1 = dem1.sample(x, y)
    z2 = dem2.sample(x+dx, y+dy)

    z1[z1<-1e10] = np.nan
    z2[z2<-1e10] = np.nan
    z1[z1==dem1.nodata] = np.nan
    z2[z2==dem2.nodata] = np.nan

    # Compute a correction using a reference polygon mask
    if len(args.reference_poly) != 0:

        # clip the overlapping DEM subsets
        bb1 = dem1.data_bbox
        bb2 = dem2.data_bbox
        extent = [max(bb1[0], bb2[0]), min(bb1[2], bb2[2]),
                  max(bb1[1], bb2[1]), min(bb1[3], bb2[3])]
        dem1c = dem1.clip(*extent)
        dem2c = dem2.clip(*extent)

        maskpoly = karta.read_shapefile(args.reference_poly)
        msk = karta.RegularGrid(dem1c.transform,
                                values=np.ones(dem1c.size, dtype=np.int8),
                                nodata_value=0,
                                crs=dem1c.crs)
        msk.mask_by_poly(maskpoly, inplace=True)

        b1 = []
        b2 = []
        for mchunk, dchunk1, dchunk2 in zip(msk.aschunks((1024, 1024)),
                                            dem1c.aschunks((1024, 1024)),
                                            dem2c.aschunks((1024, 1024))):
            b1.extend(dchunk1[mchunk[:,:]==1])
            b2.extend(dchunk2[mchunk[:,:]==1])
        b1 = np.array(b1, dtype=np.float32)
        b2 = np.array(b2, dtype=np.float32)

        nodatamask = (b1==dem1c.nodata) | (b2==dem2c.nodata)
        corr = np.nanmedian(b1[~nodatamask] - b2[~nodatamask])
        std = np.nanstd(b1[~nodatamask] - b2[~nodatamask])
        print("  Correction: {0:6.2f} +/- {1:6.2f}".format(corr, std))
    else:
        corr = 0.0

    # Add tidal corrections
    corr += args.tidal_heights[1]-args.tidal_heights[0]

    if args.to_ice_thickness:
        dzdt = (z2-z1+corr)/(1.0-args.rhoi/args.rhow)/args.baseline*365.0
    else:
        dzdt = (z2-z1+corr)/args.baseline*365.0

    options = {"compression": "gzip"}
    with h5py.File(args.output, mode="w") as ds:
        ds.attrs["file_dem1"] = args.dem1
        ds.attrs["file_dem2"] = args.dem2
        ds.attrs["file_corr"] = args.correlation
        ds.attrs["crs"] = dem1.crs.get_proj4()

        ds.create_dataset("x", data=x, **options)
        ds.create_dataset("y", data=y, **options)
        ds.create_dataset("z0", data=z1, **options)
        ds.create_dataset("z1", data=z2, **options)
        ds.create_dataset("DHDt", data=dzdt, **options)

    del dem1, dem2

