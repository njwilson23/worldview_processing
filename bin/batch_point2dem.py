import os
import subprocess
from concurrent.futures import ThreadPoolExecutor

POINTCLOUDS = "/nobackup1/natw/cryosat-alignments"
OUTDEMS = "/nobackup1/natw/aligned-dems-2m"
RES = 2

def oncomplete(fut):
    if not fut.done():
        print("point2dem error:")
        print(fut.exception())

with ThreadPoolExecutor(max_workers=4) as pool:

    for filename in filter(lambda s: s.endswith(".tif"), os.listdir(POINTCLOUDS)):
        out_prefix = os.path.join(OUTDEMS, os.path.splitext(filename)[0])

        cmd = ["point2dem",
               "--t_srs", "+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +units=m +datum=WGS84 +ellps=WGS84 +no_defs"
               "--tr", str(RES),
               os.path.join(POINTCLOUDS, filename),
               "-o", out_prefix]

        fut = pool.submit(subprocess.call, cmd)
        fut.add_done_callback(oncomplete)

