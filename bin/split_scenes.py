import os

nsplits = 100

if not os.path.isdir("scenes"):
    os.mkdir("scenes")

# get list of scenes
scenes = sorted(set([os.path.splitext(f)[0] for f in os.listdir("/nobackup1/natw/dems")]))

newscenes = [[] for _ in range(nsplits)]

# distribute pairs
cnt = 0
for s in scenes:
    newscenes[cnt].append(s)
    cnt += 1
    if cnt == nsplits:
        cnt = 0

# print to files
for i, scenelist in enumerate(newscenes):
    print("writing {0} scenes to file {1}".format(len(scenelist), i))
    with open("scenes/part_{i}.list".format(i=i), "w") as f:
        for s in scenelist:
            f.write("{0}\n".format(s))

