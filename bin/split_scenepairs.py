from scenepairs import scenepairs

nsplits = 50
newsets = [[] for i in range(nsplits)]

# distribute pairs
cnt = 0
for reg in scenepairs:
    for pair in scenepairs[reg]:
        newsets[cnt].append(pair)
        cnt += 1
        if cnt == nsplits:
            cnt = 0

# print to files
for i, newset in enumerate(newsets):
    print("writing {0} pairs to file {1}".format(len(newset), i))
    with open("scenepairs/part_scenepairs_{i}.list".format(i=i), "w") as f:
        for pair in newset:
            f.write("{0} {1}\n".format(*pair))

