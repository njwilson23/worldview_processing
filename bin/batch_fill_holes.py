#! /usr/bin/env python
from __future__ import print_function
import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
from wvutil import ensureisdir

INDIR =  ["/nobackup1/natw/dems-2m-aligned/",
          "/nobackup1/natw/dems-4m-aligned/",
          "/nobackup1/natw/dems-8m-aligned/",
          "/nobackup1/natw/dems-64m-aligned/"]
OUTDIR =  ["/nobackup1/natw/dems-2m-aligned-filled/",
           "/nobackup1/natw/dems-4m-aligned-filled/",
           "/nobackup1/natw/dems-8m-aligned-filled/",
           "/nobackup1/natw/dems-64m-aligned-filled/"]
INDIR = INDIR[1:]
OUTDIR = OUTDIR[1:]
OVERWRITE = False

fnms = []
for indir in INDIR:
    for f in os.listdir(indir):
        if f.endswith(".tif"):
            fnms.append(f)

for outdir in OUTDIR:
    ensureisdir(outdir)

def fill_nodata(fin, fout):

    cmd = ["gdal_fillnodata.py", "-md", "50",
           "-co", "COMPRESS=LZW",
           "-co", "TILED=YES",
           "-co", "BIGTIFF=YES",
           fin, fout]
    subprocess.call(cmd)

    return fin, fout

def print_finished(fut):
    print("finished", fut.result())
    return

with ThreadPoolExecutor(max_workers=16) as pool:

    for indir, outdir in zip(INDIR, OUTDIR):
        for f in fnms:

            outfnm = os.path.join(outdir, f)

            if os.path.isfile(outfnm) and not OVERWRITE:
                print("skipping %s" % f)

            else:
                fut = pool.submit(fill_nodata, os.path.join(indir, f), outfnm)
                fut.add_done_callback(print_finished)

