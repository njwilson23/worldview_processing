import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
from wvutil import ensureisdir

INPUTS = "/nobackup1/natw/dems"
OUTPUTS = "/nobackup1/natw/dems-5m"
OVERWRITE = False

ensureisdir(OUTPUTS)

def _call_cmd(cmd):
    return subprocess.call(cmd)

with ThreadPoolExecutor(max_workers=16) as pool:

    for fnm in (f for f in os.listdir(INPUTS) if f.endswith(".tif")):

        fnmout = os.path.join(OUTPUTS, fnm)
        if os.path.isfile(fnmout) and not OVERWRITE:
            print("skipping %s" % fnmout)
        else:
            cmd = ["gdalwarp",
                   "-tr", "5", "5",
                   "-co", "COMPRESS=LZW",
                   "-co", "TILED=YES",
                   os.path.join(INPUTS, fnm), fnmout]
            print(" ".join(cmd))
            pool.submit(_call_cmd, cmd)
