#! /usr/bin/env python
# Generate a set of configuration files for asputil
#
# This is a universal version for itsineq.whoi.edu, poison.whoi.edu, and cheakmus
# Nat Wilson (2015-11-08)
import os
import socket
import yaml
from wvutil import get_sceneid, get_region
from scenedata import (niog_patterns,
                       petermann_patterns,
                       ryder_patterns,
                       steensby_patterns)

def findsubdir(topdir, subdir):
    subdir = subdir.rstrip("/")
    for dirpath, dirnames, filenames in os.walk(topdir):
        if subdir in dirnames:
            return dirpath
    raise LookupError("cannot find %s under %s" % (subdir, topdir))

base_yaml = {"Nioghalvfjerdsbrae": "niog_hiquality.yaml",
             "Petermann": "petermann_hiqual.yaml",
             "Ryder": "ryder_hiqual.yaml",
             "Steensby": "steensby_hiqual.yaml"}

hostname = socket.gethostname()

if hostname == "Cheakamus":
    DATADIR = "/home/natw/Documents/Greenland_data/WorldView/"
    WORKSPACE = "/home/natw/Documents/Greenland_data/WorldView/"
    RESULTSDIR = "/home/natw/Documents/Greenland_data/WorldView/"
elif hostname == "poison":
    DATADIR = "/home/poison/natw/wvdata/"
    WORKSPACE = "/home/poison/natw/wvdata/"
    RESULTSDIR = "/home/poison/natw/wvdata/"
elif hostname == "itsineq.whoi.edu":
    DATADIR = "/Users/nwilson/DATA/wvdata/"
    WORKSPACE = "/Users/nwilson/wvdata/"
    RESULTSDIR = "/Users/nwilson/DATA/wvdata/"
elif (hostname == "eofe4") or (hostname == "eofe5"):
    DATADIR = "/nobackup1/natw/wvdata/"
    WORKSPACE = "/nobackup1/natw/wvdata/"
    RESULTSDIR = "/nobackup1/natw/wvdata/"
else:
    raise RuntimeError("paths not defined for host '%s'" % hostname)

all_patterns = niog_patterns + petermann_patterns + ryder_patterns + steensby_patterns

for wvdir, leftglob, rightglob in all_patterns:

    region = get_region(get_sceneid(wvdir))

    fnm = wvdir.rstrip("/")+".yaml"

    try:
        datapath = findsubdir(DATADIR, wvdir)
    except LookupError as e:
	print(wvdir)
