""" Some handy functions for the worldview processing scripts """

import atexit
import os
import re
import sqlite3
import datetime
import dateutil.parser
import numpy as np
import karta
from karta.vector import read_shapefile

_outlines = read_shapefile(os.path.join(os.path.split(__file__)[0],
                                        "floating-tongues.shp"))
_scenes = os.path.join(os.path.split(__file__)[0], "scenes.db")
_tides = os.path.join(os.path.split(__file__)[0], "tides.db")

if not os.path.isfile(_scenes):
    raise IOError("missing scenes.db database")
if not os.path.isfile(_tides):
    raise IOError("missing tides.db database")

con_scenes = sqlite3.connect(_scenes, check_same_thread=False)
con_tides = sqlite3.connect(_tides, check_same_thread=False)
c_scenes = con_scenes.cursor()
c_tides = con_tides.cursor()

def close(con):
    con.close()

atexit.register(close, con_scenes)
atexit.register(close, con_tides)

class UTCZero(datetime.tzinfo):

    def utcoffset(self, dt):
        return datetime.timedelta(0)

    def dst(self, dt):
        return datetime.timedelta(0)

    def tzname(self, dt):
        return None

tz_utc = UTCZero()

def _daynumber_to_datetime(year, dn):
    dt = datetime.datetime(year, 1, 1, tzinfo=tz_utc) + datetime.timedelta(days=dn-1)
    return dt

def _outline_date(ol):
    year = int(ol.properties["Year"])
    dn = int(ol.properties["DayNumber"])
    return _daynumber_to_datetime(year, dn)

def get_sceneid(fnm):
    m = re.search("[A-Z0-9]{16}", fnm)
    if m is None:
        raise ValueError("cannot parse scene id from %s" % fnm)
    else:
        return fnm[m.start():m.end()]

def get_region(sceneid):
    try:
        c_scenes.execute("SELECT region FROM scenes WHERE sceneid=?", [sceneid])
    except sqlite3.InterfaceError:
        raise ValueError("invalid input - expected string")
    reg = c_scenes.fetchone()
    if reg is None:
        raise KeyError("sceneid {0} not in database".format(sceneid))
    return reg[0]

def get_date(sceneid):
    try:
        c_scenes.execute("SELECT aqdate FROM scenes WHERE sceneid=?", [sceneid])
    except sqlite3.InterfaceError:
        raise ValueError("invalid input - expected string")
    datestr = c_scenes.fetchone()
    if datestr is None:
        raise KeyError("sceneid {0} not in database".format(sceneid))
    dt = dateutil.parser.parse(datestr[0])
    return dt.replace(tzinfo=tz_utc)

def get_baseline(s1, s2):
    return (get_date(s2)-get_date(s1)).days

def get_geometry(sceneid):
    try:
        c_scenes.execute("SELECT geometry FROM scenes WHERE sceneid=?", [sceneid])
    except sqlite3.InterfaceError:
        raise ValueError("invalid input - expected string")
    geo = c_scenes.fetchone()
    if geo is None:
        raise KeyError("sceneid {0} not in database".format(sceneid))
    return geo[0]

def get_standard_outline(region):
    """ given a region, return a default the geometry from the hard-coded
    outlines shapefile that matches the region.
    """
    if region == "Ryder":
        return get_best_outline(region, datetime.datetime(2014, 8, 11, tzinfo=tz_utc))[0]
    elif region == "Petermann":
        return get_best_outline(region, datetime.datetime(2012, 3, 28, tzinfo=tz_utc))[0]
    elif region == "Steensby":
        return get_best_outline(region, datetime.datetime(2014, 8, 11, tzinfo=tz_utc))[0]
    elif region == "Nioghalvfjerdsbrae":
        return get_best_outline(region, datetime.datetime(2016, 6, 1, tzinfo=tz_utc))[0]
    else:
        raise KeyError("No default region hard-coded for {0}".format(region))

def get_best_outline(region, date=None):
    """ given a region and a datetime, return the geometry in the hard-coded
    outlines shapefile that matches the region and is closest in date.

    if *date* is not specified, the most recent outline is returned.

    return the geometry and the temporal mismatch in days.
    """
    if date is None:
        date = datetime.datetime.now(tz=tz_utc)

    ol = None
    dtdays = 10000000
    for _ol in _outlines:
        if _ol.properties["Name"] == region:
            _d = _outline_date(_ol)
            _dtdays = abs((date-_d).days)
            if _dtdays < dtdays:
                ol = _ol
                dtdays = _dtdays
    if ol is None:
        raise KeyError("cannot find outline matching {0}".format(
                            get_region(get_sceneid(fnm))))
    else:
        return ol, dtdays

def get_tidal_height(sceneid):
    dt = get_date(sceneid)
    reg = get_region(sceneid)
    return get_tidal_height_by_region(reg, dt)

def timestamp(dt):
    delta = dt - datetime.datetime(1970, 1, 1, tzinfo=tz_utc)
    return delta.total_seconds()

def get_tidal_height_by_region(region, dt):
    tstamp = timestamp(dt)
    # select rows within 10 hours
    try:
        c_tides.execute("""SELECT timestamp, tideheight
                FROM {0}
            WHERE abs(timestamp-?) < 10*3600""".format(region), [tstamp])
    except sqlite3.InterfaceError:
        raise ValueError("invalid SQL query")
    except sqlite3.OperationalError:
        raise KeyError("no tide table for {0}".format(region))
    result = c_tides.fetchall()
    if len(result) == 0:
        raise KeyError("no results near %s" % dt.strftime("%Y-%m-%dT%H-%M-%S"))
    timestamps, tideheights = zip(*result)
    return np.interp(tstamp, timestamps, tideheights)

def is_newer(fnm1, fnm2):
    """ Is *fnm1* newer than *fnm2*? """
    if os.path.isfile(fnm1) and os.path.isfile(fnm2):
        return os.path.getmtime(fnm1) > os.path.getmtime(fnm2)
    else:
        raise FileNotFoundError()

def ensureisdir(dirname):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
        return True
    else:
        return False

def is_blacklisted(sceneid):
    return sceneid in BLACKLIST

BLACKLIST = [
    "10200100231CC500",
    "1020010030B9A400",
    "103001001817A400",
    "103001001958C200",
    "1030010036B2F500"
]
