#! /usr/bin/env python
""" Super script to compute mass balance products """

import argparse
import datetime
import logging
import multiprocessing
import os
import sys
import shutil
import subprocess
import tempfile
from concurrent.futures import ThreadPoolExecutor, as_completed

from depgraph import Dataset, DatasetGroup, buildall
import h5py
import karta
import meltpack
import numpy as np

from wvutil import (get_sceneid, get_region,
                     get_best_outline,
                     get_date, get_baseline,
                     get_tidal_height,
                     ensureisdir)

parser = argparse.ArgumentParser()
parser.add_argument("regions", nargs="+", help="regions to compute")
parser.add_argument("--dir", action="store", default="")
parser.add_argument("--dryrun", action="store_true", default=False)
args = parser.parse_args()

now = datetime.datetime.now()
log_fnm = "make_{reg}-{yr:0>4d}{mnth:0>2d}{dy:0>2d}-{hr:0>2d}{mn:0>2d}{sc:0>2d}.log".format(
                        reg=args.regions[0],
                        yr=now.year, mnth=now.month, dy=now.day,
                        hr=now.hour, mn=now.minute, sc=now.second)
logger = logging.getLogger("make")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

fh = logging.FileHandler(log_fnm)
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

logger.addHandler(ch)
logger.addHandler(fh)

if not os.path.isdir(args.dir):
    os.makedirs(args.dir)

# Outputs
LDHDT_DIR = os.path.join(args.dir, "ldhdt-50m/")
LDHDT_GRID_DIR = os.path.join(args.dir, "ldhdt-grids-128m/")
VEL_DIR = os.path.join(args.dir, "correlation-velocities-128m/")
HDIVU_DIR = os.path.join(args.dir, "hdivu-128m/")
VEL_COMPOSITE_DIR = os.path.join(args.dir, "vel-composites-128m/")

# Inputs
CORR_DIR = "/nobackup1/natw/correlation-50m-merge/"
DEM8_DIR = "/nobackup1/natw/dems-8m-autoaligned/"
DEM64_DIR = "/nobackup1/natw/dems-64m-autoaligned/"
DEM128_DIR = "/nobackup1/natw/dems-128m-autoaligned/"

for dirname in (LDHDT_DIR, LDHDT_GRID_DIR, VEL_DIR, HDIVU_DIR, VEL_COMPOSITE_DIR):
    ensureisdir(dirname)

# ------------------------------------------------------------------------------
# Construct a dependency graph based on existing DEMs and correlation data
# ------------------------------------------------------------------------------

# Read the lists of merged correlation and DEM files

correlation_files = [os.path.join(CORR_DIR, f) for f in os.listdir(CORR_DIR)
                     if f.endswith(".h5") and
                        get_region(get_sceneid(f)) in args.regions]

dem8_files = [os.path.join(DEM8_DIR, f) for f in os.listdir(DEM8_DIR)
               if f.endswith(".tif") and
                  get_region(get_sceneid(f)) in args.regions]

dem64_files = [os.path.join(DEM64_DIR, f) for f in os.listdir(DEM64_DIR)
                if f.endswith(".tif") and
                   get_region(get_sceneid(f)) in args.regions]

dem128_files = [os.path.join(DEM128_DIR, f) for f in os.listdir(DEM128_DIR)
                if f.endswith(".tif") and
                   get_region(get_sceneid(f)) in args.regions]

corr_deps = [Dataset(f) for f in correlation_files]
dem8_deps = [Dataset(f) for f in dem8_files]
dem64_deps = [Dataset(f) for f in dem64_files]
dem128_deps = [Dataset(f) for f in dem128_files]

# Define ldhdt, ldhdt-grid, and velocity targets
def ldhdt_fnm(f):
    scid1 = get_sceneid(f.split("_")[0])
    scid2 = get_sceneid(f.split("_")[1])
    return os.path.join(LDHDT_DIR, "{0}_{1}.h5".format(scid1, scid2))

def ldhdt_grid_fnm(f):
    scid1 = get_sceneid(f.split("_")[0])
    scid2 = get_sceneid(f.split("_")[1])
    return os.path.join(LDHDT_GRID_DIR, "{0}_{1}.tif".format(scid1, scid2))

def vel_fnm(f):
    scid1 = get_sceneid(f.split("_")[0])
    scid2 = get_sceneid(f.split("_")[1])
    return os.path.join(VEL_DIR, "{0}_{1}_dx.tif".format(scid1, scid2))

def hdivu_fnm(f):
    scid = get_sceneid(f)
    return os.path.join(HDIVU_DIR, "{0}.tif".format(scid))

alldeps = []
veldeps = []
for corr in corr_deps:

    scid1 = get_sceneid(os.path.split(corr.name)[1].split("_")[0])
    scid2 = get_sceneid(os.path.split(corr.name)[1].split("_")[1])
    dem1, dem2 = None, None

    for dem in dem8_deps:
        if get_sceneid(dem.name) == scid1:
            dem1 = dem
        elif get_sceneid(dem.name) == scid2:
            dem2 = dem
        if (dem1 is not None) and (dem2 is not None):
            break

    if dem1 is None:
        raise RuntimeError("cannot find DEM with id = {0}".format(scid1))

    if dem2 is None:
        raise RuntimeError("cannot find DEM with id = {0}".format(scid2))

    ldhdt = Dataset(ldhdt_fnm(corr.name), tool="lagrangian-diff")
    ldhdtgrid = Dataset(ldhdt_grid_fnm(corr.name), tool="grid-ldhdt")

    ldhdt.dependson(corr, dem1, dem2)
    ldhdtgrid.dependson(ldhdt)

    vel = Dataset(vel_fnm(corr.name), tool="gridvel")
    vel.dependson(corr)

    veldeps.append(vel)
    alldeps.extend([ldhdt, ldhdtgrid, vel])

# Construct a composite velocity
vel_composite = Dataset(os.path.join(VEL_COMPOSITE_DIR, "dx.tif"), tool="vel-composite")
vel_composite.dependson(*veldeps)
alldeps.append(vel_composite)

# Declare velocity divergence dependencies
for dem in dem128_deps:
    hdivu = Dataset(hdivu_fnm(dem.name), tool="hdivu")
    hdivu.dependson(dem, vel_composite)
    alldeps.append(hdivu)

# ------------------------------------------------------------------------------
# Define a master builder function that knows how to delegate builds
# ------------------------------------------------------------------------------

class SkipBuild(Exception):
    """ Exception to raise when a non-fatal build error occurs """
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class SelectionError(Exception):
    """ Exception to raise selecting a dependency fails """
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class NoSuitableDataset(Exception):
    """ Exception to raise when a dynamically-chosen dataset cannot be found """
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

def select(items, where=None):
    if where is None:
        raise ValueError("`where` undefined")
    for item in items:
        if where(item):
            return item
    raise SelectionError("no dataset found")

def selectall(items, where=None):
    if where is None:
        raise ValueError("`where` undefined")
    selected = []
    for item in items:
        if where(item):
            selected.append(item)
    return selected

def runcmd(cmd):
    ret = subprocess.call(cmd)
    if ret != 0:
        logger.error("failed to execute\n{0}".format(" ".join(cmd)))
    return ret

def build_target(target, dryrun=False):
    exitcode = 0
    dependencies = list(target.parents(0))

    if target.tool == "no-op":
        pass

    elif target.tool == "lagrangian-diff":

        RHOI = 920.0
        RHOW = 1028.0
        MAX_MISMATCH = 0.5
        maskpolys = {
                "Nioghalvfjerdsbrae": "Nioghalvfjerdsbrae/niog_bedrock.shp",
                "Petermann": "Petermann/petermann_bedrock.shp",
                "Ryder": "Ryder/ryder_bedrock.shp",
                "Steensby": "Steensby/steensby_bedrock.shp",
                }

        assert dependencies is not None and (len(dependencies) == 3)

        corr = select(dependencies, where=lambda d: d.name.endswith(".h5"))
        dem1, dem2 = selectall(dependencies, where=lambda d: d.name.endswith(".tif"))

        if get_date(get_sceneid(dem1.name)) > get_date(get_sceneid(dem2.name)):
            dem1, dem2 = dem2, dem1

        region = get_region(get_sceneid(target.name))
        mask = maskpolys[region]

        dt = get_baseline(get_sceneid(dem1.name), get_sceneid(dem2.name))
        tide1 = get_tidal_height(get_sceneid(dem1.name))
        tide2 = get_tidal_height(get_sceneid(dem2.name))
        logger.info("  Tides: {0:5.2f} {1:5.2f}".format(tide1, tide2))

        cmd = ["python", "lagrangian-diff.py",
               "--baseline", str(dt),
               "--to-ice-thickness",
               "--rhow", str(RHOW),
               "--rhoi", str(RHOI),
               "--maximum-mismatch", str(MAX_MISMATCH),
               "--reference-poly", mask,
               "--tidal-heights", str(tide1), str(tide2),
               "--overwrite",
               dem1.name, dem2.name, corr.name, target.name]

        logger.info(" ".join(cmd))
        if args.dryrun:
            exitcode = 0
        else:
            exitcode = runcmd(cmd)

    elif target.tool == "grid-ldhdt":

        SMOOTH_ITERATIONS = 3
        OUTPUT_RESOLUTION = 128.0

        assert dependencies is not None and (len(dependencies) == 1)
        ldhdt = select(dependencies, where=lambda d: d.name.endswith(".h5"))

        def read_hdf(fnm):
            with h5py.File(fnm, mode="r") as ds:
                x = ds["x"].value.ravel()
                y = ds["y"].value.ravel()
                DHDt = ds["DHDt"].value.ravel()
                date0 = get_date(get_sceneid(ds.attrs["file_dem1"]))
                date1 = get_date(get_sceneid(ds.attrs["file_dem2"]))

            m = np.isnan(DHDt)
            return x[~m], y[~m], DHDt[~m], date0, date1

        def choose_transform(x, y, res=64.0):
            """ from a set of coodinates, choose a reasonable geotransform """
            T = [(x.min()//res)*res-0.5*res,
                 (y.min()//res)*res-0.5*res,
                 res, res, 0.0, 0.0]
            return T

        if args.dryrun:
            logger.info("grid-ldhdt")
            return 0

        x, y, dH, date0, date1 = read_hdf(ldhdt.name)
        if len(x) < 10:
            raise SkipBuild("Too few records ({1}) in {0} to "
                            "grid".format(target.name, len(x)))

        T = choose_transform(x, y, res=OUTPUT_RESOLUTION)
        grid = karta.raster.gridpoints(x, y, dH, T, karta.crs.NSIDCNorth)

        if SMOOTH_ITERATIONS > 0:
            grid[:,:] = meltpack.filt.smooth5(grid[:,:], SMOOTH_ITERATIONS)

        grid.to_geotiff(target.name, compress="LZW", TILED="YES")

    elif target.tool == "gridvel":

        assert dependencies is not None and (len(dependencies) == 1)
        corr = select(dependencies, where=lambda d: d.name.endswith(".h5"))

        temp_outlines = tempfile.mkdtemp()

        try:
            region = get_region(get_sceneid(corr.name))
            date1 = get_date(get_sceneid(os.path.split(corr.name)[1].split("_", 1)[0]))
            date2 = get_date(get_sceneid(os.path.split(corr.name)[1].split("_", 1)[1]))
            dt = (date2-date1).days

            # Save outline json to file
            ol, _ = get_best_outline(region, date2)
            ol_fnm = os.path.join(temp_outlines, "tmp.geojson")
            ol.to_geojson(ol_fnm)

            prefix = target.name.strip("_dx.tif")

            cmd = ["python", "gridvel.py",
                    corr.name, prefix,
                    "-tr", "128.0", "128.0",
                    "--days", str(dt),
                    "--median-iterations", "5",
                    "--smooth-iterations", "20",
                    "--maximum-mismatch-factor", "0.3",
                    "--polymask", ol_fnm,
                    "--overwrite"]

            if args.dryrun:
                logger.info(" ".join(cmd))
            else:
                exitcode = runcmd(cmd)

        finally:
            shutil.rmtree(temp_outlines)

    elif target.tool == "hdivu":

        assert dependencies is not None and (len(dependencies) == 2)
        dem = select(dependencies, where=lambda d: d.name.startswith(DEM128_DIR))
        uvel = select(dependencies, where=lambda d: d.name.startswith(VEL_COMPOSITE_DIR) and "dx" in d.name)
        # Danger: just assume this exists
        vvel = Dataset(uvel.name.replace("dx.tif", "dy.tif"))

        geoid_heights = {
                "Nioghalvfjerdsbrae": 29.4,
                "Petermann": 14.9,
                "Ryder": 21.16,
                "Steensby": 19.21,
                }

        temp_outline_dir = tempfile.mkdtemp()

        try:
            region = get_region(get_sceneid(dem.name))
            date = get_date(get_sceneid(dem.name))
            ol, _ = get_best_outline(region, date)
            ol_fnm = os.path.join(temp_outline_dir, "tmp.geojson")
            ol.to_geojson(ol_fnm)

            cmd = ["python", "hdivu.py", "--is-elevation",
                    "--datum", str(geoid_heights[region]),
                    "--outline", ol_fnm,
                    "--overwrite",
                    dem.name, uvel.name, vvel.name, target.name]

            if args.dryrun:
                logger.info(" ".join(cmd))
            else:
                exitcode = runcmd(cmd)

        finally:
            shutil.rmtree(temp_outline_dir)

    elif target.tool == "vel-composite":

        assert dependencies is not None
        u_fnms = [dep.name for dep in dependencies]
        v_fnms = [dep.name.replace("dx.tif", "dy.tif") for dep in dependencies]

        logger.info("building velocity composites")

        if not args.dryrun:

            ucomp = karta.raster.merge([karta.read_geotiff(fnm) for fnm in u_fnms if os.path.isfile(fnm)])
            vcomp = karta.raster.merge([karta.read_geotiff(fnm) for fnm in v_fnms if os.path.isfile(fnm)])

            ucomp.to_geotiff(target.name)
            vcomp.to_geotiff(target.name.replace("dx.tif", "dy.tif"))

    else:
        raise ValueError("unrecognized build tool: '{0}'".format(target.tool))

    return exitcode

# ------------------------------------------------------------------------------
# Define a dummy dependency and start the build
# ------------------------------------------------------------------------------

logger.info("found {0} dependencies".format(len(alldeps)))
everything = Dataset("dummy", tool="no-op")
everything.dependson(*alldeps)

failures = 0
nprocs = max(multiprocessing.cpu_count(), 8)

with ThreadPoolExecutor(max_workers=nprocs) as executor:

    logger.info("traversing dependency graph")

    for group in buildall(everything):
        futures = []

        for dep, reason in group:
            logger.info("building {dep}\n  because {reason}".format(dep=dep.name, reason=reason))
            future = executor.submit(build_target, dep, args.dryrun)
            futures.append(future)

        for fut in as_completed(futures):
            try:
                ret = fut.result()
                failures += ret
            except SkipBuild as e:
                logger.error("SkipBuild: {0}".format(str(e)))
                failures += 1

    logger.warn("{0} failures took place while building".format(failures))
    sys.exit(failures)
