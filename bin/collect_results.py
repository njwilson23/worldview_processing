import os
import re
import shutil

def isolder(a, b):
    """ returns true if dependency *a* was last modified before dependencies
    *b* """
    sa = os.stat(a)
    sb = os.stat(b)
    return sa.st_mtime < sb.st_mtime

def get_sceneid(fnm):
    m = re.search("[A-Z0-9]{16}", fnm)
    if m is None:
        raise ValueError("cannot parse scene id from %s" % fnm)
    else:
        return fnm[m.start():m.end()]
 
datadir = "/nobackup1/natw/wvdata/"

targets = {"out-DEM.tif":             ("{0}.tif", "/nobackup1/natw/dems"),
           "projL.tif":               ("{0}.tif", "/nobackup1/natw/projL"),
           "out-IntersectionErr.tif": ("{0}.tif", "/nobackup1/natw/errors")}

for dirpath, dirnames, filenames in os.walk(datadir):
    for fnm in filenames:
        if fnm in targets:
            frompath = os.path.join(dirpath, fnm)
            scid = get_sceneid(dirpath)
            savefile = targets[fnm][0].format(scid)
            savedir = targets[fnm][1]
            savepath = os.path.join(savedir, savefile)

            if (not os.path.isfile(savepath)) or isolder(savepath, frompath):

                if not os.path.isdir(savedir):
                    os.makedirs(savedir)

                print("cp %s %s" % (frompath, savepath))
                shutil.copyfile(frompath, savepath)
