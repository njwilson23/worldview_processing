#! /usr/bin/env python

import argparse
import h5py
import karta
import numpy as np
import os

def read_hdf(fnm):
    d = {}
    with h5py.File(fnm, mode="r") as ds:
        for key in ds.keys():
            try:
                d[key] = ds[key].value
            except Exception as e:
                print("read_hdf:", e)
    return d

def read_hdf_attributes(fnm):
    d = {}
    with h5py.File(fnm, mode="r") as ds:
        for key in ds.attrs.keys():
            d[key] = ds.attrs[key].decode("utf-8")
    return d

def write_hdf(fnm, d, attrs=None):
    with h5py.File(fnm, mode="w") as ds:
        for k,v in d.items():
            ds.create_dataset(k, compression="gzip", data=v)
        if attrs is not None:
            for k,v in attrs.items():
                ds.attrs[k] = v.encode("utf-8")
    return

def extents_from_hdf(fnm):
    with h5py.File(fnm, mode="r") as ds:
        x = ds["x"].value
        y = ds["y"].value
        return np.min(x), np.max(x), np.min(y), np.max(y)

def filter_data(d, dt):
    msk = np.array([np.any(list(map(np.isnan, a))) for a in zip(*d.values())])

    mag = np.sqrt(d["dx"]**2 + d["dy"]**2)
    mis = np.sqrt(d["mismatch_x"]**2 + d["mismatch_y"]**2)
    msk = msk | ((mis > 0.5*mag) & (mis*dt/365.0 > 20))

    dnew = {k:v[~msk] for k,v in d.items()}
    return dnew

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Merge a series of corrlation "
                "datasets. Each subsequent dataset is given half the weight of "
                "the previous dataset.")
    parser.add_argument("datasets", nargs="+")
    parser.add_argument("--resolution", default=50.0, type=float)
    parser.add_argument("--baseline", default=-1.0, type=float,
                        help="image separation in days")
    parser.add_argument("--overwrite", action="store_true", default=False)
    parser.add_argument("-o", required=True, type=str)
    args = parser.parse_args()

    if os.path.isfile(args.o) and not args.overwrite:
        raise IOError("'{0}' already exists".format(args.o))

    DX, DY, MX, MY, ST = [], [], [], [], []

    if args.baseline <= 0:
        raise ValueError("positive baseline must be provided")

    xmin, xmax, ymin, ymax = extents_from_hdf(args.datasets[0])
    for fnm in args.datasets[1:]:
        _xmin, _xmax, _ymin, _ymax = extents_from_hdf(args.datasets[0])
        xmin = min(xmin, _xmin)
        xmax = max(xmax, _xmax)
        ymin = min(ymin, _ymin)
        ymax = max(ymax, _ymax)

    xmin -= 0.5*args.resolution
    xmax += 0.5*args.resolution
    ymin -= 0.5*args.resolution
    ymax += 0.5*args.resolution

    T = [xmin, ymin, args.resolution, args.resolution, 0.0, 0.0]

    for i in range(len(args.datasets)):
        data = filter_data(read_hdf(args.datasets[i]), args.baseline)

        dx = karta.raster.gridpoints(data["x"], data["y"], data["dx"], T, karta.crs.Cartesian)
        dy = karta.raster.gridpoints(data["x"], data["y"], data["dy"], T, karta.crs.Cartesian)
        mx = karta.raster.gridpoints(data["x"], data["y"], data["mismatch_x"], T, karta.crs.Cartesian)
        my = karta.raster.gridpoints(data["x"], data["y"], data["mismatch_y"], T, karta.crs.Cartesian)
        st = karta.raster.gridpoints(data["x"], data["y"], data["strength"], T, karta.crs.Cartesian)

        DX.append(dx)
        DY.append(dy)
        MX.append(mx)
        MY.append(my)
        ST.append(st)

    weights = [1.0/(2**1) for i in range(len(args.datasets))]
    dxm = karta.raster.merge(DX, weights=weights)
    dym = karta.raster.merge(DY, weights=weights)
    mxm = karta.raster.merge(MX, weights=weights)
    mym = karta.raster.merge(MY, weights=weights)
    stm = karta.raster.merge(ST, weights=weights)

    attrs = read_hdf_attributes(args.datasets[0])
    msk = dxm.data_mask * dym.data_mask
    x, y = dxm.center_coords()
    write_hdf(args.o, dict(x=x[msk],
                           y=y[msk],
                           dx=dxm.values[msk],
                           dy=dym.values[msk],
                           mismatch_x=mxm.values[msk],
                           mismatch_y=mym.values[msk],
                           strength=stm.values[msk]),
              attrs=attrs)

