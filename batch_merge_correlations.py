import os
import itertools
import subprocess
from wvutil import get_region, get_sceneid, ensureisdir, get_baseline

REGIONS = {"Nioghalvfjerdsbrae"}
INPUTS = ["correlation-50m-256-4m/", "correlation-50m-256-8m/"]
OUTPUT = "correlation-50m-merge"

ensureisdir(OUTPUT)
file_lists = [os.listdir(dirname) for dirname in INPUTS]

master_list = [f for f in set(itertools.chain(*file_lists)) if get_region(get_sceneid(f)) in REGIONS]

for fnm in master_list:
    cmd = ["python", "merge-correlations.py"]
    for dirname, lst in zip(INPUTS, file_lists):
        if fnm in lst:
            cmd.append(os.path.join(dirname, fnm))
    cmd.extend(["-o", os.path.join(OUTPUT, fnm), "--resolution", "50.0"])
    dt = get_baseline(*map(get_sceneid, fnm.split("_")))
    cmd.extend(["--baseline", str(dt)])
    print(" ".join(cmd))
    subprocess.call(cmd)

