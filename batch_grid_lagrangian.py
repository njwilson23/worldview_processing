#! /usr/bin/env python
""" Grid Dh/Dt data """

import os
import h5py
import karta
import numpy as np
from meltpack.filt import smooth5

from wvutil import get_sceneid, get_region, get_date, ensureisdir

REGIONS = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}
DHDT_OUTPUT = "ldhdt-50m/"
DHDT_GRIDS = "ldhdt-grids-100m/"

SMOOTH_ITERATIONS = 3
OUTPUT_RESOLUTION = 100.0
OVERWRITE = True

ensureisdir(DHDT_GRIDS)

def read_hdf(fnm):
    with h5py.File(fnm, mode="r") as ds:
        x = ds["x"].value
        y = ds["y"].value
        DHDt = ds["DHDt"].value
        date0 = get_date(get_sceneid(ds.attrs["file_dem1"]))
        date1 = get_date(get_sceneid(ds.attrs["file_dem2"]))

    m = np.isnan(DHDt)
    return x[~m], y[~m], DHDt[~m], date0, date1

def choose_transform(x, y, res=100.0):
    """ from a set of coodinates, choose a reasonable geotransform """
    T = [(x.min()//res)*res-0.5*res, (y.min()//res)*res-0.5*res, res, res, 0.0, 0.0]
    return T

hdf_files = [f for f in os.listdir(DHDT_OUTPUT) if f.endswith(".h5")]

for fnm in [f for f in hdf_files if get_region(get_sceneid(f)) in REGIONS]:

    out_fnm = os.path.join(DHDT_GRIDS, os.path.splitext(fnm)[0]+".tif")

    if os.path.isfile(out_fnm) and not OVERWRITE:
        print("%s already exists" % os.path.split(out_fnm)[1])

    else:
        print("reading %s" % fnm)
        x, y, dH, date0, date1 = read_hdf(os.path.join(DHDT_OUTPUT, fnm))
        print("  number of records: %d" % len(x))

        if len(x) < 10:
            print("WARNING: too few records")
            continue

        T = choose_transform(x, y, res=OUTPUT_RESOLUTION)
        grid = karta.raster.gridpoints(x, y, dH, T, karta.crs.NSIDCNorth)
        if SMOOTH_ITERATIONS > 0:
            grid.values = smooth5(grid.values, SMOOTH_ITERATIONS)
        grid.to_geotiff(out_fnm, compress="LZW", TILED="YES")
        print("  written to %s" % out_fnm)

