# Projects

Nioghalvfjerdsbrae

Petermann

Ryder

Steensby

# Scripts

Described approximately in order of application:

`make_dems.py`

Behaves somewhat like a makefile, and constructs all of the clipped, projected,
and composited DEMs from ASP outputs.

`prepare_dems_gm.py`

**DEPRECATED**

Computes and applies bundle corrections, and optionally uses a Gauss-Markov
estimator as a smoother on DEM data

`fill_dem_holes.py`

Use GDAL to patch DEMs.

`compute_velocity_divergence.py`

Calculates h div(u) term of mass continuity equation.

`compute_correlation_offsets.py`

Use an image correlator to determine matching chips between paired scenes and
save to HDF datasets in `correlation-data/`.

`interpolate_correlations.py`

Fill in nodata regions in correlation results.

`compute_lagrangian_dhdt.py`

Calculates dh/dt + u.grad(h) term of mass continuity equation. Uses the HDF data
created by `compute_correlation_offsets.py` in order to subtract matching image
chips. Saves point data to `ldhdt-output/`

`grid_ldhdt.py`

Converts Dh/Dt output in HDF files to GeoTiff images.

`combine_terms.py`

Combines the results from `compute_velocity_divergence.py` and `grid_ldhdt.py`
into a map of melt rates.

## NotImplemented

`interpolate_dhdt.py`

Uses Gauss-Markov predictor to interpolate DEMs.

