#! /usr/bin/env python
import os
from libcollect import collect

for projectname in ("Nioghalvfjerdsbrae/", "Petermann/", "Ryder/", "Steensby/"):

    results_dirs = list(filter(lambda s: s.startswith("results_"),
                               os.listdir(projectname)))
    results_dirs.sort()
    results_dirs = [os.path.join(projectname, fnm) for fnm in results_dirs]

    collect(results_dirs,
            "out-IntersectionErr.tif",
            os.path.join(projectname, "error/"),
            "error",
            overwrite=False)

