#! /usr/bin/env python
""" Write correlation data to a grid using gridvel.py """

import os
import tempfile
import subprocess
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

from wvutil import get_sceneid, get_region, get_date, ensureisdir, get_best_outline

REGIONS = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}
CORRELATION_DATA = "correlation-interp-50m/"
OUTPUT_FOLDER = "correlation-velocities-100m/"
REDUCTION_RESOLUTION = 100.0
OVERWRITE = True
NPROCS = 1

ensureisdir(OUTPUT_FOLDER)

def runcmd(cmd):
    subprocess.call(cmd)

hdf_files = [os.path.join(CORRELATION_DATA, f)
             for f in os.listdir(CORRELATION_DATA)
             if f.endswith(".h5")]

temp_outlines = tempfile.mkdtemp()

try:
    with ThreadPoolExecutor(max_workers=NPROCS) as pool:

        for i, fnm in enumerate([f for f in hdf_files
                                   if get_region(get_sceneid(f)) in REGIONS]):

            region = get_region(get_sceneid(fnm))
            date1 = get_date(get_sceneid(os.path.split(fnm)[1].split("_", 1)[0]))
            date2 = get_date(get_sceneid(os.path.split(fnm)[1].split("_", 1)[1]))
            dt = (date2-date1).days

            # Save outline json to file
            ol, _temporal_mismatch = get_best_outline(region, date2)
            ol_fnm = os.path.join(temp_outlines, "{0}.geojson".format(i))
            ol.to_geojson(ol_fnm)

            prefix = os.path.join(OUTPUT_FOLDER,
                                  os.path.split(os.path.splitext(fnm)[0])[1])
            cmd = ["python", "gridvel.py",
                    fnm, prefix,
                    "--days", str(dt),
                    "--median-iterations", "5",
                    "--smooth-iterations", "20",
                    "--maximum-mismatch-factor", "0.3",
                    "--polymask", ol_fnm]
            if OVERWRITE:
                cmd.append("--overwrite")

            print(" ".join(cmd))
            pool.submit(runcmd, cmd)

finally:
    for fnm in os.listdir(temp_outlines):
        os.remove(os.path.join(temp_outlines, fnm))
        print("removing %s" % os.path.join(temp_outlines, fnm))
    os.rmdir(temp_outlines)

