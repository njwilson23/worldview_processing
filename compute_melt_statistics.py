import os
import numpy as np
import karta
import h5py

from wvutil import get_standard_outline

def union_bbox(bbs):
    return (min(bb[0] for bb in bbs), min(bb[1] for bb in bbs),
            max(bb[2] for bb in bbs), max(bb[3] for bb in bbs))

petermann_data = {"name": "Petermann", "surface_balance": -1.98}
niog_data = {"name": "Nioghalvfjerdsbrae", "surface_balance": -1.50}
steensby_data = {"name": "Steensby", "surface_balance": -0.74}
ryder_data = {"name": "Ryder", "surface_balance": -1.16}

for d in [petermann_data, niog_data, steensby_data, ryder_data]:
    d["adot"] = karta.read_geotiff("adot-composites/{0}-128m_20161101.tif".format(d["name"]))

ryder_data["smb"] = -1.16
steensby_data["smb"] = -0.76
petermann_data["smb"] = -1.98
niog_data["smb"] = -1.5


print("Loading outlines")
for data in [petermann_data, niog_data, ryder_data, steensby_data]:
    data["outline"] = get_standard_outline(data["name"])

niog_data["datum"] = 15.6
petermann_data["datum"] = 7.2
ryder_data["datum"] = 13.3
steensby_data["datum"] = 8.2

def compute_thickness(elev, datum):
    # use SimpleBand for performance reasons (faster point sampling)
    h = (elev[:,:]-datum)/(1-920.0/1028.0)
    h[h<0] = 0.0
    return karta.raster.RegularGrid(elev.transform, values=h, crs=elev.crs)

dirnames = ["niog-analysis-new", "ryder-analysis-new", "steensby-analysis-new", "petermann-analysis-new"]
data_dicts = [niog_data, ryder_data, steensby_data, petermann_data]

for dirname, dd in zip(dirnames, data_dicts):
    u = karta.raster.read_geotiff(os.path.join(dirname, "vel-composites-128m", "dx.tif"))
    v = karta.raster.read_geotiff(os.path.join(dirname, "vel-composites-128m", "dy.tif"))
    dd["uvel"] = u
    dd["vvel"] = v

niog_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/nioghalvfjerdsbrae_composite.tif")
ryder_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/ryder_composite.tif")
steensby_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/steensby_composite.tif")
petermann_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/petermann_composite.tif")

for data in [niog_data, ryder_data, steensby_data, petermann_data]:
    data["dem"].set_nodata_value(np.nan)

niog_data["thickness"] = compute_thickness(niog_data["dem"], niog_data["datum"])
ryder_data["thickness"] = compute_thickness(ryder_data["dem"], ryder_data["datum"])
petermann_data["thickness"] = compute_thickness(petermann_data["dem"], petermann_data["datum"])
steensby_data["thickness"] = compute_thickness(steensby_data["dem"], steensby_data["datum"])

MWE = 0.92
for data_dict in (ryder_data, steensby_data, niog_data, petermann_data):
    adot = data_dict["adot"]
    pixel_area = adot.resolution[0] * adot.resolution[1]
    adot = adot[adot.data_mask]
    mean = np.mean(adot)
    low = np.percentile(-adot, 2)
    high = np.percentile(-adot, 98)
    flux = np.sum(adot * pixel_area)
    h = data_dict["thickness"].mask_by_poly(data_dict["outline"])
    volume = np.sum(h[h.data_mask] * h.resolution[0] * h.resolution[1])
    print("""{name}
  mean melt:       {mean:.2f}
  melt 2% and 98%: {low:.2f} {high:.2f}
  melt flux:       {flux:.2f}  ({fluxfrac:.3f}%)
  volume:          {volume:.2f}
  """.format(name=data_dict["name"], mean=mean, low=low, high=high,
             flux=flux/1e9*MWE, volume=volume/1e9*MWE, fluxfrac=flux/volume*100))

#####################################
#                                   #
#  SAVE RESULTS TO AN HDF DATASET   #
#                                   #
#####################################

# def _scalar_flux(grid, v):
#     return np.sum(grid.resolution[0] * grid.resolution[1] * grid.data_mask.astype(np.int32) * v)
# 
# def _flux(adot):
#     adot_ = adot[adot.data_mask]
#     pixel_area = adot.resolution[0]*adot.resolution[1]
#     return np.sum(adot_ * pixel_area)
# 
# def _volume(h, outline):
#     h_ = h.mask_by_poly(outline)[h.data_mask]
#     return np.nansum(h_ * h.resolution[0] * h.resolution[1])
# 
# data_dicts = [niog_data, petermann_data, ryder_data, steensby_data]
# meanmelt = [np.mean(d["adot"][d["adot"].data_mask]) for d in data_dicts]
# perc2 = [np.percentile(-d["adot"][d["adot"].data_mask], 2) for d in data_dicts]
# perc98 = [np.percentile(-d["adot"][d["adot"].data_mask], 98) for d in data_dicts]
# fluxes = [_flux(d["adot"]) for d in data_dicts]
# volumes = [_volume(d["thickness"], d["outline"]) for d in data_dicts]
# sfcflux = [_scalar_flux(d["adot"], d["surface_balance"]) for d in data_dicts]
# 
# with h5py.File("/home/natw/Documents/papers/meltrates/data/statistics_2.h5", "w") as ds:
# 
#     for grpname, data in zip(["mean", "perc2", "perc98", "flux", "tonguevolume", "surfaceflux"],
#                              [meanmelt, perc2, perc98, fluxes, volumes, sfcflux]):
#         g = ds.create_group(grpname)
# 
#         for i, tonguename in enumerate(["Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"]):
#             d = g.create_dataset(tonguename, (1,))
#             d[0] = data[i]
