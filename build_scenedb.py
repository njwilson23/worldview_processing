#! /usr/bin/env python
""" Construct a database of WorldView scenes

Headers:
    sceneid     pairid  acqdate     region
"""

import os
import re
import karta
import dateutil.parser
from xml.etree import ElementTree
from tinydb import TinyDB, Query

# ------------------------------------------------------------------------------
# Compile important data from PGC shapefiles into a scene database
# ------------------------------------------------------------------------------

shpfiles = ["PGC_files/Nioghalvfjerdsbrae_Stereo/Nioghalvfjerdsbrae_Stereo_Imagery_All.shp",
            "PGC_files/Peterman_Stereo/Petermann_Stereo_cc20.shp",
            "PGC_files/DG_Stereo/DG_Stereo_2015dec01.shp"]

if os.path.isfile("scenedb.json"):
    os.remove("scenedb.json")
db = TinyDB("scenedb.json")

pt_niog = karta.Point((-21.21947,79.50821), crs=karta.crs.LonLatWGS84)
pt_petermann = karta.Point((-60.72452,80.68213), crs=karta.crs.LonLatWGS84)
pt_ryder = karta.Point((-8.5e4, -9.0e5), crs=karta.crs.NSIDCNorth)
pt_steensby = karta.Point((-15.4e4, -9.1e5), crs=karta.crs.NSIDCNorth)

for shpfile in shpfiles:
    geoms = karta.read_shapefile(shpfile)
    for g in geoms:
        json = g.as_geojson(indent=None)
        sceneid = g.properties["catalogid"]
        pairid = g.properties["stereopair"]
        aqdate = dateutil.parser.parse(g.properties["acqdate"])
        if g.shortest_distance_to(pt_niog) < 100e3:
            region = "Nioghalvfjerdsbrae"
        elif g.shortest_distance_to(pt_petermann) < 50e3:
            region = "Petermann"
        elif g.contains(pt_ryder):
            region = "Ryder"
        elif g.contains(pt_steensby):
            region = "Steensby"
        elif g.shortest_distance_to(pt_ryder) < g.shortest_distance_to(pt_steensby):
            region = "Ryder"
        else:
            region = "Steensby"
    
        db.insert({"sceneid": sceneid,
                   "pairid": pairid,
                   "aqdate": aqdate.isoformat(),
                   "region": region,
                   "firstlinetime": "",
                   "geometry": json})

# ------------------------------------------------------------------------------
# Update database with exact aquisition times read from the scene xml files
# ------------------------------------------------------------------------------

def get_sceneid(fnm):
    m = re.search("[A-Z0-9]{16}", fnm)
    if m is None:
        raise ValueError("cannot parse scene id from %s" % fnm)
    else:
        return fnm[m.start():m.end()]

xmlfiles = []
scenes_covered = []
for dirpath, _, filenames in os.walk('scene-xml'):
    for fnm in filenames:
        try:
            s = get_sceneid(fnm)
            if s not in scenes_covered:
                xmlfiles.append(os.path.join(dirpath, fnm))
                scenes_covered.append(s)
            break
        except ValueError:
            continue

print("updating scenes from {0} XML files".format(len(xmlfiles)))

scene = Query()

for xmlfnm in xmlfiles:
    tree = ElementTree.parse(xmlfnm)
    sceneid = get_sceneid(xmlfnm)
    try:
        timestr = tree.find("IMD/IMAGE/FIRSTLINETIME").text
    except AttributeError:
        print("IMD/IMAGE/FIRSTLINETIME not found in {0}".format(xmlfnm))

    db.update({"firstlinetime": timestr}, scene.sceneid == sceneid)


