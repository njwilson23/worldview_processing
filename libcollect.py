import os
import re
import shutil

def get_fileid(s):
    m = re.search("[A-Z0-9]{16}", s)
    return s[m.start():m.end()]

def collect(directories, fnm, newdir, prefix, overwrite=False, fileid_func=get_fileid):
    """ Copy files in *directories* matching the pattern `directory/fnm`
    to the new directory *newdir*, renaming as *prefix*+*fileid*.tif where
    *fileid* is taken from the input directory.
    """

    if not os.path.isdir(newdir):
        os.makedirs(newdir)

    for directory in directories:

        try:

            f_old = os.path.join(directory, fnm)

            if not os.path.isfile(f_old):
                raise IOError("%s not found" % f_old)

            fileid = fileid_func(directory)
            ext = os.path.splitext(fnm)[1]
            f_new = os.path.join(newdir, prefix+"_"+fileid+ext)

            if os.path.isfile(f_new) and not overwrite:
                raise IOError("%s already exists" % f_new)
            else:
                shutil.copyfile(f_old, f_new)

        except IOError as e:
            print(e)

