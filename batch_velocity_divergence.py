#! /usr/bin/env python
import os
import tempfile
import shutil
import subprocess
from wvutil import get_sceneid, get_region, get_date, get_baseline, ensureisdir, get_best_outline

DEMDIR = "dems-100m-aligned/"
VELDIR = "correlation-velocities-100m/"
SAVEDIR = "hdivu-100m/"
REGIONS = {"Nioghalvfjerdsbrae", "Ryder", "Steensby", "Petermann"}
OVERWRITE = True

ensureisdir(SAVEDIR)

# copied from EGM 2008
geoid_heights = {
        "Nioghalvfjerdsbrae": 29.4,
        "Petermann": 14.9,
        "Ryder": 21.16,
        "Steensby": 19.21,
        }

class NoSuitableDataset(Exception):
    def __init__(self):
        return

def split_scids(fnm):
    s = fnm.split("_", 1)
    return get_sceneid(s[0]), get_sceneid(s[1])

def get_velocity_for_dem(dem, veldir):
    """ given the filename for a DEM, return a pair of geotiffs with the best
    representative velocity.
    
    chooses the shelf velocity that that fits within a pair with a baseline
    nearest 90 days
    """

    def spans(trial_date, begin, end):
        """ returns true if begin < trial_date < end """
        return begin <= trial_date <= end

    scid = get_sceneid(dem)
    region = get_region(scid)
    date = get_date(scid)

    # 1. get available velocity datasets
    pairs = [split_scids(f) for f in os.listdir(veldir) if f.endswith("_dx.tif")
                    and get_region(get_sceneid(f)) == region]

    # 2. filter by the ones spanning the dataset
    candidate_pairs = [p for p in pairs
                         if spans(date, get_date(p[0]), get_date(p[1]))]

    if len(candidate_pairs) == 0:
        raise NoSuitableDataset

    # 3. compute baselines and choose the one closest to 90 days
    baselines = [get_baseline(*p) for p in candidate_pairs]
    mismatch = [abs(b-90) for b in baselines]
    best_pair = candidate_pairs[mismatch.index(min(mismatch))]
    
    # 4. compose filenames
    fnms = [os.path.join(veldir, "{0}_{1}_dx.tif".format(*best_pair)),
            os.path.join(veldir, "{0}_{1}_dy.tif".format(*best_pair))]
    return fnms

dems = [fnm for fnm in os.listdir(DEMDIR) if fnm.endswith(".tif") and
               get_region(get_sceneid(fnm)) in REGIONS]

temp_outline_dir = tempfile.mkdtemp()

try:
    for i, dem in enumerate(dems):

        outfnm = os.path.join(SAVEDIR, dem)
        if os.path.isfile(outfnm) and not OVERWRITE:
            print("Skipping {0}".format(dem))
            continue

        region = get_region(get_sceneid(dem))
        date = get_date(get_sceneid(dem))
        ol, _ = get_best_outline(region, date)
        ol_fnm = os.path.join(temp_outline_dir, "{0}.shp".format(i))
        ol.to_shapefile(ol_fnm)

        try:
            velocity_fnms = get_velocity_for_dem(dem, VELDIR)
        except NoSuitableDataset:
            print("cannot find appropriate velocity dataset for {0} ({1})".format(
                dem, region))
            continue

        print("Processing %s (%s)" % (dem, region))
        cmd = ["python", "hdivu.py", "--is-elevation",
                "--datum", str(geoid_heights[region]),
                "--outline", ol_fnm,
                os.path.join(DEMDIR, dem),
                velocity_fnms[0], velocity_fnms[1],
                outfnm]

        if OVERWRITE:
            cmd.append("--overwrite")

        res = subprocess.call(cmd)

        if res != 0:
            print("execution failed:")
            print(" ".join(cmd))

finally:
    shutil.rmtree(temp_outline_dir)
