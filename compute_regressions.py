import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import LeaveOneOut, cross_val_score
# from scipy.optimize import minimize
import pandas
import karta
from karta import RegularGrid, read_geotiff, read_geojson
import mapping
import meltpack
from wvutil import get_standard_outline


DATAFRAMES = {
    "79N": pandas.read_csv("/home/natw/Documents/papers/meltrates/data/meltpixels-79N-512m.csv"),
    "RG": pandas.read_csv("/home/natw/Documents/papers/meltrates/data/meltpixels-RG-512m.csv"),
    "PG": pandas.read_csv("/home/natw/Documents/papers/meltrates/data/meltpixels-PG-512m.csv")
}

from os.path import join
_path = "/home/natw/Documents/Greenland_data/WorldView/"

NAMES = ["79N", "RG", "PG"]

DATUMS = {
    "79N": 15.2,
    "RG": 12.1,
    "PG": 7.24
}

OUTLINES = {
    "79N": get_standard_outline("Nioghalvfjerdsbrae"),
    "RG": get_standard_outline("Ryder"),
    "PG": get_standard_outline("Petermann")
}

GRIDS = {
    "79N": {
        "dem": read_geotiff(join(_path, "dem-composites-512m/Nioghalvfjerdsbrae-512m.tif")),
        "massbalance": read_geotiff(join(_path, "adot-composites/Nioghalvfjerdsbrae-512m_20160921.tif")),
        "driving": read_geotiff(join(_path, "thermal-driving-512m/Nioghalvfjerdsbrae.tif")),
    },
    "RG": {
        "dem": read_geotiff(join(_path, "dem-composites-512m/Ryder-512m.tif")),
        "massbalance": read_geotiff(join(_path, "adot-composites/Ryder-512m_20160921.tif")),
        "driving": read_geotiff(join(_path, "thermal-driving-512m/Ryder.tif")),
    },
    "PG": {
        "dem": read_geotiff(join(_path, "dem-composites-512m/Petermann-512m.tif")),
        "massbalance": read_geotiff(join(_path, "adot-composites/Petermann-512m_20160921.tif")),
        "driving": read_geotiff(join(_path, "thermal-driving-512m/Petermann.tif")),
    },
}

_shp = karta.read_shapefile(join(_path, "rapid-melt-regions.shp"))
namecodes = {"Petermann": "PG", "Nioghalvfjerdsbrae": "79N", "Ryder": "RG", "Steensby": "SG"}
RAPID_MELT_POLY = {namecodes[g.properties["Glacier"]]: g for g in _shp}

for glacier in GRIDS:
    dem = GRIDS[glacier]["dem"]
    g = karta.RegularGrid(dem.transform, values=np.ones(dem.size, dtype=np.int8), crs=dem.crs)
    g.mask_by_poly(RAPID_MELT_POLY[glacier], inplace=True)
    g[g[:,:]==g.nodata] = 0
    GRIDS[glacier]["highmeltzone"] = g

R = 917 / 1028

for glacier in GRIDS:
    GRIDS[glacier]["dem"].mask_by_poly(OUTLINES[glacier], inplace=True)
    GRIDS[glacier]["depth"] = GRIDS[glacier]["dem"].apply(
        lambda a: -(a-DATUMS[glacier])*R / (1-R))
    _slope = karta.raster.slope(GRIDS[glacier]["depth"])
    _slope[:,:] = meltpack.filt.smooth5(_slope[:,:].astype(np.float64), 3)
    GRIDS[glacier]["slopebase"] = _slope


def print_model_performance(model, xvars, yvar):
    """ Fit a model to combined data and print the combined score and the score for each individual glacier """
    X = np.r_[DATAFRAMES["79N"][xvars[0]].values[:,np.newaxis],
              DATAFRAMES["RG"][xvars[0]].values[:,np.newaxis],
              DATAFRAMES["PG"][xvars[0]].values[:,np.newaxis]]
    y = np.r_[DATAFRAMES["79N"][yvar].values[:,np.newaxis],
              DATAFRAMES["RG"][yvar].values[:,np.newaxis],
              DATAFRAMES["PG"][yvar].values[:,np.newaxis]]
    for xv in xvars[1:]:
        X = np.hstack([X, np.r_[DATAFRAMES["79N"][xv].values[:,np.newaxis],
                                DATAFRAMES["RG"][xv].values[:,np.newaxis],
                                DATAFRAMES["PG"][xv].values[:,np.newaxis]]])
    model.fit(X, y)
    print("parameters:", model.coef_, end=" ")
    try:
        print(model.intercept_)
    except AttributeError:
        print()
    print("combined score: {0:.3f}".format(model.score(X, y)))

    for name in NAMES:
        X = DATAFRAMES[name][xvars[0]].values[:,np.newaxis]
        y = DATAFRAMES[name][yvar].values[:,np.newaxis]
        for xv in xvars[1:]:
            X = np.hstack([X, DATAFRAMES[name][xv].values[:,np.newaxis]])
        print("{0:14s}: {1:.3f}".format(name, model.score(X, y)))
    return

print("Depth + slope")
print_model_performance(LinearRegression(), ["depth", "slopebase"], "massbalance")

print("\nThermal driving (linear) + slope")
print_model_performance(LinearRegression(), ["driving", "slopebase"], "massbalance")

print("\nDepth")
print_model_performance(LinearRegression(), ["depth"], "massbalance")

print("\nSlope")
print_model_performance(LinearRegression(), ["slopebase"], "massbalance")

print("\nThermal driving (linear)")
print_model_performance(LinearRegression(), ["driving"], "massbalance")
