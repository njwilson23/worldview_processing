#! /usr/bin/env python

from __future__ import print_function

import os
import subprocess
from wvutil import get_baseline, ensureisdir, get_sceneid, get_region, get_tidal_height
from scenepairs import scenepairs

DEMDIR = "dems-10m-aligned/"
CORRDIR = "correlation-50m-merge/"
SAVEDIR = "ldhdt-50m/"

REGIONS = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}
RHOI = 920.0
RHOW = 1028.0
MAX_MISMATCH = 0.5
OVERWRITE = True

ensureisdir(SAVEDIR)

maskpolys = {
        "Nioghalvfjerdsbrae": "Nioghalvfjerdsbrae/niog_bedrock.shp",
        "Petermann": "Petermann/petermann_bedrock.shp",
        "Ryder": "Ryder/ryder_bedrock.shp",
        "Steensby": "Steensby/steensby_bedrock.shp",
        }

def getfile(fragment, dirname, ext=None):
    for fnm in os.listdir(dirname):
        if fragment in fnm:
            if (ext is None) or fnm.endswith(ext):
                return os.path.join(dirname, fnm)
    raise KeyError("*{0}* not in {1}".format(fragment, dirname))

def runcmd(cmd):
    ret = subprocess.call(cmd)
    if ret != 0:
        print("failed to execute\n{0}".format(" ".join(cmd)))

for region in REGIONS:
    for pair in scenepairs[region]:
        print(*pair)

        try:
            demfnm1 = getfile(pair[0], DEMDIR, ext=".tif")
            demfnm2 = getfile(pair[1], DEMDIR, ext=".tif")
            corrfnm = getfile("{0}_{1}".format(*pair), CORRDIR)
        except KeyError:
            print("cannot find inputs for {0}, {1}".format(*pair))
            continue

        outfnm = os.path.join(SAVEDIR, "{0}_{1}.h5".format(*pair))
        if os.path.isfile(outfnm) and not OVERWRITE:
            print("{0} already exists".format(outfnm))
            continue

        region = get_region(get_sceneid(pair[0]))
        mask = maskpolys[region]

        dt = get_baseline(*pair)
        tide1 = get_tidal_height(pair[0])
        tide2 = get_tidal_height(pair[1])
        print("  Tides: {0:5.2f} {1:5.2f}".format(tide1, tide2))

        cmd = ["python", "lagrangian-diff.py",
               "--baseline", str(dt),
               "--to-ice-thickness",
               "--rhow", str(RHOW),
               "--rhoi", str(RHOI),
               "--maximum-mismatch", str(MAX_MISMATCH),
               "--reference-poly", mask,
               "--tidal-heights", str(tide1), str(tide2),
               "--overwrite",
               demfnm1, demfnm2, corrfnm, outfnm]

        runcmd(cmd)

