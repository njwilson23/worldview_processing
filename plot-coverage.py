#! /usr/bin/env python
import os
import matplotlib.pyplot as plt
import karta
import mapping
import numpy as np
import argparse

def grid_stats(gridmean, grids):
    """ Returns the variance and pixel counts for a list of grids relative to a sample mean """
    # Check grid class
    if not all(isinstance(grid, karta.RegularGrid) for grid in grids):
        raise NotImplementedError("All grids must be type RegularGrid")

    T = grids[0].transform
    # Check grid stretch and skew
    for i, grid in enumerate(grids[1:]):
        if grid.transform[2:6] != T[2:6]:
            raise NotImplementedError("grid %d transform stretch and skew "
                    "does not match grid 1" % (i+2,))

    # Check grid offset
    excmsg = "grid %d not an integer translation from grid 1"
    for i, grid in enumerate(grids[1:]):
        if ((grid.transform[0]-T[0]) / float(T[2])) % 1 > 1e-15:
            raise NotImplementedError(excmsg % (i+2,))
        if ((grid.transform[1]-T[1]) / float(T[3])) % 1 > 1e-15:
            raise NotImplementedError(excmsg % (i+2,))

    xmin, xmax, ymin, ymax = gridmean.get_extent(reference="edge")
    ny, nx = gridmean.size

    # Allocate data array and copy each grid's data
    typ = gridmean.bands[0].dtype
    values = np.zeros([ny, nx], dtype=typ)
    counts = np.zeros([ny, nx], dtype=np.int16)
    for grid in grids:
        _xmin, _xmax, _ymin, _ymax = grid.get_extent(reference='edge')
        offx = int((_xmin-xmin) / T[2])
        offy = int((_ymin-ymin) / T[3])
        _ny, _nx = grid.size

        mask = grid.data_mask
        counts[offy:offy+_ny,offx:offx+_nx][mask] += 1
        values[offy:offy+_ny,offx:offx+_nx][mask]
        values[offy:offy+_ny,offx:offx+_nx][mask] += \
            typ((grid[mask]-gridmean[offy:offy+_ny, offx:offx+_nx][mask])**2)
        del mask

    validcountmask = (counts!=0)
    values[validcountmask] = values[validcountmask] / counts[validcountmask]
    values[~validcountmask] = grids[0].nodata
    Tmerge = [xmin, ymin] + list(T[2:])
    return (karta.RegularGrid(Tmerge, values=values, crs=gridmean.crs, nodata_value=gridmean.nodata),
            karta.RegularGrid(Tmerge, values=counts, crs=gridmean.crs, nodata_value=-1))

if __name__ ==  "__main__":

    parser = argparse.ArgumentParser(description="Creates a 3-paned plot of mean elevation, elevation variance, and scene counts for a region. ")
    #parser.add_argument("region", action="store", help="Nioghalvfjerdsbrae|Peterman|Ryder|Steensby")
    parser.add_argument("dir", action="store", help="Directory of (DEM) grids to average")
    parser.add_argument("--outline", action="store", help="GeoJSON file containing glacier outline")
    parser.add_argument("--cmap", action="store", default="coolwarm_r", help="GeoJSON file containing glacier outline")
    parser.add_argument("-o", action="store", default=None, help="Optional file to write plot to. Otherwise, plot is displayed.")
    args = parser.parse_args()

    fnms = map(lambda s: os.path.join(args.dir, s),
            filter(lambda s: s.endswith(".tif"), os.listdir(args.dir)))

    print("reading gridded data")
    def overlaps_outline(g, ol):
        gmsked = g.mask_by_poly(ol)
        arr = gmsked[gmsked.data_mask]
        return arr.size > 0

    def is_valid_data(g):
        v = np.median(g[g.data_mask])
        return (-1000 < v < 1000)

    if len(args.outline) != 0:
        ol = karta.read_geojson(args.outline)[0]
        filt = lambda g: (overlaps_outline(g, ol) and is_valid_data(g))
    else:
        filt = is_valid_data

    grids = list(filter(filt, map(lambda s: karta.read_geotiff(s), fnms)))

    print("computing mean field")
    hmean = karta.raster.merge(grids)
    print("computing variance and counts")
    hvar, hcount = grid_stats(hmean, grids)

    fig = plt.figure(figsize=(10, 7))
    ax1 = fig.add_subplot(1, 3, 1)
    ax2 = fig.add_subplot(1, 3, 2)
    ax3 = fig.add_subplot(1, 3, 3)

    vmin = np.percentile(hmean[hmean.data_mask], 5)
    vmax = np.percentile(hmean[hmean.data_mask], 95)
    m1 = mapping.plot(hmean, ax=ax1, vmin=vmin, vmax=vmax, cmap=args.cmap)
    vmax = np.percentile(hvar[hvar.data_mask], 95)
    m2 = mapping.plot(hvar, ax=ax2, vmin=0, vmax=vmax, cmap=plt.cm.inferno)
    m3 = mapping.plot(hcount, vmin=0, ax=ax3)

    plt.colorbar(m1, ax=ax1, orientation="horizontal")
    plt.colorbar(m2, ax=ax2, orientation="horizontal")
    plt.colorbar(m3, ax=ax3, orientation="horizontal")

    if len(args.outline) != 0:
        for ax in (ax1, ax2, ax3):
            mapping.plot(ol, edgecolor="steelblue", linewidth=2, ax=ax, crs=hmean.crs)

    for ax in (ax1, ax2, ax3):
        for labelset in (ax.get_xticklabels(), ax.get_yticklabels()):
            for label in labelset:
                label.set_rotation(30)
                label.set_size(8)

    plt.tight_layout(pad=0)

    if args.o is not None:
        plt.savefig(args.o)
    else:
        plt.show()
