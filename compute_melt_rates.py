import os
import datetime
import karta
import numpy as np

import mapping
import meltpack
import wvutil

ρi = 920.0
ρf = 920.0
ρw = 1028.0

MONTHS = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE",
          "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"]

def union_bbox(bbs):
    return (min(bb[0] for bb in bbs), min(bb[1] for bb in bbs),
            max(bb[2] for bb in bbs), max(bb[3] for bb in bbs))

def smb_correction(dh, df):
    return dh + df/(1-ρf/ρw) - df/(1-ρi/ρw)

def exclude_bad_pixels(a):
    a[np.abs(a) > 100] = np.nan
    return a

def dates_from_filename(fnm):
    scid1 = wvutil.get_sceneid(fnm.split("_")[0])
    scid2 = wvutil.get_sceneid(fnm.split("_")[1])
    return wvutil.get_date(scid1), wvutil.get_date(scid2)

def compute_month_fracs(date1, date2):
    """ return a list of twelve floating point numbers, indicating the multiple
    of a month covered by the span date1 - date2

    e.g. Jun 15 2013 - October 31 2013 would be
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0]

    for [Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Oct  Sep  Nov  Dec].
    """
    DAYS_PER_MONTH = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    t = date1
    fracs = [0.0 for _ in range(12)]
    while t < date2:
        mon = t.month
        next_t = min(datetime.datetime(t.year + t.month//12, (t.month)%12+1, 1, tzinfo=t.tzinfo), date2)
        fracs[mon-1] += (next_t-t).days / DAYS_PER_MONTH[mon-1]
        t = next_t
    return fracs

def month_as_vec(month):
    vec = [0.0 if i+1 != month else 1.0 for i in range(12)]
    return vec

def fnm2baseline(fnm):
    return wvutil.get_baseline(*map(wvutil.get_sceneid, fnm.split("_")))

if __name__ == "__main__":

    # for integrated RACMO estimates see notebook `analysis-racmo.ipynb`

    petermann_data = {"name": "Petermann",
            "smb": -1.98,
            "hdivu_dir": "petermann-analysis-new/hdivu-128m/",
            "ldhdt_dir": "petermann-analysis-new/ldhdt-grids-128m/"}

    niog_data = {"name": "Nioghalvfjerdsbrae",
            "smb": -1.5,
            "hdivu_dir": "niog-analysis-new/hdivu-128m/",
            "ldhdt_dir": "niog-analysis-new/ldhdt-grids-128m/"}

    steensby_data = {"name": "Steensby",
            "smb": -0.76,
            "hdivu_dir": "steensby-analysis-new/hdivu-128m/",
            "ldhdt_dir": "steensby-analysis-new/ldhdt-grids-128m/"}

    ryder_data = {"name": "Ryder",
            "smb": -1.16,
            "hdivu_dir": "ryder-analysis-new/hdivu-128m/",
            "ldhdt_dir": "ryder-analysis-new/ldhdt-grids-128m/"}

    data_dicts = [petermann_data, niog_data, ryder_data, steensby_data]

    print("Loading velocity divergence terms")
    for data in data_dicts:
        dirname = data["hdivu_dir"]
        print("\t{0}".format(data["name"]))
        fnms = list(filter(lambda f: f.endswith(".tif") and not wvutil.is_blacklisted(wvutil.get_sceneid(f)),
                           map(lambda f: os.path.join(dirname, f), os.listdir(dirname))))
        grids = list(map(lambda f: karta.raster.read_geotiff(f).apply(exclude_bad_pixels), fnms))
        bb = union_bbox([g.bbox for g in grids])
        grids = [g.resize(bb) for g in grids]
        month_vectors = [month_as_vec(wvutil.get_date(wvutil.get_sceneid(f)).month) for f in fnms]

        monthly_averages = []
        for month in range(0, 12):
            weights = [mv[month]/sum(mv) for mv in month_vectors]
            if sum(weights) != 0.0:
                monthly_averages.append(karta.raster.merge(grids, weights))
            else:
                print("  warning: no hdivu data for "
                      "{0}, {1}".format(MONTHS[month], dirname))

        hdivu = karta.raster.merge(monthly_averages)
        hdivu[:,:] = meltpack.filt.smooth5(hdivu[:,:].astype(np.float64), 25)

        data["hdivu"] = hdivu

    print("Loading outlines")
    for data in data_dicts:
        ol = wvutil.get_standard_outline(data["name"])
        data["outline"] = ol

    print("Loading Dh/Dt terms")
    for data in data_dicts:
        dirname = data["ldhdt_dir"]
        print("\t{0}".format(data["name"]))
        fnms = list(map(lambda f: os.path.join(dirname, f),
                        filter(lambda s: s.endswith(".tif")
                                and fnm2baseline(s) > 180
                                and not wvutil.is_blacklisted(wvutil.get_sceneid(s)),
                                os.listdir(dirname))))
        grids = list(map(lambda f: karta.raster.read_geotiff(f).apply(exclude_bad_pixels), fnms))

        if data["name"] == "Petermann":
            baselines = [fnm2baseline(f) for f in fnms]
            pixelcounts = [np.sum(g.data_mask) for g in grids]
            fnms = [f for (f,b,pc) in zip(fnms, baselines, pixelcounts)
                        if b > 180 and pc > 10000]
            grids = [g for (g,b,pc) in zip(grids, baselines, pixelcounts)
                        if b > 180 and pc > 10000]

        bb = union_bbox([g.bbox for g in grids])
        grids = [g.resize(bb) for g in grids]
        month_vectors = [compute_month_fracs(*dates_from_filename(f)) for f in fnms]

        monthly_averages = []
        for month in range(0, 12):
            weights = [mv[month]/sum(mv) for mv in month_vectors]
            if sum(weights) != 0.0:
                monthly_averages.append(karta.raster.merge(grids, weights))
                if month in (6, 7, 8):
                    monthly_averages[-1].apply(lambda a: a-data["smb"]/3, inplace=True)
            else:
                print("  critical warning: no dhdt data for "
                      "{0}, {1}".format(MONTH[month], dirname))

        dhdt = karta.raster.merge(monthly_averages) \
                .mask_by_poly(data["outline"])
        dhdt[:,:] = meltpack.filt.smooth5(dhdt[:,:].astype(np.float64), 2)

        data["dhdt"] = dhdt

    for data in data_dicts:
        try:
            dhdt = data["dhdt"]
            hdivu = data["hdivu"]

            bb = union_bbox([dhdt.bbox, hdivu.bbox])
            dhdt_rs = dhdt.resize(bb)
            hdivu_rs = hdivu.resize(bb)

            data["adot"] = dhdt_rs + hdivu_rs
            now_str = datetime.datetime.now().strftime("%Y%m%d")
            fnm = "adot-composites/{0}-128m_{1}.tif".format(data["name"], now_str)
            print("writing {0}".format(fnm))
            data["adot"].to_geotiff(fnm)
        except KeyError as e:
            print(e)

