#! /usr/bin/env python
#
# Command line tool for computing velocity divergence for an ice shelf
#
# Inputs:
# - ice thickness or elevation (GeoTiff)
# - ice velocity (GeoTiff x2)
#
# Outputs:
# - h div(u) (GeoTiff)

import argparse
import os
import subprocess
import numpy as np
import karta
import meltpack

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=
            """Compute the h div(u) term for ice shelf mass balance.
            """)
    parser.add_argument("thickness", action="store", metavar="THICKNESS",
            help="Ice shelf thickness grid or ice shelf elevation (see "
                 "--is-elevation, --datum)")
    parser.add_argument("uvel", action="store", metavar="EASTWEST_VELOCITY",
            help="Horizontal (u) velocity grid")
    parser.add_argument("vvel", action="store", metavar="SOUTHNORTH_VELOCITY",
            help="Vertical (v) velocity grid")
    parser.add_argument("out", action="store", metavar="OUTPUT", type=str,
            help="Output filename")
    parser.add_argument("--is-elevation", action="store_true", default=False,
            help="Indicates that elevation was provided rather than ice "
                 "thickness")
    parser.add_argument("--rhoi", action="store", default=920.0,
            help="Specify ice density (default 920 kg/m^3)")
    parser.add_argument("--rhow", action="store", default=1028.0,
            help="Specify water density (default 1028 kg/m^3)")
    parser.add_argument("--datum", action="store", default=0.0, type=float,
            help="Specify the mean sea level height above the ellipsoid "
                 "(assumed to be 0.0, but can vary by tens of meters)")
    parser.add_argument("--outline", action="store",
            help="Provide a singlepart vector polygon to clip output.")
    parser.add_argument("--overwrite", action="store_true", default=False,
            help="Permit output to be overwritten if it exists")

    args = parser.parse_args()

    # File checks
    if os.path.isfile(args.out) and not args.overwrite:
        raise FileExistsError("{0} already exists".format(args.out))

    if not all([os.path.isfile(args.thickness),
                os.path.isfile(args.uvel),
                os.path.isfile(args.vvel)]):
        raise FileNotFoundError("inputs missing")

    # Read data
    thick = karta.read_geotiff(args.thickness)
    thick[~thick.data_mask] = np.nan

    if args.is_elevation:
        s2h = 1.0 / (1.0-args.rhoi/args.rhow)   # transfer ceofficient
        msk = thick.data_mask
        thick[msk] = (thick[msk] - args.datum) * s2h

    uvel = karta.read_geotiff(args.uvel)
    vvel = karta.read_geotiff(args.vvel)

    uvel[:,:] = np.where(uvel.data_mask, uvel[:,:], np.nan)
    vvel[:,:] = np.where(vvel.data_mask, vvel[:,:], np.nan)
    uvel._nodata = np.nan
    vvel._nodata = np.nan

    if (thick.resolution != uvel.resolution) or (thick.resolution != vvel.resolution):
        raise ValueError("grids must have a common resolution")

    thick[:,:] = np.where(thick.data_mask, thick[:,:], np.nan)
    thick._nodata = np.nan
    thick[:,:] = meltpack.filt.smooth5(thick[:,:].astype(np.float64), 20).astype(np.float32)
    dx, dy = thick.resolution

    # Warning!
    # If velocity and thickness are not co-registered, adjust thickness so that it is
    # Will cause errors of up to half a cell dimension
    mx = ((thick.transform[0] - uvel.transform[0]) / dx) % 1
    my = ((thick.transform[1] - uvel.transform[1]) / dy) % 1

    if (mx != 0) or (my != 0):
        print("Warning: thickness being shifted to align with velocity grid")
    if mx != 0:
        print("\teasting offset:  {0} cells".format(mx))
    if my != 0:
        print("\tnorthing offset: {0} cells".format(my))

    T = thick.transform
    if mx < 0.5:
        thick._transform = (T[0]-mx*dx, T[1], T[2], T[3], T[4], T[5])
    else:
        thick._transform = (T[0]+(1-mx)*dx, T[1], T[2], T[3], T[4], T[5])

    T = thick.transform
    if my < 0.5:
        thick._transform = (T[0], T[1]-my*dy, T[2], T[3], T[4], T[5])
    else:
        thick._transform = (T[0], T[1]+(1-my)*dy, T[2], T[3], T[4], T[5])

    # Clip to a common bounding box
    try:
        xmin, ymin, xmax, ymax = meltpack.utilities.overlap_bbox(uvel.bbox, thick.bbox)
    except ValueError as e:
        print("failure:", str(e))
    uvelc = uvel.clip(xmin, xmax, ymin, ymax)
    vvelc = vvel.clip(xmin, xmax, ymin, ymax)
    thickc = thick.clip(xmin, xmax, ymin, ymax)

    for sz in uvelc.size:
        if sz < 3:
            raise ValueError("overlapping grid dimension impractically small")

    divu = meltpack.divergence.divergence(dx, dy,
                    np.ones(uvelc.size, dtype=np.float64),
                    uvelc[:,:].astype(np.float64),
                    vvelc[:,:].astype(np.float64))

    hdivu = thickc[1:-1,1:-1]*divu
    hdivu = meltpack.filt.smooth5(hdivu, 5).astype(np.float32)

    T = [thickc.transform[0]+dx, thickc.transform[1]+dy,
         thickc.transform[2], thickc.transform[3], 0, 0]

    karta.RegularGrid(T, crs=thickc.crs, values=hdivu).to_geotiff("tmp.tif")

    # Fill nodata
    cmd = ["gdal_fillnodata.py", "-md", "10", "-q", "tmp.tif", "tmp2.tif"]
    subprocess.call(cmd)
    g = karta.read_geotiff("tmp2.tif")
    if args.outline:
        ol = karta.read_shapefile(args.outline)
        try:
            ol[1]
            raise ValueError("outline file must contain only a single geometry")
        except IndexError:
            pass
        g.mask_by_poly(ol[0], inplace=True)

    g.to_geotiff(args.out)
    os.remove("tmp.tif")
    os.remove("tmp2.tif")

