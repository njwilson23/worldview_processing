#! /usr/bin/env python
import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
from wvutil import ensureisdir

INDIR =  "hillshades-10m-aligned/"
OUTDIR = "hillshades-10m-aligned-filled/"
OVERWRITE = False

fnms = [f for f in os.listdir(INDIR) if f.endswith(".tif")]

ensureisdir(OUTDIR)

def fill_nodata(fin, fout):

    cmd = ["gdal_fillnodata.py", "-md", "50",
           "-co", "COMPRESS=LZW",
           "-co", "TILED=YES",
           fin, fout]
    subprocess.call(cmd)

    return fin, fout

def print_finished(fut):
    print("finished", fut.result())
    return

with ThreadPoolExecutor(max_workers=4) as pool:

    for f in fnms:

        outfnm = os.path.join(OUTDIR, f)

        if os.path.isfile(outfnm) and not OVERWRITE:
            print("skipping %s" % f)

        else:
            fut = pool.submit(fill_nodata, os.path.join(INDIR, f), outfnm)
            fut.add_done_callback(print_finished)

