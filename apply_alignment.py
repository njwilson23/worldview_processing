#! /usr/bin/env python
#
# Takes a DEM, applies a translation read from a file, and/or resamples to a
# grid using gdalwarp

import argparse
import karta
import numpy as np
import os
import subprocess
import sys
import tempfile

def apply_translation_inplace(grid, transform):
    grid._transform = (grid.transform[0]+transform[0,3],
                       grid.transform[1]+transform[1,3],
                       grid.transform[2], grid.transform[3],
                       grid.transform[4], grid.transform[5])
    grid.values[grid.data_mask] += transform[2,3]
    return grid

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=
            """apply translations and/or resample. if --transform-file is
provided, a grid transformation occurs. if -tr and --origin are
provided, grid resampling occurs. """)

    parser.add_argument("dem", action="store", type=str, metavar="INFILE",
            help="name of the input DEM")
    parser.add_argument("out", action="store", type=str, metavar="OUTFILE",
            help="name of the output DEM")
    parser.add_argument("--overwrite", action="store_true", default=False,
            help="overwrite existing files")
    parser.add_argument("--transform-file", action="store", default="",
            help="file (from compute_misalignment.py) with translation data")
    parser.add_argument("-tr", nargs=2, action="store", metavar=("DX", "DY"),
            help="output resolution", type=float)
    parser.add_argument("--origin", nargs=2, action="store", metavar=("X", "Y"),
            help="meta-origin. the actuall output origin will be an integer "
                 "factor of the grid resolution from this point", type=float)

    args = parser.parse_args()

    if os.path.isfile(args.out) and not args.overwrite:
        raise FileExistsError(args.out)

    if args.transform_file is None and None in (args.tr, args.origin):
        parser.print_usage()
        sys.exit(1)

    tmpdir = tempfile.mkdtemp()
    try:
        dem = karta.read_geotiff(args.dem)

        if args.transform_file:

            with open(args.transform_file, "r") as f:
                rows = [list(map(float, line.split())) for line in f.readlines()]
            apply_translation_inplace(dem, np.array(rows))

        if args.tr and args.origin:

            tmpfnm = os.path.join(tmpdir, os.path.split(args.dem)[1])
            dem.to_geotiff(tmpfnm, compress="LZW", tiled=True)

            T = dem.transform
            ny, nx = dem.size
            del dem

            # compute the grid cell offsets from the meta-origin that define
            # the resampling grid
            nx0 = (T[0] - args.origin[0]) // args.tr[0]
            ny0 = (T[1] - args.origin[1]) // args.tr[1]
            nx1 = (T[0] + nx*T[2] - args.origin[0]) // args.tr[0] + 1
            ny1 = (T[1] + ny*T[3] - args.origin[1]) // args.tr[1] + 1

            x0 = args.origin[0] + nx0*args.tr[0]
            y0 = args.origin[1] + ny0*args.tr[1]
            x1 = args.origin[0] + nx1*args.tr[0]
            y1 = args.origin[1] + ny1*args.tr[1]

            cmd = ["gdalwarp",
                    "-tr", str(args.tr[0]), str(args.tr[1]),
                    "-te", str(x0), str(y0), str(x1), str(y1),
                    "-co", "COMPRESS=LZW",
                    "-co", "TILED=YES",
                    "-ot", "Float32",
                    "-multi",
                    "-r", "average",
                    tmpfnm, args.out]
            if args.overwrite:
                cmd.append("-overwrite")
            ret = subprocess.call(cmd)
            if ret != 0:
                raise RuntimeError("failure executing\n{0}".format(" ".join(cmd)))

        else:
            dem.to_geotiff(args.out, compress="LZW", tiled=True)
            del dem

    finally:
        # assumes no subdirectories
        for fnm in os.listdir(tmpdir):
            os.remove(os.path.join(tmpdir, fnm))
        os.removedirs(tmpdir)

