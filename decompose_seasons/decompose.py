import datetime
import numpy as np
import karta
from scipy.sparse import coo_matrix
from scipy.sparse.linalg import lsmr

from monthday import MonthDay

def seasonal_fractions(ts, seasons):
    """ Given a timespan (tuple of datetimes), compute the fraction of the span
    that is within each of a list of seasons. """
    dt = ts[1]-ts[0]
    ndays = dt.days+1

    # Get the initial season
    iseason = 0
    start = MonthDay(ts[0].month, ts[0].day)
    for season in seasons:
        if season.contains(start):
            break
        iseason += 1
    if iseason == len(seasons):
        raise RuntimeError("failure to identify start season")

    ndays = [0 for _ in seasons]
    t = ts[0]

    season_end = datetime.datetime(t.year,
                                   seasons[iseason].end.month,
                                   seasons[iseason].end.day,
                                   tzinfo=datetime.timezone.utc)
    if season_end < t:
        season_end = datetime.datetime(t.year+1,
                                       seasons[iseason].end.month,
                                       seasons[iseason].end.day,
                                       tzinfo=datetime.timezone.utc)

    niter = 0
    while t < ts[1]:

        if season_end <= ts[1]:
            ndays[iseason] += (season_end-t).days
            t = season_end

            if iseason != len(seasons)-1:
                iseason += 1
            else:
                iseason = 0

            season_end = datetime.datetime(t.year,
                                           seasons[iseason].end.month,
                                           seasons[iseason].end.day,
                                           tzinfo=datetime.timezone.utc)
            if season_end < t:
                season_end = datetime.datetime(t.year+1,
                                               seasons[iseason].end.month,
                                               seasons[iseason].end.day,
                                               tzinfo=datetime.timezone.utc)

        else:
            ndays[iseason] += (ts[1] - t).days
            t = ts[1]
        niter += 1
        if niter == 10:
            raise RuntimeError("too many loop repetitions")

    n = float(sum(ndays))
    return [a/n for a in ndays]

def build_mask(grids, partitions, min_obs):
    msk0 = np.zeros(grids[0].size, dtype=np.int16)
    msk1 = np.zeros(grids[0].size, dtype=np.int16)

    for (f0, f1), grid in zip(partitions, grids):
        if f0 > 0:
            msk0[grid.data_mask] += 1
        if f1 > 0:
            msk1[grid.data_mask] += 1

    return (msk0 > min_obs) & (msk1 > min_obs)

def generate_edges(ny, nx):
    """ for a grid with size (ny, nx), return a generator for the connected cells """
    i = 0
    while i != ny-1:
        j = 0
        while j != nx-1:
            yield (i, j), (i, j+1)
            yield (i, j), (i+1, j)
            j += 1
        i += 1
    return

def edges_from_mask(msk):
    """ from a mask grid, compute the connected edges between cells where msk
    is True, and return the numeric ids of the True cells

    E.g.
    ::
        msk = [[0, 0, 1, 0],        # [[-, -, 0, -],    (msk numbers)
               [0, 1, 1, 0],        #  [-, 1, 2, -],
               [0, 1, 1, 1]]        #  [-, 3, 4, 5]]

        edges_from_mask(msk) ->
            (0, 2), (1, 2), (1, 3), (2, 4), (3, 4), (4, 5)

    """
    ny, nx = msk.shape
    msk_numbers = np.nan * np.zeros([ny, nx])
    msk_numbers[msk] = np.arange(np.sum(msk))
    for (i0, j0), (i1, j1) in generate_edges(ny, nx):
        if msk[i0, j0] and msk[i1, j1]:
            yield msk_numbers[i0, j0], msk_numbers[i1, j1]

def build_adot_vector(grids, msk, partitions):
    npixels = np.sum(msk)
    ngrids = len(grids)
    nedges = len([_ for _ in edges_from_mask(msk)])
    # sized for every observation, edge regularization, seasonal difference
    # penalty, zero-mean error constraint, observed mean constraint
    adotvec = np.zeros(ngrids * npixels + 2*nedges + npixels + 1 + ngrids + 1)

    i = 0
    for grid in grids:
        v = grid[msk]
        v = v[~np.isnan(v)]
        sz = len(v)
        adotvec[i:i+sz] = v
        i = i+sz

    # Leave zeroes for edge regularization
    i += 2*nedges

    # Leave zeros for seasonal difference penalty
    i += npixels

    # Leave zeros for error penalty
    i += ngrids

    # Leave zero for error constraint
    i += 1

    # enforce observed mean
    fsummer = np.mean([p[0] for p in partitions])
    fwinter = np.mean([p[1] for p in partitions])
    tmp = karta.raster.merge(grids)
    μ = np.nanmean(tmp[msk])
    adotvec[i] = 2*μ*(fsummer+fwinter)

    return adotvec[:i+1]

def build_P_matrix(grids, msk, partitions, regularization=0.0,
                   mismatch_penalty=0.0, error_penalty=0.0, weights=None):
    if weights is None:
        weights = [1 for _ in partitions]
    npixels = np.sum(msk)
    ngrids = len(grids)
    row_indices = []
    col_indices = []
    data = []

    i = 0
    for niter, ((fsummer, fwinter), grid) in enumerate(zip(partitions, grids)):
        colmsk = ~np.isnan(grid[msk])
        sz = np.sum(colmsk)

        irows = np.arange(i, i+sz)
        icols0 = np.argwhere(colmsk).ravel()
        icols1 = icols0 + npixels

        row_indices.extend(irows)
        col_indices.extend(icols0)
        data.extend([fsummer for _ in irows])
        row_indices.extend(irows)
        col_indices.extend(icols1)
        data.extend([fwinter for _ in irows])

        # error term
        row_indices.extend(irows)
        col_indices.extend([2*npixels+niter for _ in irows])
        data.extend([1 for _ in irows])

        i = i+sz

    # add edge regularization
    for icol0, icol1 in edges_from_mask(msk):
        row_indices.extend([i, i, i+1, i+1])
        col_indices.extend([icol0, icol1, icol0+npixels, icol1+npixels])
        data.extend([regularization, -regularization, regularization, -regularization])
        i += 2

    for j in range(npixels):
        row_indices.append(i)
        row_indices.append(i)
        col_indices.append(j)
        col_indices.append(j+npixels)
        data.append(mismatch_penalty)
        data.append(-mismatch_penalty)
        i += 1

    # add error penalty
    for j in range(ngrids):
        row_indices.append(i)
        col_indices.append(2*npixels+j)
        data.append(error_penalty)
        i += 1

    # add error constraint
    row_indices.extend([i for _ in range(ngrids)])
    col_indices.extend(range(2*npixels, 2*npixels+ngrids))
    data.extend([2*w for w in weights])  # 2 is for tidiness, but irrelevant

    # add mean value constraint
    row_indices.extend([i+1 for _ in range(2*npixels)])
    col_indices.extend(range(2*npixels))
    fsummer = np.mean([p[0] for p in partitions])
    fwinter = np.mean([p[1] for p in partitions])
    data.extend([2*fsummer/npixels*(fsummer+fwinter) for _ in range(npixels)])
    data.extend([2*fwinter/npixels*(fsummer+fwinter) for _ in range(npixels)])

    Pcoo = coo_matrix((data, (row_indices, col_indices)))
    return Pcoo.tocsr()

def decompose(grids, partitions, min_obs=3, regularization=0.0,
              mismatch_penalty=0.0, error_penalty=0.0, weights=None):
    """ Given a list of grids representing partitions among seasons, compute the
    protypical field for each season.
    """
    msk = build_mask(grids, partitions, min_obs)
    adotvec = build_adot_vector(grids, msk, partitions)
    P = build_P_matrix(grids, msk, partitions,
                       regularization=regularization,
                       mismatch_penalty=mismatch_penalty,
                       error_penalty=error_penalty,
                       weights=weights)
    print("system size:    {0}×{1}".format(*P.shape))

    res = lsmr(P, adotvec, atol=1e-8, btol=1e-8)
    print("stop criterion: {0}".format(res[1]))
    print("# iterations:   {0}".format(res[2]))
    α = res[0]
    α_summer = np.nan*np.zeros(grids[0].size)
    α_winter = np.nan*np.zeros(grids[0].size)

    npixels = np.sum(msk)
    α_summer[msk] = α[:npixels]
    α_winter[msk] = α[npixels:2*npixels]
    ϵ = α[2*npixels:]
    return α_summer, α_winter, ϵ

