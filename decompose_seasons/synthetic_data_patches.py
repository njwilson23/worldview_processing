import numpy as np
import karta
import mapping
import matplotlib.pyplot as plt
from decompose import decompose
from monthday import MonthDay, MonthDaySpan

# construct fake data
x = np.linspace(0, 2*np.pi, 80)
y = np.atleast_2d(np.linspace(0, 2*np.pi, 50)).T
summer_melt = karta.RegularGrid([0, 0, 1, 1, 0, 0],
                                values=20*np.sin(x/2)*np.cos(y)+10,
                                crs=karta.crs.NSIDCNorth)
winter_melt = karta.RegularGrid([0, 0, 1, 1, 0, 0],
                                values=15*np.cos(x)*np.sin(y/3)+20,
                                crs=karta.crs.NSIDCNorth)

summer_melt[20:38,24:38] = np.nan*np.zeros([18, 14])
winter_melt[20:38,24:38] = np.nan*np.zeros([18, 14])

grids = []

partitions = [(0.2, 0.8), (0.5, 0.5), (0.9, 0.1), (0.6, 0.4), (0.6, 0.4),
              (0.4, 0.6), (0.3, 0.7), (0.1, 0.9), (0.4, 0.6)]
errors = [8, -2, -3, 1, 3, -4, 2, 1, -3]
# errors = [0 for _ in errors]
errors = [e*5 for e in errors]

np.random.seed(42)
bb = summer_melt.bbox
obs_xmin = np.random.randint(bb[0], bb[0]+20, len(partitions))
obs_ymin = np.random.randint(bb[1], bb[1]+20, len(partitions))
obs_bbs = [(xmin, ymin, np.random.randint(xmin+20, bb[2]), np.random.randint(ymin+20, bb[3]))
           for (xmin, ymin) in zip(obs_xmin, obs_ymin)]
print(obs_bbs)

for (fsummer, fwinter), err, bb_ in zip(partitions, errors, obs_bbs):
    newgrid = winter_melt.copy()
    newgrid[:,:] = (fsummer*summer_melt[:,:] + fwinter*winter_melt[:,:] +
                    err + 0.5*np.random.random(newgrid.size))
    newgrid = newgrid.resize(bb_).resize(bb)
    grids.append(newgrid)

summer_computed, winter_computed, ϵ = decompose(grids, partitions,
                                                regularization=0.0)

fs = np.mean([p[0] for p in partitions])
fw = np.mean([p[1] for p in partitions])

print("true summer mean:     {0}".format(np.nanmean(summer_melt[:,:])))
print("computed summer mean: {0}".format(np.nanmean(summer_computed)))
print("true winter mean:     {0}".format(np.nanmean(winter_melt[:,:])))
print("computed winter mean: {0}".format(np.nanmean(winter_computed)))
print("true observed mean:   {0}".format(np.nanmean(fs*summer_melt[:,:] + fw*winter_melt[:,:])))
print("computed mean:        {0}".format(np.nanmean(fs*summer_computed + fw*winter_computed)))

plotkw = dict(vmin=-30, vmax=30, cmap=plt.cm.RdBu)
fig = plt.figure()
ax1 = fig.add_subplot(3, 2, 1)
ax2 = fig.add_subplot(3, 2, 2)
ax3 = fig.add_subplot(3, 2, 3)
ax4 = fig.add_subplot(3, 2, 4)
ax5 = fig.add_subplot(3, 2, 5)
ax6 = fig.add_subplot(3, 2, 6)
m = mapping.plot(summer_melt, ax=ax1, **plotkw)
plt.colorbar(m, ax=ax1, orientation="vertical")
m = mapping.plot(winter_melt, ax=ax2, **plotkw)
plt.colorbar(m, ax=ax2, orientation="vertical")
m = ax3.imshow(summer_computed, origin="bottom", **plotkw)
plt.colorbar(m, ax=ax3, orientation="vertical")
m = ax4.imshow(winter_computed, origin="bottom", **plotkw)
plt.colorbar(m, ax=ax4, orientation="vertical")
m = ax5.imshow(summer_computed-summer_melt[:,:], origin="bottom", cmap=plt.cm.viridis)
plt.colorbar(m, ax=ax5, orientation="vertical")
m = ax6.imshow(winter_computed-winter_melt[:,:], origin="bottom", cmap=plt.cm.viridis)
plt.colorbar(m, ax=ax6, orientation="vertical")

ax1.set_title("Summer")
ax2.set_title("Winter")
ax1.set_ylabel("Computed")
ax3.set_ylabel("Computed")
ax5.set_ylabel("Error")

plt.show()
