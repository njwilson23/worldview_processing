import datetime

class MonthDay(object):
    def __init__(self, month, day):
        self.month = month
        self.day = day

    def __sub__(self, other):
        return MonthDaySpan(other, self)

    def __str__(self):
        return "MonthDay({0}, {1})".format(self.month, self.day)

class MonthDaySpan(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def days(self, leap_year=False):
        year0 = 2001
        if self.end.month > self.start.month:
            year1 = 2001
        elif (self.end.month == self.start.month) and (self.end.day > self.start.day):
            year1 = 2001
        else:
            year1 = 2002
        ndays = (datetime.datetime(year1, self.end.month, self.end.day),
                 datetime.datetime(year0, self.start.month, self.start.day)).days
        if leap_year:
            ndays += 1
        return ndays

    def contains(self, md):
        end_n = normalize(self.start, self.end)
        md_n = normalize(self.start, md)
        if self.start.month > md_n.month:
            return False
        elif (self.start.month == md_n.month) and self.start.day > md_n.day:
            return False
        elif md_n.month > end_n.month:
            return False
        elif (md_n.month == end_n.month) and md_n.day > end_n.day:
            return False
        else:
            return True

def normalize(md0, md1):
    """ Given a *start* and *end* MonthDay, return a MonthDay instance *out*
    equivalent to the end but such that `out.month - start.month` is equal to
    the number of months between the two inputs.
    """
    if (md1.month > md0.month) or ((md1.month == md0.month) and (md1.day > md0.day)):
        return md1
    else:
        mnth = md0.month
        for month in month_generator(md0.month, md1.month):
            mnth += 1
        return MonthDay(mnth, md1.day)

def month_generator(start, stop):
    """ From a start month to an end month, generate all months inbetween.

    E.g.
    ::
        month_generator(7, 5) ->
            7, 8, 9, 10, 11, 12, 1, 2, 3, 4
    """
    i = start
    while i != stop:
        yield i
        i += 1
        if i == 13:
            i = 1
