import numpy as np
import karta
import mapping
import matplotlib.pyplot as plt
from decompose import decompose
from monthday import MonthDay, MonthDaySpan

# construct fake data
x = np.linspace(0, 2*np.pi, 40)
y = np.atleast_2d(np.linspace(0, 2*np.pi, 40)).T
summer_melt = karta.RegularGrid([0, 0, 1, 1, 0, 0],
                                values=20*np.sin(x/2)*np.cos(y),
                                crs=karta.crs.NSIDCNorth)
winter_melt = karta.RegularGrid([0, 0, 1, 1, 0, 0],
                                values=15*np.cos(x)*np.sin(y/3),
                                crs=karta.crs.NSIDCNorth)

partitions = [(0.2, 0.8), (0.6, 0.4)]
errors = [0, 0]
grids = []

np.random.seed(42)
for (fsummer, fwinter), err in zip(partitions, errors):
    newgrid = winter_melt.copy()
    newgrid[:,:] = (fsummer*summer_melt[:,:] + fwinter*winter_melt[:,:] +
                    err + 5.0*np.random.random(newgrid.size))
    grids.append(newgrid)

summer_computed, winter_computed, ϵ = decompose(grids, partitions, min_obs=1)

fs = np.mean([p[0] for p in partitions])
fw = np.mean([p[1] for p in partitions])

print("true summer mean:     {0}".format(np.nanmean(summer_melt[:,:])))
print("computed summer mean: {0}".format(np.nanmean(summer_computed)))
print("true winter mean:     {0}".format(np.nanmean(winter_melt[:,:])))
print("computed winter mean: {0}".format(np.nanmean(winter_computed)))
print("true observed mean:   {0}".format(np.nanmean(fs*summer_melt[:,:] + fw*winter_melt[:,:])))
print("computed mean:        {0}".format(np.mean(fs*summer_computed + fw*winter_computed)))

plotkw = dict(vmin=-30, vmax=30, cmap=plt.cm.coolwarm_r)
fig = plt.figure(1, figsize=(10, 4))
ax1 = fig.add_subplot(1, 2, 1)
ax2 = fig.add_subplot(1, 2, 2)
m = mapping.plot(summer_melt, ax=ax1, **plotkw)
plt.colorbar(m, ax=ax1, orientation="vertical")
m = mapping.plot(winter_melt, ax=ax2, **plotkw)
plt.colorbar(m, ax=ax2, orientation="vertical")

fig2 = plt.figure(2, figsize=(10, 4))
ax3 = fig2.add_subplot(1, 2, 1)
ax4 = fig2.add_subplot(1, 2, 2)
m = mapping.plot(grids[0], ax=ax3, **plotkw)
plt.colorbar(m, ax=ax3, orientation="vertical")
m = mapping.plot(grids[1], ax=ax4, **plotkw)
plt.colorbar(m, ax=ax4, orientation="vertical")

fig3 = plt.figure(3, figsize=(10, 4))
ax5 = fig3.add_subplot(1, 2, 1)
ax6 = fig3.add_subplot(1, 2, 2)
m = ax5.imshow(summer_computed, origin="bottom", **plotkw)
plt.colorbar(m, ax=ax5, orientation="vertical")
m = ax6.imshow(winter_computed, origin="bottom", **plotkw)
plt.colorbar(m, ax=ax6, orientation="vertical")

# m = ax5.imshow(summer_computed-summer_melt[:,:], origin="bottom", cmap=plt.cm.viridis)
# plt.colorbar(m, ax=ax5, orientation="vertical")
# m = ax6.imshow(winter_computed-winter_melt[:,:], origin="bottom", cmap=plt.cm.viridis)
# plt.colorbar(m, ax=ax6, orientation="vertical")

ax1.set_title("Summer")
ax2.set_title("Winter")
ax3.set_title("Obs 1")
ax4.set_title("Obs 2")
ax5.set_title("Computed summer")
ax6.set_title("Computed winter")

for fig_ in (fig, fig2, fig3):
    fig_.tight_layout()

fig.savefig("figures/synth1_proto.png")
fig2.savefig("figures/synth1_obs.png")
fig3.savefig("figures/synth1_result.png")
