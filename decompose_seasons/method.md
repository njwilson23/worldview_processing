# Seasonal decomposition of satellite-derived melt rates

## Methodology

Melt rate data derived from satellites such as IceSat (Moholdt et al, 2014),
EnviSat (Paolo et al, 2015), and Worldview (Shean et al, 2016) provide an
estimate of submarine processes that are otherwise difficult to measure.
Compared to direct observation (e.g. with phase sensitive radar, Corr et al
2002), remote-sensing based flux divergence and Lagrangian ice thickness change
methods typically have relatively high spatial resolution at the cost of
temporal resolution. Interannual changes in the melt rates are measurable, but
because error due to ice thickness uncertainty grows by reducing the temporal
baseline between observations, shorter timescale variability is difficult to
measure accurately.

This describes a method for inferring seasonal variability from
satellite-derived melt rate fields using a simple linear inversion method. Given
a list of $n$ melt rate estimates $a_i$ for $i\in[1,n]$, a direct approach would
be to compute those estimates that approximately represent each timespan of
interest, and average those. This requires estimates with the appropriate
baseline to exist. Furthermore, it may be problematic when trying to construct
estimates for short (< 2 months) melt seasons, because uncertainty in the each
estimate will be relatively high.

An alternative approach, described here, is to invert for seasonal melt fields
using melt estimates that are only required to intersect the timespan of
interest. For each melt estimate, compute the fraction $f_s$ of the scene that
is within the season of interest, and $f_w = 1-f_s$. Then, the observed melt
rate $a_i$ is
$$ a_i \approx f_s a_s + f_w a_w + \epsilon_i $$
where $a_s$ and $a_w$ are the melt rates for the season of interest and the
remainder, and $\epsilon_i$ is a constant offset error for estimate $a_i$. The
error $\epsilon_i$ may be due to errors in satellite position, errors in grid
registration, or uncertainties in the ice thickness derived from observed
surface elevation.

Written as a linear system, this yields
$$ \mathbf{a_{obs}} \approx \mathbf{P} \mathbf{x} $$
where $\mathbf{x}$ is a vector containing the seasonal melt rates $a_s$ and
$a_w$ and the errors $\epsilon_i$, and $\mathbf{P}$ is a sparse matrix encoding
the seasonal fractions for each scene, as well as a row that minimizes the
sum of $\epsilon_i$ using a Lagrange multiplier.

The well-known normal equations for the linear least squares system are
$$\mathbf{P}^T\mathbf{P}\mathbf{x}=\mathbf{P}^T\mathbf{a_{obs}}.$$

## Synthetic data

As a test, I define a synthetic summer and winter melt rate

$$ a_s = 20 \sin{\left(x\over 2\right)} \cos{y} $$
and
$$ a_w = 15 \cos{x} \sin{\left(y \over 3\right)}. $$

![Synthetic seasonal "melt rates"](figures/synth1_proto.png)

I invent two "observations" with seasonal partitions $(f_s, f_w)$ of $(0.2,
0.8)$ and $(0.4, 0.6)$, respectively, and add some noise to the observations.
Leaving the error terms zero for now, the inputs can be computed by inverting
the system.

![Observations of synthetic seasonal data](figures/synth1_obs.png)

![Result of first synthetic test](figures/synth1_result.png)

To test the impact of constant offsets in observed melt rate due to errors in
ice thickness, I repeat this exercise with four observations with seasonal
partitions $(0.2, 0.8)$, $(0.6, 0.4)$, $(0.4, 0.6)$, $(0.3, 0.7)$, and error
rates of $(-2, 5, -3, 1)$ intentionally added to the observations.

![Observations of synthetic data with constant errors added](figures/synth2_obs.png)

The estimated summer and winter melt rates come from solving the least squares
system.

![Result of second synthetic test](figures/synth2_result.png)

## Extensions to the method

_TODO: describe these in more detail_

- Enforce the observed mean ($\mu$) via a second Lagrange multiplier.
  Define the functional $J$ such that
  $$ J = \left[ \mu - \sum\limits_{i=1}^n \left(f_s a_s(i) + f_wa_w(i)\right) \right]^2. $$
  Minimize the functional by setting the derivative
  $$ \frac{\partial J}{\partial a(i)} = 2 \sum\limits_{j=1}^n \left(\mu -
  \sum\limits_{i=1}^n\left(f_sa_s(i) + f_wa_w(i)\right)\right) f(j) $$
  equal to zero.

- Compute the connected edges between grid cells, given by indices $i$ and $j$,
  and add rows to the least squares system that penalize large differences, e.g.
  $$0 = \beta_s \left(a(i)-a(j)\right) $$
  where $\beta_s$ is a regularization parameter that may be tuned to achieve
  smoothness in the results. This makes sense because the observations have
  spatial correlation, so it seems reasonable that the results should too.

- Penalization of large $\epsilon$, because otherwise the seasonal means aren't
  directly constrained

- Penalization of large summer/winter contrasts (see problems below)

## Application to real data

I have used this method to estimate seasonal melt rates over Nioghalvfjerdsbræ,
in northeast Greenland, and Ryder Glacier, in northern Greenland. I have used
June 16 - August 31 as the season of interest, based on estimated surface mass
balance from RACMO model output. This timespan is labelled "Summer" in the
figures below.

In the following figures, the second row shows differences between the two
images above.

![Ryder Glacier](figures/ryder.png)

At Ryder Glacier there are several interesting features. The first is that the
melt rate is more evenly-distributed along glacier in the summer, while in the
winter, there is a stronger concentration of negative mass balance near the
grounding zone (bottom of figure). This may be due to the contribution of
surface melt in the summer, which is evenly distributed in space, and the
dominance of submarine melting in the winter, which occurs where the ice tongue
is deepest.

Secondly, in the winter, the channel that runs from the grounding zone to the
terminus of Ryder glacier exhibits negligible mass balance. In the summer
there is little evidence for any effect due to the channel. This may reflect the
redistribution of snow into the channel during winter, or it may indicate
differences in the circulation of heat in the summer versus the winter.

The winter melt rate structure closely approximates the total melt rate
structure, so despite the large summer mass balance terms, the background winter
rate appears to control the ice tongue mass loss.

![Nioghalvfjerdsbræ](figures/niog.png)

At Nioghalvfjerdsbræ, some of the same observations apply. There is a broader
distribution of high-melt areas in the summer, but the winter melt distribution
closely approximates the mean annual melt rate field. Seasonal changes in melt
rate near the grounding zone are very small, while differences along the
downstream ice tongue are large and somewhat unintuitively structured (see
problems, below).

## Current limitations and problems

1. While spatial structure is preserved, the basic method appears to be poor at
   computing the mean seasonal melt rates because of the lack of a constraint on
   the offset term. I've addressed this by penalizing large $\epsilon_i$, but
   this isn't a very satisfying solution.

2. There is a tendency for large structures in one season to be balanced by the
   inverse structure in the other season. This maintains the observed mean in
   the combined estimate (which is a constraint), but is unrealistic. A prior
   expectation of small oscillations in the melt rate can be added via a
   regularization term, while a prior expectation that melt rates will have
   approximately the same magnitude in each season can be enforced by penalizing
   the difference in melt rate. Neither of these is particularly easy to compute
   a correct value for.

3. Positive mass balance tends to appear in regions; see in particular the melt
   rates estimated for Nioghalvfjerdsbrae. This is likely related to the
   previous point. While not physically impossible, it defies expectations.

4. There are spatial smoothness regularization, seasonal mismatch, and error
   weight tunable parameters, which have the potential to alter the inverted
   melt rates if used blindly. A good physical justification for choosing
   particular parameters is lacking.

