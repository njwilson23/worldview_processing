import os
import time
import numpy as np
import karta
import mapping
import matplotlib.pyplot as plt

from decompose import (decompose, seasonal_fractions)
from monthday import MonthDay, MonthDaySpan
from helpers import bboxaspoly, union_bbox, intersection_bbox, exclude_bad_pixels

import sys
sys.path.append("/home/natw/Documents/Greenland_data/WorldView/bin")
from wvutil import get_sceneid, get_date, get_baseline

DIRNAME = "/home/natw/Documents/Greenland_data/WorldView/ryder-analysis/"
INVERSION_RESOLUTION = (128, 128)
REGULARIZATION = 0.5
MISMATCH_PENALTY = 0.5
ERROR_PENALTY = 20.0
MIN_OBS = 2
BBOX = (-1e12, -1e12, 1e12, 1e12)
FIGNAME = "figures/ryder.png"

# DIRNAME = "/home/natw/Documents/Greenland_data/WorldView/petermann-analysis/"
# INVERSION_RESOLUTION = (256, 256)
# REGULARIZATION = 0.5
# MISMATCH_PENALTY = 0.5
# ERROR_PENALTY = 40
# MIN_OBS = 2
# MIN_AREA = 0.0
# MIN_INTERVAL = 0.5 # years
# BBOX = (-284640, -1000570, -250520, -969560)
# FIGNAME = "figures/petermann.png"

# DIRNAME = "/home/natw/Documents/Greenland_data/WorldView/niog-analysis/"
# INVERSION_RESOLUTION = (512, 512)
# REGULARIZATION = 1.0
# MISMATCH_PENALTY = 1.0
# ERROR_PENALTY = 50.0
# MIN_OBS = 2
# BBOX = (-1e12, -1e12, 1e12, 1e12)
# FIGNAME = "figures/niog.png"

SEASONS = [MonthDaySpan(MonthDay(6, 1), MonthDay(8, 31)),
           MonthDaySpan(MonthDay(9, 1), MonthDay(5, 31))]
# SEASONS = [MonthDaySpan(MonthDay(4, 1), MonthDay(9, 30)),
#            MonthDaySpan(MonthDay(10, 1), MonthDay(3, 31))]

dhdt_fnms = [f for f in os.listdir(os.path.join(DIRNAME, "ldhdt-grids-128m"))
             if f.endswith(".tif")]

hdivu_fnms = [f for f in os.listdir(os.path.join(DIRNAME, "hdivu-128m"))
              if f.endswith(".tif")]

adot_grids = []
adot_timespans = []

_bndcls = karta.raster.SimpleBand
tstart = time.time()

for fnm in dhdt_fnms:

    scid1 = get_sceneid(fnm.split("_")[0])
    scid2 = get_sceneid(fnm.split("_")[1])

    if "{0}.tif".format(scid1) in hdivu_fnms:
        f = os.path.join(DIRNAME, "hdivu-128m", "{0}.tif".format(scid1))
        hdivu1 = karta.read_geotiff(f, bandclass=_bndcls).apply(exclude_bad_pixels)
    else:
        hdivu1 = None

    if "{0}.tif".format(scid2) in hdivu_fnms:
        f = os.path.join(DIRNAME, "hdivu-128m", "{0}.tif".format(scid2))
        hdivu2 = karta.read_geotiff(f, bandclass=_bndcls).apply(exclude_bad_pixels)
    else:
        hdivu2 = None

    if hdivu1 is not None and hdivu2 is not None:
        bb = union_bbox(hdivu1.bbox, hdivu2.bbox)
        _parts = [hdivu1.resize(bb), hdivu2.resize(bb)]
        hdivu = karta.raster.merge(_parts)
    elif hdivu1 is not None:
        hdivu = hdivu1
    elif hdivu2 is not None:
        hdivu = hdivu2
    else:
        print("No hdivu exists for {0}".format(fnm))
        continue

    dhdt = karta.read_geotiff(os.path.join(DIRNAME, "ldhdt-grids-128m", fnm),
                            bandclass=_bndcls).apply(exclude_bad_pixels)
    bb0 = dhdt.bbox # used for filter hack
    interval = get_baseline(scid1, scid2) / 365.0
    try:
        bb = intersection_bbox(dhdt.bbox, hdivu.bbox, BBOX)
    except ValueError as e:
        print("{0}: {1}".format(fnm, e))
        continue

    dhdt = dhdt.resize(bb)
    hdivu = hdivu.resize(bb)
    adot_vals = np.where(dhdt.data_mask & hdivu.data_mask, dhdt[:,:]+hdivu[:,:], dhdt.nodata)
    adot = karta.RegularGrid(dhdt.transform, values=adot_vals, crs=dhdt.crs,
                             nodata_value=dhdt.nodata)

    # Hack in a filter to avoid small Petermann scenes
    if ("petermann" in DIRNAME) and ((interval < 0.6) or (bboxaspoly(bb0).area < MIN_AREA)):
        del dhdt, hdivu1, hdivu2, hdivu
        continue

    adot_grids.append(adot)
    adot_timespans.append((get_date(scid1), get_date(scid2)))

    del dhdt, hdivu1, hdivu2, hdivu

print("# adot estimate scenes: {0}".format(len(adot_grids)))
tload = time.time()

bb = union_bbox(*[g.bbox for g in adot_grids])
adot_grids_resized = [g.resize(bb).resample(*INVERSION_RESOLUTION) for g in adot_grids]
partitions = [seasonal_fractions(ts, SEASONS) for ts in adot_timespans]
tprep = time.time()
summer_computed, winter_computed, ϵ = decompose(adot_grids_resized, partitions,
                                                min_obs=MIN_OBS,
                                                mismatch_penalty=MISMATCH_PENALTY,
                                                error_penalty=ERROR_PENALTY,
                                                regularization=REGULARIZATION)
tdecomp = time.time()

print("""\ntiming data
-----------
time to load:               {tload:.1f} s
time to pre-process:        {tprep:.1f} s
time to decompose:          {tdecomp:.1f} s""".format(tload=tload-tstart,
                                                      tprep=tprep-tload,
                                                      tdecomp=tdecomp-tprep))

fs = np.mean([p[0] for p in partitions])
fw = np.mean([p[1] for p in partitions])
total_obs = karta.raster.merge(adot_grids_resized)

print("""\ninversion statistics
--------------------
mean summer rate:           {summer:.1f} m/yr
mean winter rate:           {winter:.1f} m/yr
mean total rate:            {total:.1f} m/yr
(observed total rate:       {obs_total:.1f} m/yr)
error mean:                 {err_mean: .1f} m/yr
error stdev:                {err_std: .1f} m/yr""".format(
    summer=np.nanmean(summer_computed),
    winter=np.nanmean(winter_computed),
    total=np.nanmean(fs*summer_computed+fw*winter_computed),
    obs_total=np.mean(total_obs[total_obs.data_mask]),
    err_mean=np.mean(ϵ),
    err_std=np.std(ϵ)))

plotkw = dict(vmin=-30, vmax=30, cmap=plt.cm.coolwarm_r)
fig = plt.figure(figsize=(8, 6))
ax1 = fig.add_subplot(1, 3, 1)
ax2 = fig.add_subplot(1, 3, 2, sharex=ax1, sharey=ax1)
ax3 = fig.add_subplot(1, 3, 3, sharex=ax1, sharey=ax1)

ax1.imshow(fs*summer_computed+fw*winter_computed, origin="bottom",
           extent=total_obs.extent, **plotkw)
ax2.imshow(summer_computed, origin="bottom", extent=total_obs.extent, **plotkw)
m = ax3.imshow(winter_computed, origin="bottom", extent=total_obs.extent, **plotkw)

plt.colorbar(m)

ax1.set_title("Annual melting")
ax2.set_title("Summer melting")
ax3.set_title("Winter melting")

for ax in (ax1, ax2, ax3):
    ax.set_xticks([])
    ax.set_yticks([])
    for spn in ax.spines:
        ax.spines[spn].set_visible(False)

# dev figure
# plotkw = dict(vmin=-30, vmax=30, cmap=plt.cm.coolwarm_r)
# 
# fig = plt.figure(figsize=(12, 8))
# gs = plt.GridSpec(2, 8)
# ax1 = fig.add_subplot(gs[0,0:2])
# ax2 = fig.add_subplot(gs[0,2:4], sharex=ax1, sharey=ax1)
# ax3 = fig.add_subplot(gs[0,4:6], sharex=ax1, sharey=ax1)
# ax4 = fig.add_subplot(gs[0,6:8], sharex=ax1, sharey=ax1)
# 
# ax_season_diff = fig.add_subplot(gs[1,1:4], sharex=ax1, sharey=ax1)
# ax_total_diff = fig.add_subplot(gs[1,5:8], sharex=ax1, sharey=ax1)
# 
# ax1.imshow(summer_computed, origin="bottom", extent=total_obs.extent, **plotkw)
# ax2.imshow(winter_computed, origin="bottom", extent=total_obs.extent, **plotkw)
# ax3.imshow(fs*summer_computed+fw*winter_computed, origin="bottom", extent=total_obs.extent, **plotkw)
# mapping.plot(total_obs, ax=ax4, **plotkw)
# 
# diffplotkw = dict(vmin=-20, vmax=20, cmap=plt.cm.coolwarm)
# 
# m = ax_season_diff.imshow(summer_computed-winter_computed, origin="bottom",
#                       extent=total_obs.extent, **diffplotkw)
# plt.colorbar(m, ax=ax_season_diff)
# m = ax_total_diff.imshow(fs*summer_computed+fw*winter_computed - total_obs[:,:],
#                      origin="bottom", extent=total_obs.extent, **diffplotkw)
# plt.colorbar(m, ax=ax_total_diff)
# 
# for ax in (ax1, ax2, ax3, ax4, ax_season_diff, ax_total_diff):
#     ax.set_xticks([])
#     ax.set_yticks([])
#     for spn in ax.spines:
#         ax.spines[spn].set_visible(False)
# 
# ax1.set_title("Computed summer")
# ax2.set_title("Computed winter")
# ax3.set_title("Computed total")
# ax4.set_title("Observed total")

plt.tight_layout(0)
# plt.savefig(FIGNAME)
plt.show()

inversion_results = karta.RegularGrid(adot_grids_resized[0].transform,
        values=np.dstack([summer_computed, winter_computed]),
        crs=karta.crs.NSIDCNorth)
inversion_results.to_geotiff(FIGNAME.replace("figures/", "results/").replace(".png", ".tif"))

# inverted_total = karta.RegularGrid(adot_grids_resized[0].transform,
#             values=fs*summer_computed+fw*winter_computed,
#             crs=karta.crs.NSIDCNorth)
# inverted_total.to_geotiff("../petermann-analysis/adot_regularized.tif")
