import numpy as np
import karta

def union_bbox(*bbs):
    return (min(bb[0] for bb in bbs), min(bb[1] for bb in bbs),
            max(bb[2] for bb in bbs), max(bb[3] for bb in bbs))

def intersection_bbox(*bbs):
    bb = (max(bb[0] for bb in bbs), max(bb[1] for bb in bbs),
          min(bb[2] for bb in bbs), min(bb[3] for bb in bbs))
    if (bb[0] > bb[2]) or (bb[1] > bb[3]):
        raise ValueError("no intersection")
    return bb

def exclude_bad_pixels(pixel_values, threshold=50):
    out = np.nan*np.zeros_like(pixel_values)
    msk = ~np.isnan(pixel_values)
    out[msk] = np.where(np.abs(pixel_values[msk]) < threshold, pixel_values[msk], np.nan)
    return out

def bboxaspoly(bbox):
    return karta.Polygon([(bbox[0], bbox[1]), (bbox[0], bbox[3]),
                          (bbox[2], bbox[3]), (bbox[2], bbox[1])],
                         crs=karta.crs.NSIDCNorth)
