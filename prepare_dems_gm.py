#! /usr/bin/env python
""" Use a Gauss-Markov Estimator to predict ice thickness from DEMs. """

import dateutil.parser
import karta
import meltpack
import numpy as np
import os
import pandas
import re

from wvutil import get_sceneid, get_date, ensureisdir

GAUSSMARKOV = False
OVERWRITE = True

# Normal
# REQUIRE_BUNDLE_CORRECTION = True
# REGIONS_TO_COMPUTE = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}
# OUTDIR = "gm-dems-nosmooth"

# Tide correction
REQUIRE_BUNDLE_CORRECTION = True
REGIONS_TO_COMPUTE = {"Nioghalvfjerdsbrae", "Petermann"}
REGIONS_TO_COMPUTE = {"Petermann"}
OUTDIR = "gm-dems-nosmooth-tides/"

# Grid chunking parameters (units are raster cell dimensions, e.g. 100 m)
CHUNKSIZE = (100, 100)
OVERLAP = (20, 20)

ensureisdir(OUTDIR)

mask_polygons = {
        # "Nioghalvfjerdsbrae": 
        #     karta.read_shapefile("Nioghalvfjerdsbrae/niog_bedrock.shp"),
        "Nioghalvfjerdsbrae":
            karta.read_shapefile("Nioghalvfjerdsbrae/niog_ocean.shp"),
        # "Petermann":
        #     karta.read_shapefile("Petermann/petermann_bedrock.shp"),
        "Petermann":
            karta.read_shapefile("Petermann/petermann_ocean2.shp"),
        "Ryder":
            karta.read_shapefile("Ryder/ryder_bedrock.shp"),
        "Steensby":
            karta.read_shapefile("Steensby/steensby_bedrock.shp"),
    }

grid_dict = {
    "Nioghalvfjerdsbrae":
        {'dem_10200100151B8600_30m.tif', 'dem_1020010015509A00_30m.tif',
         'dem_1020010023B84000_30m.tif', 'dem_1020010023DB5E00_30m.tif',
         'dem_1020010024638D00_30m.tif', 'dem_1020010024AA0300_30m.tif',
         'dem_103001001BC5CD00_30m.tif', 'dem_1030010022905800_30m.tif',
         'dem_1030010024723400_30m.tif', 'dem_10300100248ECF00_30m.tif',
         'dem_1030010025119200_30m.tif', 'dem_1030010025360100_30m.tif',
         'dem_1030010025892400_30m.tif'},

    "Ryder":
        {'dem_102001002FEA4E00_30m.tif', 'dem_1020010031030D00_30m.tif',
         'dem_1020010032DB6B00_30m.tif', 'dem_1020010033A3B000_30m.tif',
         'dem_1030010035523100_30m.tif', 'dem_1030010036772600_30m.tif',
         'dem_10300100418F7F00_30m.tif'},

    "Steensby":
        {'dem_1020010022E42600_30m.tif', 'dem_102001002470CA00_30m.tif',
         'dem_10300100229CB700_30m.tif', 'dem_1030010030A1C700_30m.tif',
         'dem_10300100323EE200_30m.tif', 'dem_103001004131E700_30m.tif',
         'dem_103001004237C100_30m.tif'},

    "Petermann":
        {'dem_10200100145E7300_30m.tif', 'dem_102001003231EA00_30m.tif',
         'dem_103001000B826F00_30m.tif', 'dem_103001000B966D00_30m.tif',
         'dem_103001000BBD3F00_30m.tif', 'dem_103001000BD86D00_30m.tif',
         'dem_103001000C1FFB00_30m.tif', 'dem_1030010034082800_30m.tif',
         'dem_1030010044154700_30m.tif', 'dem_104001000D88E700_30m.tif'},
    }

# Of the form: V * exp(-x**2/[L]**2)
structure_funcs = {
        "Nioghalvfjerdsbrae": lambda x: 500*np.exp(-x**2/1500**2),
        "Petermann": lambda x: 500*np.exp(-x**2/1500**2),
        "Ryder": lambda x: 500*np.exp(-x**2/1000**2),
        "Steensby": lambda x: 500*np.exp(-x**2/800**2),
    }

topdirs = {
        "Nioghalvfjerdsbrae":   "dems/",
        "Petermann":            "dems/",
        "Ryder":                "dems/",
        "Steensby":             "dems/",
    }

outlines = {
        "Nioghalvfjerdsbrae":   "Nioghalvfjerdsbrae/niog_outline.shp",
        "Petermann":            "Petermann/petermann_outline.shp",
        "Ryder":                "Ryder/ryder_outline.shp",
        "Steensby":             "Steensby/steensby_outline.shp",
    }

def fweight(fnm1, fnm2):
    scene_id1 = get_sceneid(fnm1)
    scene_id2 = get_sceneid(fnm2)
    dt = get_date(scene_id2) - get_date(scene_id1)
    return 1.0/(np.abs(dt.days)+5.0)    # five in denominator makes it a bit fuzzier

def predict_grid(grid, structure, **kw):
    kw.setdefault("eps0", 500)
    kw.setdefault("compute_uncertainty", False)
    kw.setdefault("use_kd_trees", False)

    xx, yy = grid.center_coords()
    x = xx.ravel()
    y = yy.ravel()
    z = grid.values.ravel()

    # Scales
    H = np.nanmean(z) / (1.0-920.0/1028.0)
    L = 2.0*H

    kw.setdefault("maxdist", 3*L)

    def structure(x):
        return 500*np.exp(-x**2/L**2)

    m = np.isnan(z)
    _zi, _ = meltpack.gaussmarkov.predict(structure, np.c_[x, y][~m],
                                          np.c_[x, y][~m], z[~m], **kw)
    zi = np.nan*np.zeros(z.shape)
    zi[~m] = _zi
    return karta.RegularGrid(grid.transform, values=zi.reshape(grid.size),
                crs=grid.crs)

for reg in REGIONS_TO_COMPUTE:
    print(reg)

    topdir = topdirs[reg]
    outline = karta.read_shapefile(outlines[reg])[0]
    grids = grid_dict[reg]

    # structure = structure_funcs[reg]
    structure = None        # not used

    grid_fnms = [os.path.join(topdir, grid) for grid in grids]
    corrections = meltpack.bundle_adjust.compute_vertical_corrections(grid_fnms,
            weighting_func=fweight, polymasks=mask_polygons[reg])
    print("\nRelative datum corrections:\n")
    for k,v in corrections.items():
        print("  {0}    {1:4.2f} m".format(get_sceneid(k),v))

    print("\nSum of corrections: {0}\n".format(sum(corrections.values())))

    for fnm in grids:
        
        if os.path.exists(os.path.join(OUTDIR, fnm)) and not OVERWRITE:
            print("  skipping %s because it would overwrite and existing file" % fnm)
        elif (os.path.join(topdir, fnm) not in corrections) and REQUIRE_BUNDLE_CORRECTION:
            print("  skipping %s because it is missing a bundle correction" % fnm)
        else:
            print("\n  {0}: ".format(fnm))
            g = karta.read_geotiff(os.path.join(topdir, fnm))
            g.values[g.values<-1000000] = np.nan
            mask = g.mask_by_poly(outline)
            g.values[np.isnan(mask.values)] = np.nan

            subs = []
            for i, sub_g in enumerate(g.aschunks(size=CHUNKSIZE, overlap=OVERLAP)):
                if np.sum(~np.isnan(sub_g.values)) > 200:
                    print("    computing subgrid: {0}".format(sub_g.bbox))
                    if GAUSSMARKOV:
                        sub_g_sm = predict_grid(sub_g, structure, use_kd_trees=False)
                    else:
                        sub_g_sm = sub_g
                    subs.append(sub_g_sm)
                del sub_g

            g_sm = karta.raster.merge(subs)

            try:
                dz = corrections[os.path.join(topdir, fnm)]
                print("  applying bundle correction: {0:4.2f} meters".format(dz))
                g_sm.values += dz
            except KeyError:
                print("  Warning: no bundle correction found:\n"
                      "    {0}".format(os.path.join(topdir, fnm)))

            g_sm.to_geotiff(os.path.join(OUTDIR, fnm))
            del subs, g, g_sm
            print("")
