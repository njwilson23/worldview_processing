#! /usr/bin/env python
import dateutil.parser
import karta
import numpy as np
import os
import re

from wvutil import get_sceneid, get_date, get_region
import meltpack.bundle_adjust

topdir = "gm-dems/"
savedir = "gm-dems-corrected/"

def fweight(fnm1, fnm2):
    scene_id1 = get_sceneid(fnm1)
    scene_id2 = get_sceneid(fnm2)
    dt = get_date(scene_id2) - get_date(scene_id1)
    return 1.0/(np.abs(dt.days)+5.0)    # five in denominator makes it a bit fuzzier

# Read in the DEMs from *topdir*
fnms = [os.path.join(topdir, fnm) for fnm in os.listdir(topdir)
                                  if fnm.endswith(".tif")]

# Sort into groups based on region
regions = set(a["region"] for a in wvutil._DB.all())
mask_polygons = {
    "Nioghalvfjerdsbrae": None,
    "Petermann": None,
    "Ryder": karta.read_shapefile("Ryder/ryder_bedrock.shp"),
    "Steensby": karta.read_shapefile("Steensby/steensby_bedrock.shp"),
                 }

groups = {}
for reg in regions:
    groups[reg] = []

for fnm in fnms:
    reg = get_region(get_sceneid(fnm))
    groups[reg].append(fnm)

print(groups)

# For each group, apply bundle adjustment
for reg, fnms in groups.items():
    if len(fnms) < 2:
        pass
    else:
        print(reg)
        corrections = meltpack.bundle_adjust.compute_vertical_corrections(fnms,
                            weighting_func=fweight, polymasks=mask_polygons[reg])

        for dem in fnms:
            corr = corrections.get(dem, 0.0)
            print("\t", dem, "\t", corr)
            DEM = karta.read_geotiff(dem)
            DEM.values += corr
            DEM.to_geotiff(os.path.join(savedir, os.path.basename(dem)))
            del DEM
