#! /usr/bin/env python

import datetime
import h5py
import karta
import numpy as np
import os
import scipy.interpolate as scint
from wvutil import get_sceneid, get_region, get_date, get_best_outline, ensureisdir

REGIONS = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}
INPUTDIR = "correlation-50m-merge/"
OUTPUTDIR = "correlation-interp-50m/"
RESOLUTION = 50.0   # should be nominal resolution of input, not downsampled!
OVERWRITE = False

ensureisdir(OUTPUTDIR)

def daynumber_to_datetime(year, dn):
    dt = datetime.datetime(year, 1, 1) + datetime.timedelta(dn-1)
    return dt

def outline_date(ol):
    year = int(ol.properties["Year"])
    dn = int(ol.properties["DayNumber"])
    return daynumber_to_datetime(year, dn)

def usefile(fnm):
    return fnm.endswith(".h5") and (get_region(get_sceneid(fnm)) in REGIONS)

for fnm in map(lambda f: os.path.join(INPUTDIR, f),
               filter(usefile, os.listdir(INPUTDIR))):

    outfnm = os.path.join(OUTPUTDIR, os.path.split(fnm)[1])
    if os.path.isfile(outfnm) and not OVERWRITE:
        print("skipping %s" % fnm)

    else:
        print("working on %s" % fnm)
        try:
            with h5py.File(fnm) as ds:
                x = ds["x"].value
                y = ds["y"].value
                dx = ds["dx"].value
                dy = ds["dy"].value
                strength = ds["strength"].value
                mx = ds["mismatch_x"].value
                my = ds["mismatch_y"].value
        except KeyError:
            print("can't find expected datasets in file %s" % fnm)
            continue

        # Figure out the grid
        xmin = np.min(x)
        ymin = np.min(y)
        xmax = np.max(x)
        ymax = np.max(y)

        nx = np.ceil((xmax-xmin) / RESOLUTION)+1
        ny = np.ceil((ymax-ymin) / RESOLUTION)+1

        T = [xmin-0.5*RESOLUTION, ymin-0.5*RESOLUTION,
             RESOLUTION,          RESOLUTION,
             0.0,                 0.0]

        # Figure out the missing values
        grid = karta.RegularGrid(T,
                                 np.zeros([ny, nx], dtype=np.int8),
                                 crs=karta.crs.NSIDCNorth,
                                 nodata_value=-128)

        mismatch = np.sqrt(mx**2+my**2)
        magnitude = np.sqrt(dx**2+dy**2)
        mask = (mismatch/(magnitude+1e-15) < 0.5)

        xvalid = x[mask]
        yvalid = y[mask]
        dxvalid = dx[mask]
        dyvalid = dy[mask]
        mxvalid = mx[mask]
        myvalid = my[mask]

        if len(xvalid) < 10:
            print("insufficient data to interpolate (<10)")
            continue

        i, j = grid.get_indices(xvalid, yvalid)
        grid.values[i, j] = 1

        # choose the outline closest to the scene date
        reg = get_region(get_sceneid(fnm))
        date = get_date(get_sceneid(fnm))
        ol, dtdays = get_best_outline(reg, date)

        # sets points outside boundary to NODATA, so that remaining points equal
        # to zero are in the glacier with missing or bad correlations
        grid.mask_by_poly(ol, inplace=True)

        X, Y = grid.center_coords()
        xi = X[grid.values==0]
        yi = Y[grid.values==0]

        # do interpolation for horizontal component
        print("  interpolating {0} points from {1} data".format(len(xi), len(xvalid)))
        dxi = scint.griddata(np.c_[xvalid, yvalid], dxvalid, np.c_[xi, yi], "linear")
        dyi = scint.griddata(np.c_[xvalid, yvalid], dyvalid, np.c_[xi, yi], "linear")
        mxi = scint.griddata(np.c_[xvalid, yvalid], mxvalid, np.c_[xi, yi], "linear")
        myi = scint.griddata(np.c_[xvalid, yvalid], myvalid, np.c_[xi, yi], "linear")

        with h5py.File(outfnm, "w") as ds:
            ds["x"] = np.hstack([xvalid, xi])
            ds["y"] = np.hstack([yvalid, yi])
            ds["dx"] = np.hstack([dxvalid, dxi])
            ds["dy"] = np.hstack([dyvalid, dyi])
            ds["mismatch_x"] = np.hstack([mxvalid, mxi])
            ds["mismatch_y"] = np.hstack([myvalid, myi])
            ds["interpolated"] = np.hstack([np.zeros(len(xvalid), dtype=np.int8),
                                            np.ones(len(xi), dtype=np.int8)])

