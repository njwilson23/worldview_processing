import math
import os
import numpy as np
import karta
import scipy.integrate
from scipy.interpolate import interp1d
import h5py

petermann_data = {"name": "Petermann", "dirname":"petermann-analysis-new"}
niog_data = {"name": "Nioghalvfjerdsbrae", "dirname":"niog-analysis-new"}
steensby_data = {"name": "Steensby", "dirname":"steensby-analysis-new"}
ryder_data = {"name": "Ryder", "dirname":"ryder-analysis-new"}

DATADICTS = [niog_data, ryder_data, petermann_data, steensby_data]

ryder_data["smb"] = -1.16
steensby_data["smb"] = -0.76
petermann_data["smb"] = -1.98
niog_data["smb"] = -1.5

ryder_data["smb_rmsd"] = 0.8
steensby_data["smb_rmsd"] = 0.8
petermann_data["smb_rmsd"] = 0.8
niog_data["smb_rmsd"] = 0.8

ryder_data["meltgrid"] = karta.read_geotiff("adot-composites/Ryder-128m_20161101.tif")
steensby_data["meltgrid"] = karta.read_geotiff("adot-composites/Steensby-128m_20161101.tif")
petermann_data["meltgrid"] = karta.read_geotiff("adot-composites/Petermann-128m_20161101.tif")
niog_data["meltgrid"] = karta.read_geotiff("adot-composites/Nioghalvfjerdsbrae-128m_20161101.tif")

# get these from jackknife_flux.py
ryder_data["melt_std"] = 0.1283e9
steensby_data["melt_std"] = 0.0e9        # not computed
petermann_data["melt_std"] = 1.3119e9
niog_data["melt_std"] = 1.0426e9

### ICE FLUX

_glgates = karta.vector.read_shapefile("grounding-line-gates.shp")
glgates = {geo.properties["Glacier"]:geo for geo in _glgates}

_dir = "/home/natw/Documents/papers/meltrates/data/"
niog_data["elev_uncertainty"] = karta.read_geotiff(_dir + "niog_elev_std.tif")
petermann_data["elev_uncertainty"] = karta.read_geotiff(_dir + "petermann_elev_std.tif")
ryder_data["elev_uncertainty"] = karta.read_geotiff(_dir + "ryder_elev_std.tif")
steensby_data["elev_uncertainty"] = karta.read_geotiff(_dir + "steensby_elev_std.tif")

for dd in DATADICTS:
    dd["thickness_uncertainty"] = dd["elev_uncertainty"].apply(lambda a: a/(1-920.0/1028.0))

#niog_data["datum"] = 29.4
#petermann_data["datum"] = 14.9
#ryder_data["datum"] = 21.16
#steensby_data["datum"] = 19.21
niog_data["datum"] = 15.6
petermann_data["datum"] = 7.2
ryder_data["datum"] = 13.3
steensby_data["datum"] = 8.2

def compute_thickness(elev, datum):
    # use SimpleBand for performance reasons (faster point sampling)
    h = karta.raster.RegularGrid(elev.transform,
                                 values=(elev[:,:]-datum)/(1-920.0/1028.0), crs=elev.crs,
                                 bandclass=karta.raster.band.SimpleBand)
    return h

for dd in DATADICTS:
    u = karta.raster.read_geotiff(os.path.join(dd["dirname"], "vel-composites-128m", "dx.tif"))
    v = karta.raster.read_geotiff(os.path.join(dd["dirname"], "vel-composites-128m", "dy.tif"))
    dd["uvel"] = u
    dd["vvel"] = v

niog_data["dem"] = karta.raster.read_geotiff("dem-composites-128m/Nioghalvfjerdsbrae-128m.tif")
ryder_data["dem"] = karta.raster.read_geotiff("dem-composites-128m/Ryder-128m.tif")
steensby_data["dem"] = karta.raster.read_geotiff("dem-composites-128m/Steensby-128m.tif")
petermann_data["dem"] = karta.raster.read_geotiff("dem-composites-128m/Petermann-128m.tif")

#niog_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/nioghalvfjerdsbrae_composite.tif")
#ryder_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/ryder_composite.tif")
#steensby_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/steensby_composite.tif")
#petermann_data["dem"] = karta.raster.read_geotiff("dem-composites-10m/petermann_composite.tif")

for data in DATADICTS:
    data["dem"].set_nodata_value(np.nan)

niog_data["thickness"] = compute_thickness(niog_data["dem"], niog_data["datum"])
ryder_data["thickness"] = compute_thickness(ryder_data["dem"], ryder_data["datum"])
petermann_data["thickness"] = compute_thickness(petermann_data["dem"], petermann_data["datum"])
steensby_data["thickness"] = compute_thickness(steensby_data["dem"], steensby_data["datum"])

def dot(u, v):
    return u[0]*v[0] + u[1]*v[1]

def normalise(u):
    mag = math.sqrt(u[0]*u[0] + u[1]*u[1])
    return [u[0]/mag, u[1]/mag]

def unit_vector(az):
    az = 90-az   # convert geographical angle to canonical mathematical angle
    u = math.cos(az*math.pi/180.0), math.sin(az*math.pi/180.0)
    return normalise(u)

def rot90_vector(u):
    """ rotate a vector by 90 degrees """
    return [-u[1], u[0]]

def compute_normal_flux(line, uflux, vflux, resolution=None):
    """ compute flux through a Line. return (x, F, pts) where

        x is the cumulative distance from point to point generated along a line
        F is the flux through each point
        pts is the list of generated points
    """
    if resolution is None:
        resolution = min(uflux.resolution)
    points = line.to_points(resolution)
    uflux_smpl = uflux.sample(points)
    vflux_smpl = vflux.sample(points)
    point_dist = karta.Line(points).cumulength()

    seg_azimuths = []
    seg_midpoints = []
    for seg in line.segments:
        az = seg[0].azimuth(seg[1])
        seg_azimuths.append(az)
        seg_azimuths.append(az)
        seg_midpoints.append(seg[0].walk(1, az))
        seg_midpoints.append(seg[0].walk(seg[0].distance(seg[1])-1, az))
    seg_dist = karta.Line(seg_midpoints).cumulength()

    az_int = interp1d(seg_dist, seg_azimuths, kind="nearest", fill_value="extrapolate")(point_dist)
    flux = [abs(dot(rot90_vector(unit_vector(az)), [u,v]))
            for az, u, v in zip(az_int, uflux_smpl.ravel(), vflux_smpl.ravel())]
    return point_dist, flux, points

def interpolate_nans(a):
    x = np.arange(len(a))
    f = interp1d(x[~np.isnan(a)], np.array(a)[~np.isnan(a)], fill_value="extrapolate")
    return f(x)

mc_thickness = karta.raster.read_geotiff("../BedMachine/thickness.tif")\
                           .resize([-6e5, -11e5, 8e5, -8e5])
mc_thickness = karta.RegularGrid(mc_thickness.transform,
                                 values=mc_thickness[:,:],
                                 crs=mc_thickness.crs,
                                 bandclass=karta.raster.SimpleBand)

for data in DATADICTS:

    uvel = data["uvel"]
    vvel = data["vvel"]
    cg = uvel.coordinates()
    uflux_arr = uvel[:,:,0] * mc_thickness.sample(*cg[:,:]).squeeze()
    vflux_arr = vvel[:,:,0] * mc_thickness.sample(*cg[:,:]).squeeze()
    uflux_arr[~uvel.data_mask] = uvel.nodata
    vflux_arr[~vvel.data_mask] = vvel.nodata
    data["uflux_mc"] = karta.RegularGrid(uvel.transform, values=uflux_arr, nodata_value=uvel.nodata, crs=uvel.crs)
    data["vflux_mc"] = karta.RegularGrid(vvel.transform, values=vflux_arr, nodata_value=vvel.nodata, crs=vvel.crs)

    bb = uvel.bbox()
    res = uvel.resolution

    th = data["thickness"].resize([bb[0]-res[0], bb[1]-res[1], bb[2]+res[0], bb[3]+res[1]])
    uflux_arr = uvel[:,:,0] * th.sample(*cg[:,:]).squeeze()
    vflux_arr = vvel[:,:,0] * th.sample(*cg[:,:]).squeeze()
    uflux_arr[~uvel.data_mask] = uvel.nodata
    vflux_arr[~vvel.data_mask] = vvel.nodata
    data["uflux"] = karta.RegularGrid(uvel.transform, values=uflux_arr, nodata_value=uvel.nodata, crs=uvel.crs)
    data["vflux"] = karta.RegularGrid(vvel.transform, values=vflux_arr, nodata_value=vvel.nodata, crs=vvel.crs)

    thu = data["thickness_uncertainty"].resize([bb[0]-res[0], bb[1]-res[1], bb[2]+res[0], bb[3]+res[1]])
    uflux_arr = uvel[:,:,0] * thu.sample(*cg[:,:]).squeeze()
    vflux_arr = vvel[:,:,0] * thu.sample(*cg[:,:]).squeeze()
    uflux_arr[~uvel.data_mask] = uvel.nodata
    vflux_arr[~vvel.data_mask] = vvel.nodata
    data["uflux_err"] = karta.RegularGrid(uvel.transform, values=uflux_arr, nodata_value=uvel.nodata, crs=uvel.crs)
    data["vflux_err"] = karta.RegularGrid(vvel.transform, values=vflux_arr, nodata_value=vvel.nodata, crs=vvel.crs)

    x, y, pointdata = compute_normal_flux(glgates[data["name"]],
                                          data["uflux_mc"], data["vflux_mc"])
    data["gl_flux_mc"] = scipy.integrate.simps(interpolate_nans(y), x=x)
    data["gl_pointdata_mc"] = pointdata

    x, y, pointdata = compute_normal_flux(glgates[data["name"]],
                                          data["uflux"], data["vflux"])
    _, y_err, _ = compute_normal_flux(glgates[data["name"]],
                                      data["uflux_err"], data["vflux_err"])
    data["gl_flux"] = scipy.integrate.simps(interpolate_nans(y), x=x)
    data["gl_flux_err"] = scipy.integrate.simps(interpolate_nans(y_err), x=x)
    data["gl_pointdata"] = pointdata

# Print a report
MWE = 0.92

for data in DATADICTS:
    meltgrid = data["meltgrid"]
    thickgrid = data["thickness"]
    pixelarea = meltgrid.resolution[0] * meltgrid.resolution[1]
    submelt = np.sum(meltgrid[meltgrid.data_mask]) * pixelarea
    melt_std = data["melt_std"]
    sfc_melt = np.sum(meltgrid.data_mask * data["smb"] * pixelarea / .92)
    #sfc_melt_rmsd = np.sqrt(np.sum(meltgrid.data_mask * data["smb_rmsd"]**2)) * pixelarea / .92
    # treat sfc rmsd as an estimate with 1 degree of freedom
    sfc_melt_rmsd = np.sum(meltgrid.data_mask * data["smb_rmsd"]) * pixelarea / .92
    submelt_std = np.sqrt(melt_std**2 + sfc_melt_rmsd**2)
    data["submelt_std"] = submelt_std
    glf = abs(data["gl_flux"])
    glf_err = abs(data["gl_flux_err"])
    glfmc = abs(data["gl_flux_mc"])
    vol = (np.nansum(thickgrid[meltgrid.data_mask])*pixelarea)
    submelt_percent = np.abs(submelt) / vol * 100
    shrink_percent = (np.abs(submelt) + np.abs(sfc_melt) - np.abs(glf)) / vol * 100

    print("""{name}

\tvolume         \t{vol:.2f} km^3
\tsubmarine flux:\t{mf:.3f} ± {mf_std:.3f} km^3/yr
\t               \t({submelt_perc:.2f}% of shelf volume)
\tsurface flux   \t{smf:.3f} ± {smf_rmsd:.3f} km^3/yr
\ttotal melt flux\t{tmf:.3f} ± {tmf_rmsd:.3f} km^3/yr
\tGL flux:       \t{glf:.3f} ± {glf_err:.3f} km^3/yr ({glf_rat:.2f})
\tGL flux (MC):  \t{glfmc:.3f} km^3/yr ({glfmc_rat:.2f})
\tvol change:    \t{shrink_perc:.2f}%
""".format(name=data["name"], vol=vol/1e9*MWE,
           glf=glf/1e9*MWE, glfmc=glfmc/1e9*MWE, glf_err=glf_err/1e9*MWE,
           glf_rat=glf/submelt, glfmc_rat=glfmc/submelt,
           mf=submelt/1e9*MWE, mf_std=submelt_std/1e9*MWE,
           smf=sfc_melt/1e9*MWE, smf_rmsd=sfc_melt_rmsd/1e9*MWE,
           tmf=(submelt+sfc_melt)/1e9*MWE, tmf_rmsd=melt_std/1e9*MWE,
           submelt_perc=submelt_percent, shrink_perc=-shrink_percent))

#####################################
#                                   #
#  SAVE RESULTS TO AN HDF DATASET   #
#                                   #
#####################################

print("writing HDF5 file")
with h5py.File("/home/natw/Documents/papers/meltrates/data/fluxes_4.h5", "w") as ds:
    g_gl = ds.create_group("groundinglineflux")
    g_gl_err = ds.create_group("groundinglineflux_err")
    g_sub = ds.create_group("submarinemeltflux")
    g_sub_err = ds.create_group("submarinemeltflux_std")
    g_sfc = ds.create_group("surfacemeltflux")
    g_sfc_err = ds.create_group("surfacemeltflux_rmsd")
    g_tot_err = ds.create_group("totalmeltflux_std")

    for d in DATADICTS:
        _d = g_gl.create_dataset(d["name"], (1,))
        _d[0] = abs(d["gl_flux"])

        _d = g_gl_err.create_dataset(d["name"], (1,))
        _d[0] = abs(d["gl_flux_err"])

        _d = g_sub.create_dataset(d["name"], (1,))
        grid = d["meltgrid"]
        pixelarea = grid.resolution[0] * grid.resolution[1]
        _d[0] = np.nansum(grid[grid.data_mask]) * pixelarea

        _d = g_sub_err.create_dataset(d["name"], (1,))
        _d[0] = d["submelt_std"]

        _d = g_sfc.create_dataset(d["name"], (1,))
        _d[0] = -abs(np.sum(grid.data_mask * d["smb"] * pixelarea)/0.92)

        _d = g_sfc_err.create_dataset(d["name"], (1,))
        _d[0] = abs(np.sum(grid.data_mask * d["smb_rmsd"] * pixelarea)/0.92)

        _d = g_tot_err.create_dataset(d["name"], (1,))
        _d[0] = d["melt_std"] * 0.92


