#! /usr/bin/env python
"""
Align a DEM using a combination of GIMP and CryoSat data.

This is a rewrite designed to be more easily unit-tested.
"""

import os
import io
import sys
from glob import fnmatch
import logging
from subprocess import Popen, PIPE
import tempfile
import traceback

import karta
import numpy as np

sys.path.append("../bin/")
import wvutil

import termcolor

INPUTS = "../dems-64m"
PATTERN = "*.tif"
OUTPUTS = "dems-aligned"

REFERENCE_DEM = "../gimpdem_90m_northern.tif"
ICEMASK = "../../GIMP/GimpIceMask_90m_8bit.tif"
OVERWRITE = False

TEMPDIR = "tmp-align"

# Strategy
#
# make a mask for non-ocean, non-ice pixels
# use pc_align to compute horizontal translation from gimp
# apply tide correction
# use combination of masked gimp + tide-corrected cryosat to compute vertical offset

class ColorFormatter(logging.Formatter):

    def __init__(self):
        self.fmt = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    def format(self, record):
        s = self.fmt.format(record)
        if record.levelno == 10:    # DEBUG
            return termcolor.colored(s, "grey")
        elif record.levelno == 20:  # INFO
            return termcolor.colored(s, "cyan")
        elif record.levelno == 30:  # WARN
            return termcolor.colored(s, "yellow")
        elif record.levelno == 40:  # ERROR
            return termcolor.colored(s, "red")
        elif record.levelno == 50:  # FATAL
            return termcolor.colored(s, "red", attrs=["bold"])
        else:
            return s

def apply_raster_mask(dem, mask):
    """ mask a dem using global ICEMASK_GRID """
    pieces = []
    for chunk in dem.aschunks(dem.bands[0].size):
        x, y = chunk.coordmesh()
        msk = ICEMASK_GRID.sample_bilinear(x.ravel(), y.ravel()).astype(np.bool)
        msk = msk.reshape(chunk.size)
        chunk[msk] = chunk.nodata
        pieces.append(chunk)
    mask_dem = karta.raster.merge(pieces)
    return mask_dem

def translation_pc_align(dem, reference):
    try:
        # Hope for no file collisions
        demtmpname = os.path.join(TEMPDIR, tempfile.mkstemp(dir=TEMPDIR)[1]+".tif")
        reftmpname = os.path.join(TEMPDIR, tempfile.mkstemp(dir=TEMPDIR)[1]+".tif")
        prefix = os.path.join(TEMPDIR, tempfile.mkstemp(dir=TEMPDIR)[1])
        dem.to_geotiff(demtmpname, compress='LZW')
        reference.to_geotiff(reftmpname)

        cmd = ["pc_align", "--max-displacement", "100.0",
               "--compute-translation-only",
               demtmpname, reftmpname, "-o", prefix]
        LOG.info("executing {0}".format(" ".join(cmd)))
        proc = Popen(cmd, stdout=PIPE)
        for line in proc.stdout.readlines():
            LOG.debug(line.decode("utf-8").rstrip("\n"))

    except Exception as e:
        LOG.error(traceback.format_exc())
        sys.exit(1)

    finally:
        os.remove(reftmpname)
        os.remove(demtmpname)

    try:
        trans = read_transform(prefix)
        return trans
    except FileNotFoundError:
        return None

def read_transform(prefix):
    with open(prefix+"-inverse-transform.txt") as f:
        rows = [list(map(float, line.split())) for line in f.readlines()]
    return np.array(rows)

def translate_grid_in_place(grid, transform):
    T = grid.transform
    grid._transform = (T[0]+transform[0,3], T[1]+transform[1,3], T[2], T[3], T[4], T[5])
    grid.apply(lambda z: z + transform[2, 3], inplace=True)
    return

def apply_tidal_correction(dem, scid):
    """ de-tides floating ice tongue pixels in dem in-place """
    ztide = wvutil.get_tidal_height(scid)
    outline, _ = wvutil.get_best_outline(wvutil.get_region(scid),
                                         wvutil.get_date(scid))
    msk = dem.mask_by_poly(outline)
    dem[msk.data_mask] -= ztide
    return

def compute_cryosat_offset(dem, scid, timedelta):
    # load all cryosat data within dem bbox and time delta
    # sample the dem at the cryosat points
    # return the mean and standard deviation of the difference
    raise NotImplementedError()


def filter_unrealistic_values(arr):
    realmsk = ~np.isnan(arr)
    real = arr[realmsk]
    neg = real < -100
    pos = real > 10000
    real[neg] = np.nan
    real[pos] = np.nan
    arr[realmsk] = real
    return arr

if __name__ == "__main__":

    LOG = logging.Logger(__file__)
    handler = logging.StreamHandler(sys.stdout)
    formatter = ColorFormatter()
    handler.setFormatter(formatter)
    LOG.addHandler(handler)

    LOG.debug("checking directories")
    wvutil.ensureisdir(OUTPUTS)
    wvutil.ensureisdir(TEMPDIR)

    LOG.debug("loading reference data")
    ICEMASK_GRID = karta.read_geotiff(ICEMASK)
    REFERENCE_GRID = karta.read_geotiff(REFERENCE_DEM)

    for demfnm in filter(lambda f: fnmatch.fnmatch(f, PATTERN), os.listdir(INPUTS)):

        scid = wvutil.get_sceneid(demfnm)
        outfnm = os.path.join(OUTPUTS, os.path.split(demfnm)[1])

        if os.path.isfile(outfnm) and not OVERWRITE:
            LOG.warning("skipping %s" % outfnm)
            continue

        LOG.info("working on scene %s" % demfnm)
        dem = karta.read_geotiff(os.path.join(INPUTS, demfnm))
        if len(dem.bands) != 1:
            LOG.error("multiple DEM bands unsupported because it will raise an "
                      "exception in raster.grid.merge")
            continue
        try:
            dem.set_nodata_value(np.nan)
        except:
            LOG.critical(traceback.format_exc())
        dem.apply(filter_unrealistic_values, inplace=True)

        LOG.debug("resizing and masking grids")
        try:
            ref = REFERENCE_GRID.resize(dem.bbox)
            mask = ICEMASK_GRID.resize(dem.bbox)
            dem_masked = apply_raster_mask(dem, mask)
        except Exception as e:
            LOG.error(traceback.format_exc())
            sys.exit(1)

        LOG.debug("computing translation")
        try:
            # Hack for space efficiency (works with LZW compression)
            dem_masked.set_nodata_value(-3.4028234663852886e+38)
            trans = translation_pc_align(dem_masked, ref)
        except Exception as e:
            LOG.error(traceback.format_exc())
            sys.exit(1)

        if trans is None:
            LOG.error("failure to compute translation")
            del dem, ref, mask
            continue

        LOG.info("translation {0}".format(trans))
        translate_grid_in_place(dem, trans)
        LOG.debug("de-tiding ice shelf pixels")
        apply_tidal_correction(dem, scid)

        # z_off, z_std = compute_cryosat_offset(dem, scid)
        z_off = 0.0; z_std = 1.0
        if abs(z_off) > z_std:
            dem.apply(lambda z: z-z_off, inplace=True)

        dem.to_geotiff(outfnm)
        del dem, ref, mask

