""" convenience functions for cryosat data """
import datetime

def _read_csv(fnm):
    lines = []
    with open(fnm, "r") as f:
        while True:
            try:
                line = f.readline()
            except EOFError:
                break
            if len(line) == 0:
                break
            line = line.split(",")
            try:
                days = int(line[0])
                seconds = int(line[1])
                microseconds = int(line[2])
                lon = float(line[3])
                lat = float(line[4])
                elev = float(line[5])
                quality = int(line[6])
                quality_flag = int(line[7])
                lines.append((days, seconds, microseconds,
                              lon, lat, elev,
                              quality, quality_flag))
            except IndexError:
                print("malformed record: {0}".format(line))
                break
            except ValueError:
                print("line")
                print(line)
    return lines

def asdatetime(*args):
    """ convert either a tuple *(days, seconds, microseconds)* or three
    arguments *days*, *seconds*, *microseconds* to a datetime """
    if len(args) == 1:
        days, seconds, microseconds = args[0]
    elif len(args) == 3:
        days, seconds, microseconds = args
    else:
        raise ValueError("asdatetime takes 1 or 3 arguments")
    return datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc) + \
           datetime.timedelta(days=days, seconds=seconds + microseconds / 1000.0)

def search_date(records, date):
    """ search for the largest record index that is <= *date*
    raises ValueError if date > records[-1]
    """
    # binary search
    imin = 0
    imax = len(records)

    if (asdatetime(*records[-1][:3]) < date):
        raise ValueError("final record precedes date")

    i = imax // 2
    while (imax-imin > 2):
        recdate = asdatetime(*records[i][:3])
        if recdate < date:
            imin = i
            i = i + (imax - i) // 2
        elif recdate > date:
            imax = i
            i = imin + (i - imin) // 2
        else:
            return i
    if asdatetime(*records[i][:3]) > date:
        i -= 1
    return i

def iswithin(x, y, bbox):
    return (bbox[0] <= x < bbox[2]) and (bbox[1] <= y < bbox[3])

def search_within(records, bbox):
    """ return a list of indices indicating records within a bounding box """
    return [i for i, rec in enumerate(records) if iswithin(rec[3], rec[4], bbox)]

petermann = _read_csv("/home/natw/Documents/Greenland_data/CryoSat/petermann-all.csv")

