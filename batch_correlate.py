#! /usr/bin/env python

from __future__ import print_function
import os
import sys
import subprocess
import traceback
import wvutil

TOPDIR = "hillshades-25m-aligned/"
SAVEDIR = "correlation-data-25m/"
OVERWRITE = False
RES = (100.0, 100.0)
REGIONS = {"Nioghalvfjerdsbrae", "Petermann", "Ryder", "Steensby"}

wvutil.ensureisdir(SAVEDIR)

scenepairs_fnm = sys.argv[1]
with open(scenepairs_fnm, "r") as f:
    pairs = [line.split() for line in f.readlines() if len(line) != 0]

def get_input_image(sceneid, dirname):
    for fnm in os.listdir(dirname):
        if (fnm.endswith(".tif") and (wvutil.get_sceneid(fnm) == sceneid)):
            return os.path.join(dirname, fnm)
    raise KeyError("%s not found in %s" % (sceneid, dirname))

velocity_data = {
        "Nioghalvfjerdsbrae":
            {"u": "../MEaSUREs_velocity/niog_2008_2009_vx.tif",
             "v": "../MEaSUREs_velocity/niog_2008_2009_vy.tif"},
        "Petermann":
            {"u": "../MEaSUREs_velocity/petermann_vx.tif",
             "v": "../MEaSUREs_velocity/petermann_vy.tif"},
        "Ryder":
            {"u": "../MEaSUREs_velocity/ryder_vx.tif",
             "v": "../MEaSUREs_velocity/ryder_vy.tif"},
        "Steensby":
            {"u": "../MEaSUREs_velocity/steensby_vx.tif",
             "v": "../MEaSUREs_velocity/steensby_vy.tif"},
            }

for i, (scid1, scid2) in enumerate(pairs):

    reg = wvutil.get_region(scid1)

    if reg not in REGIONS:
        continue

    try:
        if wvutil.get_date(scid1) > wvutil.get_date(scid2):
            scid1, scid2 = scid2, scid1
    except Exception as e:
        traceback.print_exc()
        continue

    try:
        image1 = get_input_image(scid1, TOPDIR)
        image2 = get_input_image(scid2, TOPDIR)
    except KeyError:
        continue

    dt = wvutil.get_baseline(scid1, scid2)
    out_fnm = os.path.join(SAVEDIR, "{0}_{1}.h5".format(scid1, scid2))

    print("Processing %s, %s" % (scid1, scid2))
    print("\t", wvutil.get_date(scid1), wvutil.get_date(scid2))

    cmd = ["python", "correlate-scenes.py",
            image1, image2,
            velocity_data[reg]["u"],
            velocity_data[reg]["v"],
            out_fnm,
            "--baseline", str(dt),
            "--resolution", str(RES[0]), str(RES[1]),
            "--search-size", str(256), str(256),
            "--ref-size", str(64), str(64),
            "--threads", "4"]
    if OVERWRITE:
        cmd.append("--overwrite")

    subprocess.call(cmd)

