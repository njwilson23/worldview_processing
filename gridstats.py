import karta
import numpy as np

def gridstats(grids):
    """ Returns the mean, variance and pixel counts for a list of grids """
    # Check grid class
    if not all(isinstance(grid, karta.RegularGrid) for grid in grids):
        raise NotImplementedError("All grids must be type RegularGrid")

    T = grids[0].transform
    # Check grid stretch and skew
    for i, grid in enumerate(grids[1:]):
        if grid.transform[2:6] != T[2:6]:
            raise NotImplementedError("grid %d transform stretch and skew "
                    "does not match grid 1" % (i+2,))

    # Check grid offset
    excmsg = "grid %d not an integer translation from grid 1"
    for i, grid in enumerate(grids[1:]):
        if ((grid.transform[0]-T[0]) / float(T[2])) % 1 > 1e-15:
            raise NotImplementedError(excmsg % (i+2,))
        if ((grid.transform[1]-T[1]) / float(T[3])) % 1 > 1e-15:
            raise NotImplementedError(excmsg % (i+2,))

    gridmean = karta.raster.merge(grids)
    xmin, xmax, ymin, ymax = gridmean.get_extent(reference="edge")
    ny, nx = gridmean.size

    # Allocate data array and copy each grid's data
    typ = gridmean.bands[0].dtype
    values = np.zeros([ny, nx], dtype=typ)
    counts = np.zeros([ny, nx], dtype=np.int16)
    for grid in grids:
        _xmin, _xmax, _ymin, _ymax = grid.get_extent(reference='edge')
        offx = int((_xmin-xmin) / T[2])
        offy = int((_ymin-ymin) / T[3])
        _ny, _nx = grid.size

        mask = grid.data_mask
        counts[offy:offy+_ny,offx:offx+_nx][mask] += 1
        values[offy:offy+_ny,offx:offx+_nx][mask] += \
            typ((grid[mask]-gridmean[offy:offy+_ny, offx:offx+_nx][mask])**2)
        del mask

    validcountmask = (counts!=0)
    values[validcountmask] = values[validcountmask] / counts[validcountmask]
    values[~validcountmask] = grids[0].nodata
    Tmerge = [xmin, ymin] + list(T[2:])
    return (gridmean,
            karta.RegularGrid(Tmerge, values=values, crs=gridmean.crs,
                              nodata_value=gridmean.nodata),
            karta.RegularGrid(Tmerge, values=counts, crs=gridmean.crs,
                              nodata_value=-1))
