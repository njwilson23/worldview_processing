#! /usr/bin/env python
""" Replaces the finicky Makefiles in each project folder

- Projects and clips all DEMs and error outputs
- Creates mosaic'd DEM (but note that bundle adjustment is not performed)
"""

from __future__ import print_function

import os
from os import path
import re
import subprocess

PROJECTS = {"Nioghalvfjerdsbrae":
                {"bbox": (431950, -1090050, 503550, -996950),
                 "directory": "Nioghalvfjerdsbrae/"},
            "Petermann":
                {"bbox": (-287050, -1008050, -251950, -929950),
                 "directory": "Petermann/"},
            "Ryder":
                {"bbox": (-95050, -920050, -79950, -879950),
                 "directory": "Ryder/"},
            "Steensby":
                {"bbox": (-158050 -919050 -144950 -898950),
                 "directory": "Steensby/"},
            }

def compare_files(*files):
    """ Returns a list of integers representing the order of modification times
    of the inputs """
    mtimes = [os.path.getmtime(f) for f in files]

    # Find the sorted order
    idxs = [a[1] for a in sorted(zip(mtimes, range(len(mtimes))))]
    order = [a[1] for a in sorted(zip(idxs, range(len(mtimes))))]
    return order

def mosaic_grids(infiles, outfnm):
    cmd = ["gdal_merge.py", "-o", outfnm,
           "-of", "GTiff",
           "-n", "-3.4028234663852886e+38",
           "-a_nodata", "-3.4028234663852886e+38"]
    cmd.extend(infiles)
    print("\n"," ".join(cmd),"\n")
    return subprocess.call(cmd)

def project_nsidc(fnm, outfnm, bbox, **kw):
    kw.setdefault("resolution", (100, 100))
    cmd = ["gdalwarp", "-t_srs",
           "+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs +a=6378137.0 +f=0.0033528106647474805",
           "-te"] + list(map(str, bbox))
    dx, dy = kw["resolution"]
    cmd.extend(["-tr", str(dx), str(dy), "-overwrite", fnm, outfnm])
    print("\n"," ".join(cmd),"\n")
    return subprocess.call(cmd)

def directory_project_nsidc(dirname, bbox, **kw):
    """ Projects all Tiffs in *dirname* without "NSIDC" in the name to
    NSIDCNorth, clipped to *bbox* """
    raw_dems = list(filter(lambda s: s.endswith(".tif") and "NSIDC" not in s,
                           os.listdir(dirname)))
    nsidc_dems = [path.splitext(fnm)[0]+"-NSIDC.tif" for fnm in raw_dems]

    for rawdem, nsidcdem in zip(raw_dems, nsidc_dems):
        rawdem_path = path.join(dirname, rawdem)
        nsidcdem_path = path.join(dirname, nsidcdem)

        if path.isfile(nsidcdem_path) and compare_files(rawdem_path, nsidcdem_path)[0]==0:
            print("skipping %s because it is up to date" % nsidcdem_path)
        else:
            err = project_nsidc(rawdem_path, nsidcdem_path, bbox, **kw)
            if err != 0:
                raise RuntimeError()

def update_project(projname):
    """ Projects/clips all DEMs and error grids associated with a project and
    builds a rough composite image. """
    if projname not in PROJECTS:
        raise KeyError("No project called '%s'" % projname)
    project = PROJECTS[projname]

    directory_project_nsidc(path.join(project["directory"], "dems"), project["bbox"],
                            resolution=(100,100))
    directory_project_nsidc(path.join(project["directory"], "error"), project["bbox"],
                            resolution=(100, 100))
    directory_project_nsidc(path.join(project["directory"], "projL"), project["bbox"],
                            resolution=(10, 10))

    dem_pieces = list(filter(lambda s: s.endswith(".tif") and "NSIDC" in s,
                             os.listdir(path.join(project["directory"], "dems"))))
    dem_pieces = [path.join(project["directory"], "dems", fnm) for fnm in dem_pieces]
    composite_dem = path.join(project["directory"], "composite_dem.tif")

    if path.isfile(composite_dem) and \
            compare_files(composite_dem, *dem_pieces)[0]==len(dem_pieces):
        print("skipping because %s is up to date" % composite_dem)
    else:
        err = mosaic_grids(dem_pieces, composite_dem)
        if err != 0:
            raise RuntimeError()

if __name__ == "__main__":

    update_project("Ryder")
    update_project("Steensby")
    update_project("Petermann")
    update_project("Nioghalvfjerdsbrae")

